<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <title>@yield('title')</title>
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="{{asset('/elementUI/index.js')}}"></script>
    <script src="{{asset('/js/axios.min.js')}}"></script>
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/elementUI/index.css')}}">
    @yield('link')
</head>
<body>
<header id="header">
    <el-page-header @back="goBack" content="@yield('headerTitle')" class="go-back">
    </el-page-header>
</header>

<section id="main">
    @yield('content')
</section>


@yield('js')
<script>
    var header = new Vue({
        el: '#header',
        data() {
            return {
            }
        },
        methods: {
            goBack() {
                history.back(-1);
            },
        }

    })
</script>

<style>
    *{
        padding: 0;
        margin: 0;
    }
    #header {
        min-width: 1400px;
    }
    .go-back {
        height: 45px;
        line-height: 45px;
        background-color: #2a3237;
        color: white !important;
        padding-left: 1%;
    }
    .go-back .el-page-header__content{
        color: white !important;
    }

    .el-step__head.is-finish, .el-step__title.is-finish {
        color: #ff6d1c;
        border-color: #ff6d1c;
    }

    .el-step__head.is-finish .el-step__line {
        /*border: #2a3237 1px solid;*/
        color: #ff6d1c;
    }
</style>
</body>
</html>
