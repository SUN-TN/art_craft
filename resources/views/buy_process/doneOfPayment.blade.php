@extends('buy_process.master')

@section('title')
    支付完成-craft
@endsection

@section('link')
@endsection

@section('headerTitle')
    支付完成
@endsection

@section('content')
    <section id="app">
        <el-row style="margin-top: 50px">
            <el-col :span="18" :offset="3">
                <el-steps :active="orderStatus">
                    <el-step title="下单" icon="el-icon-tickets"></el-step>
                    <el-step title="支付" icon="el-icon-coin"></el-step>
                    <el-step title="下单成功" icon="el-icon-circle-check"></el-step>
                </el-steps>
            </el-col>
        </el-row>


        <section class="guess-like-section">
            <div class="title">猜你喜欢</div>
            <el-row class="guess-like-row">
                <el-col :span="18" :offset="3">
                    <el-row :gutter="30">
                        <el-col :span="6" class="guess-like-box" v-for="(item,index) in likeRow1" :key="item.id">
                            <div>
                                <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                <div class="img-info" @click="detail(item)">
                                    <div class="img-info-item">@{{ item.author }}</div>
                                    <div class="img-info-item">@{{ item.name }}</div>
                                    <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                </div>
                            </div>
                        </el-col>
                    </el-row>

                    <el-row class="guess-like-row" :gutter="30">
                        <el-col :span="6" class="guess-like-box" v-for="(item,index) in likeRow2" :key="item.id">
                            <div>
                                <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl" @click="detail(item)">
                                <div class="img-info" @click="detail(item)">
                                    <div class="img-info-item">@{{ item.author }}</div>
                                    <div class="img-info-item">@{{ item.name }}</div>
                                    <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                </div>
                            </div>
                        </el-col>
                    </el-row>

                </el-col>
            </el-row>
        </section>
    </section>
@endsection

@section('js')
    <script>
        var app = new Vue({
            el: '#app',
            created(){
                for (let i = 0; i < this.guessLike.length; i++) {
                    if (i < this.guessLike.length / 2) {
                        this.likeRow1.push(this.guessLike[i]);
                    } else {
                        this.likeRow2.push(this.guessLike[i]);
                    }
                }
            },
            data() {
                return {
                    guessLike:(@json($data)).like,
                    orderId:(@json($data)).id,
                    likeRow1: [],
                    likeRow2: [],
                    orderStatus:3,
                }
            },
            methods: {
                detail(item){
                    window.location='/detail/'+item.id;
                }
            }

        });

        function img_info() {
            $('.goods_img').on({
                'mouseenter': function () {
                    $(this).next().show();
                },
                "mouseleave": function () {
                    $(this).next().hide();
                }
            });
            $(".img-info").on({
                'mouseenter': function () {
                    $(this).show();
                },
                "mouseleave": function () {
                    $(this).hide();
                }
            });

        }

        img_info();
    </script>

    <style>
        #app{
            min-width: 1400px;
        }
        .guess-like-section{
            margin-top: 50px;
            padding-bottom: 80px;
        }
        .guess-like-section .title{
            width: 200px;
            height: 80px;
            line-height: 80px;
            font-size: 2em;
            background-color: #2a3237;
            color: white;
            margin: 0 auto;
            border-radius: 5px;
            text-align: center;
        }
        .guess-like-box{
            margin-top: 50px;
            overflow: hidden;
            cursor: pointer;
        }
        .guess-like-box div{
            position: relative;
            width: 100%;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border-radius: 5px;
        }
        .guess-like-box .goods_img{
            transition: 0.5s;
            width: 100%;
            height: 300px;
            border-radius: 5px;
            overflow: hidden;
            object-fit: cover;
            z-index: 1;
        }

        .guess-like-box .img-info{
            transition: 0.5s;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            overflow: hidden;
            height: 300px;
            color: white;
            z-index: 5;
            background-color: rgba(61, 67, 67, 0.7);
            text-align: center;
            display: none;
        }

        .guess-like-box .img-info-item:first-child {
            margin-top: 18%;
        }

        .guess-like-box .img-info-item {
            margin-top: 30px;
            font-size: 1.2em;
        }

        @media screen and (max-width:1750px) {
            .guess-like-box .goods_img,.guess-like-box .img-info{
                height: 280px;
            }
        }
        @media screen and (max-width:1700px) {
            .guess-like-box .goods_img,.guess-like-box .img-info{
                height: 270px;
            }
        }

        @media screen and (max-width:1600px) {
            .guess-like-box .goods_img,.guess-like-box .img-info{
                height: 260px;
            }
        }

        @media screen and (max-width:1500px) {
            .guess-like-box .goods_img,.guess-like-box .img-info{
                height: 220px;
            }
        }

        @media screen and (max-width:1400px) {
            .guess-like-box .goods_img,.guess-like-box .img-info{
                height: 200px;
            }
        }

    </style>

@endsection
