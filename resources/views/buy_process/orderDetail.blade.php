@extends('buy_process.master')

@section('title')
    订单详情-craft
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/buy_process/orderDetail.css')}}">
@endsection

@section('headerTitle')
    订单详情
@endsection

@section('content')
    <section id="app" style="margin-top: 50px;padding-bottom: 50px">
        <el-row>
            <el-col :span="18" :offset="3">
                <el-row :gutter="50" style="margin-top: 50px">

                    <el-col :span="10">
                        <el-image class="crafts-item-img" :src="'{{asset('/')}}'+order.imgUrl"
                                  @click="detail"></el-image>
                    </el-col>


                    <el-col :span="11" :offset="1" class="orderInfo">
                        <el-form :model="order" label-position="top">
                            <el-form-item label="收货人地址" class="address">
                                <el-row>
                                    <div class="useAddress-item">@{{ order.name }}</div>
                                    <div class="useAddress-item">@{{ order.phone }}</div>
                                    <div class="useAddress-item">
                                        @{{ order.province }} |
                                        @{{ order.city }} |
                                        @{{ order.county }} |
                                        @{{ order.region }}
                                    </div>
                                    <div class="useAddress-item">@{{ order.detail_address }}</div>
                                </el-row>
                            </el-form-item>

                            <el-form-item label="作品信息">
                                <div>@{{ order.goods_name }}</div>
                                <div>@{{ order.author }}</div>
                                <div>￥ @{{ order.price }} | @{{ order.size }}</div>
                            </el-form-item>

                            <el-form-item label="备注">
                                <span>
                                    @{{ order.remark }}
                                </span>
                            </el-form-item>

                            <el-form-item label="合计">
                                <div>￥ @{{ order.price }}</div>
                            </el-form-item>

                            <el-form-item>
                                <el-timeline>
                                    <el-timeline-item
                                        v-for="(item, index) in timelines"
                                        :key="index"
                                        :color="timelinesColor"
                                        :timestamp="item.timestamp">
                                        @{{item.content}}
                                    </el-timeline-item>
                                </el-timeline>
                            </el-form-item>

                            <el-form-item>
                                <div>
                                    <el-button @click="applyRefund" class="btn_submit_order btn"
                                               v-if="order.status===1">
                                        申请退款
                                    </el-button>
                                </div>
                                <div>
                                    <el-button @click="delivered" class="btn_submit_order btn"
                                               v-if="order.status===1 && user.is_sale===1">
                                        已发货
                                    </el-button>
                                </div>
                                <el-button @click="applyRefundAndReturn" class="btn_submit_order btn"
                                           v-if="order.status===2">
                                    申请退货退款
                                </el-button>

                                <el-button @click="confirmRefunded" class="btn_submit_order btn"
                                           v-if="order.status===100 && user.is_sale===1 &&  myCreator_id===goodsCreator_id">
                                    确认退款
                                </el-button>

                                <el-button @click="confirmRefundedAndReturn" class="btn_submit_order btn"
                                           v-if="order.status===200 && user.is_sale===1 && myCreator_id===goodsCreator_id">
                                    确认退款退货
                                </el-button>
                            </el-form-item>
                        </el-form>
                    </el-col>
                </el-row>
            </el-col>
        </el-row>
    </section>
@endsection

@section('js')
    <script>
        var app = new Vue({
            el: '#app',
            created() {
                this.setTimelines();
            },
            data() {
                return {
                    user:@json(Auth::user()),
                    order: (@json($data)).order,
                    myCreator_id:(@json($data)).my_creator_id,
                    goodsCreator_id:(@json($data)).goods_creator_id,
                    orderStatus: 3,
                    timelines: [],
                    timelinesColor: '#0bbd87'
                }
            },
            methods: {
                detail() {
                    window.location = '/detail/' + this.order.goods_id;
                },
                //已发货
                delivered(){
                    let data = new FormData();
                    data.append('id',this.order.id);
                    axios.post('/user/myDeal/delivered',data)
                        .then(res=>{
                            if(res.data.status_code===200){
                                app.order.status = 2;
                                app.order.delivery_time = res.data.data;
                                app.setTimelines();
                                app.noticeSuccess(res.data.message);
                            }else{
                                app.noticeWarnings(res.data.error);
                            }
                        })
                        .catch(err=>{
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后再试');
                        })
                },
                //确认退款
                confirmRefunded(){
                    let data = new FormData();
                    data.append('id',this.order.id);
                    axios.post('/user/myDeal/confirmRefunded',data)
                        .then(res=>{
                            app.order.status = 5;
                            app.order.refunded_time = res.data.data;
                            app.setTimelines();
                            app.noticeSuccess(res.data.message);
                        })
                        .catch(err=>{
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后再试');
                        })
                },

                //确认退款退货
                confirmRefundedAndReturn(){
                    let data = new FormData();
                    data.append('id',this.order.id);
                    axios.post('/user/myDeal/confirmRefundedAndReturn',data)
                        .then(res=>{
                            app.order.status = 5;
                            app.order.refunded_return_time = res.data.data;
                            app.setTimelines();
                            app.noticeSuccess(res.data.message);
                        })
                        .catch(err=>{
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后再试');
                        })
                },
                //申请退款
                applyRefund() {
                    this.$confirm('确定要提交此申请吗？')
                        .then(() => {
                            let data = new FormData();
                            data.append('id', this.order.id);
                            data.append('goods_id', this.order.goods_id);
                            axios.post('/order/applyRefund', data)
                                .then(res => {
                                    if (res.data.status_code === 200) {
                                        app.order.status = 100;
                                        app.order.apply_refund_time = res.data.data;
                                        app.setTimelines();
                                        app.noticeSuccess(res.data.message);
                                    } else {
                                        app.noticeWarnings(res.data.error);
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                    app.noticeError('服务器错误，请刷新重试！');
                                })
                        })
                        .catch();
                },
                //申请退货退款
                applyRefundAndReturn() {
                    this.$confirm('确定要提交此申请吗？')
                        .then(() => {
                            let data = new FormData();
                            data.append('id', this.order.id);
                            data.append('goods_id', this.order.goods_id);
                            axios.post('/order/applyRefundAndReturn', data)
                                .then(res => {
                                    if (res.data.status_code === 200) {
                                        app.order.status = 200;
                                        app.order.apply_refund_time = res.data.data;
                                        app.setTimelines();
                                        app.noticeSuccess(res.data.message);
                                    } else {
                                        app.noticeWarnings(res.data.error);
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                    app.noticeError('服务器错误，请刷新重试！');
                                })
                        })
                        .catch();
                },
                //添加支付时间节点
                addTimelineOfPay() {
                    let timeline = {
                        content: '已付款',
                        timestamp: this.order.pay_time
                    };
                    this.timelines.push(timeline);
                    if (this.order.status === 1) {
                        timeline = {
                            content: '等待卖家发货',
                            timestamp: '等待卖家发货'
                        };
                        this.timelines.push(timeline);
                    }
                },
                //添加申请退款时间节点
                addTimelineOfRefund() {
                    let timeline = {
                        content: '申请退款',
                        timestamp: this.order.apply_refund_time
                    };
                    this.timelines.push(timeline);
                    if (this.order.status === 100) {
                        timeline = {
                            content: '等待卖家确认',
                            timestamp: '等待卖家确认'
                        };
                        this.timelines.push(timeline);
                    }
                },
                //添加申请退款完成时间节点
                addTimelineOfRefunded() {
                    let timeline = {
                        content: '已退款',
                        timestamp: this.order.refunded_time
                    };
                    this.timelines.push(timeline);
                },
                //添加发货时间节点
                addTimelineOfDelivery() {
                    let timeline = {
                        content: '卖家已发货',
                        timestamp: this.order.delivery_time
                    };
                    this.timelines.push(timeline);
                    if (this.order.status === 2) {
                        timeline = {
                            content: '等待货物送达',
                            timestamp: '等待货物送达'
                        };
                        this.timelines.push(timeline);
                    }
                },
                //添加申请退货退款时间节点
                addTimelineOfRefundAndReturn() {
                    let timeline = {
                        content: '申请退款退货',
                        timestamp: this.order.apply_refund_return_time
                    };
                    this.timelines.push(timeline);
                    if (this.order.status === 200) {
                        timeline = {
                            content: '等待卖家确认',
                            timestamp: '等待卖家确认'
                        };
                        this.timelines.push(timeline);
                    }
                },

                //添加申请退货退款完成时间节点
                addTimelineOfRefundAndReturnDone() {
                    let timeline = {
                        content: '已退款退货',
                        timestamp: this.order.refunded_return_time
                    };
                    this.timelines.push(timeline);
                },

                //添加交易完成时间节点
                addTimelineOfDone() {
                    let timeline = {
                        content: '交易完成',
                        timestamp: '',
                    };
                    this.timelines.push(timeline);
                },
                //添加交易关闭时间节点
                addTimelineOfClose() {
                    let timeline = {
                        content: '订单关闭',
                        timestamp: ''
                    };
                    this.timelines.push(timeline);
                },

                //设置时间线
                setTimelines() {
                    let item = {
                        content: '提交订单',
                        timestamp: this.order.created_at,
                    };
                    this.timelines = [];
                    this.timelines.push(item);
                    switch (this.order.status) {
                        //已付款
                        case 1:
                            this.addTimelineOfPay();
                            break;
                        //申请退款
                        case 100:
                            this.addTimelineOfPay();
                            this.addTimelineOfRefund();
                            break;
                        //忆发货
                        case 2:
                            this.addTimelineOfPay();
                            this.addTimelineOfDelivery();
                            break;
                        //申请退货退款
                        case 200:
                            this.addTimelineOfPay();
                            this.addTimelineOfDelivery();
                            this.addTimelineOfRefundAndReturn();
                            break;
                        //订单已完成
                        case 4:
                            this.addTimelineOfPay();
                            this.addTimelineOfDelivery();
                            this.addTimelineOfDone();
                            break;
                        //订单已关闭（订单取消，退款完成，退款退货完成）
                        case 5:
                            //是否付款
                            if (this.order.pay_time !== null) {
                                this.addTimelineOfPay();
                                //是否申请退款
                                if (this.order.apply_refund_time !== null) {
                                    console.log(this.order.apply_refund_time);
                                    this.addTimelineOfRefund();
                                    this.addTimelineOfRefunded()
                                } else {
                                    //是否发货
                                    if (this.order.delivery_time !== null) {
                                        this.addTimelineOfDelivery();
                                        this.addTimelineOfRefundAndReturn();
                                    }
                                }
                            }
                            this.addTimelineOfClose();
                            break;
                    }
                },

                //成功通知
                noticeSuccess(message) {
                    this.$notify.success({
                        title: '提示',
                        message: message
                    })
                },
                //错误通知
                noticeError(error) {
                    this.$notify.error({
                        title: '错误',
                        message: error
                    })
                },

                //警告提示
                noticeWarning(message) {
                    this.$notify.warning({
                        title: '提示',
                        message: message
                    })
                },
                //多警告提示
                noticeWarnings(messages) {
                    messages.forEach((item, index) => {
                        setTimeout(function () {
                            this.$notify.warning({
                                title: '提示',
                                message: item
                            })
                        }, 1);
                    });
                }

            }
        })

    </script>
@endsection
