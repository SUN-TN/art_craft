@extends('layout.master')

@section('title')
    发 现-Craft
@endsection
@section('link')
    <link rel="stylesheet" href="{{asset('/css/show/eyes.css')}}">
{{--    <script src="{{asset('/js/masonry.pkgd.min.js')}}"></script>--}}
{{--    <script src="{{asset('/js/imagesloaded.pkgd.min.js')}}"></script>--}}
@endsection

@section('content')
    <section id="app">
        <el-row class="filter-box" :gutter="50">
            <el-col :span="10" :offset="2">
                <div class="genre">
                    <el-radio-group v-model="genreRadio">
                        <el-radio-button v-for="(item,index) in genres"
                                         :key="item.id" :label="item.genre">
                        </el-radio-button>
                    </el-radio-group>
                </div>
            </el-col>
            <el-col :span="10">
                <div class="block">
                    <el-slider
                        v-model="sliderValue"
                        range
                        show-stops
                        :show-tooltip="false"
                        :step="2000"
                        :max="sliderMax"
                        :marks="marks">
                    </el-slider>
                </div>
            </el-col>
        </el-row>

        <el-row style="margin-top: 20px">
            <el-col :span="12" :offset="6">
                <div class="search">
                    <el-input placeholder="作品名称" v-model="searchText">
                        <el-button slot="append" icon="el-icon-search btn_search" @click="search"></el-button>
                    </el-input>
                </div>
            </el-col>
        </el-row>
        <el-divider></el-divider>

        <el-row>
            <el-col :span="18" :offset="3">
                <div id="masonry" class="masonry" style="height: auto !important; opacity: 0">
                    <div class="masonry-item" id="masonry-item" v-for="(item,index) in goods" :key="item.id">
                        <el-image class="crafts-item-img" :src="'{{asset('/')}}'+item.imgUrl"
                                  @click="detail(item)"></el-image>
                        <div class="crafts-item-info">
                            <div class="crafts-item-name">@{{item.name}}</div>
                            <div class="crafts-item-author">@{{item.author}}</div>
                            <div class="crafts-item-Size">￥@{{item.price}} | @{{item.size}}</div>
                        </div>
                    </div>

                    <div v-show="goods === null || goods.length === 0"
                         style="width: 100%;text-align: center;margin-top: 50px;font-size: 2em;"
                         class="noData">
                        暂无数据
                    </div>
                </div>
            </el-col>
        </el-row>

        <el-row class="pagination-row">
            <el-col :span="18" :offset="3" style="text-align: center">
                <el-pagination
                    class="pagination"
                    background
                    layout="prev, pager, next"
                    :page-size="pageSize"
                    :total="count"
                    :current-page.sync="currentPage"
                    @current-change="currentChange">
                </el-pagination>
            </el-col>
        </el-row>
    </section>

@endsection


@section('js')
    <script>
        var app = new Vue({
            el: '#app',
            created() {
                this.genres.unshift({'genre': '所有'});
            },
            data() {
                return {
                    sliderMax: 20000,
                    sliderValue: [0, 20000],
                    marks: {
                        0: 'min', 2000: '2000', 4000: '4000', 6000: '6000', 8000: '8000', 10000: '10000',
                        12000: '12000', 14000: '14000', 16000: '16000', 18000: '18000', 20000: 'max',
                    },
                    genreRadio: '所有',
                    genres:@json($data).genres,
                    searchText: '',
                    goods:@json($data).goods,
                    count:@json($data).count,
                    pageSize: 20,
                    currentPage:1,

                    currentFilter: {
                        min: 0,
                        max: 'max',
                        genre_id: 'all',
                        searchText: '',
                    }
                }
            },
            methods: {
                //根据筛选条件加载当前页数据
                currentChange(currentPage) {
                    let ft=this.currentFilter;
                    let params =currentPage+'/'+ft.min+'/'+ft.max+'/'+ft.genre_id+'/'+ft.searchText;
                    axios.get('/eyes/newPage/' + params)
                        .then(res => {
                            app.goods = [];
                            app.count=res.data.data.count;
                            res.data.data.goods.forEach((item, index) => {
                                app.goods.push(item);
                            });
                            app.$nextTick(executeMyMasonry());
                            setTimeout(function () {
                                app.$nextTick(up());
                            }, 500);
                        })
                        .catch(err => {
                            app.$notify.error('服务器繁忙，请稍后再试！');
                        })
                },
                //分析设置当前筛选条件 并加载当前筛选条件第一页数据
                search(){
                    //分析设置价格筛选条件
                    this.currentFilter.min=this.sliderValue[0];
                    if(this.sliderValue[1]===this.sliderMax){
                        this.currentFilter.max='max'
                    }else{
                        this.currentFilter.max=this.sliderValue[1];
                    }

                    //分析设置分类筛选条件
                    if(this.genreRadio==='所有'){
                        this.currentFilter.genre_id='all';
                    }else{
                        for(let i=0;i<this.genres.length;i++){
                            if(this.genres[i].genre===this.genreRadio){
                                this.currentFilter.genre_id=this.genres[i].id;
                                console.log(this.genres[i].id);
                                break;
                            }
                        }
                    }

                    //设置模糊查询搜索作品名
                    this.currentFilter.searchText=this.searchText;
                    //设置当前页为第1页
                    this.currentPage=1;
                    //加载当前筛选条件下第一页数据
                    this.currentChange(1);
                },
                detail(item) {
                    let url='{{asset('/detail')}}'+'/'+item.id;
                    window.open(url, "_blank");
                },
            }
        });




        let finalH1 = 0;
        let finalH2 = 0;
        let finalH3 = 0;
        let finalH4 = 0;
        let topH = 80;
        //根据每个item高度设置瀑布布局
        var myMasonry = function () {
            let h1 = 80;
            let h2 = 80;
            let h3 = 80;
            let h4 = 80;
            setTimeout(function () {
                $(".masonry-item").each(function (index, el) {
                    if (h1 <= h2 && h1 <= h3 && h1 <= h4) {
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item1");
                        $(this).css({"top": h1 + "px", 'opacity': 1});
                        h1 += $(this).height() + topH;
                    } else if (h2 < h1 && h2 <= h3 && h2 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item2");
                        $(this).css({"top": h2 + "px", 'opacity': 1});
                        h2 += $(this).height() + topH;
                    } else if (h3 < h1 && h3 < h2 && h3 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item4');
                        $(this).addClass("item3");
                        $(this).css({"top": h3 + "px", 'opacity': 1});
                        h3 += $(this).height() + topH;
                    } else {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).addClass("item4");
                        $(this).css({"top": h4 + "px", 'opacity': 1});
                        h4 += $(this).height() + topH;
                    }
                });
                setHeight();
                // adjustMasonry();
            }, 300)
        };
        var setHeight = function () {
            finalH1 = 0;
            finalH2 = 0;
            finalH3 = 0;
            finalH4 = 0;
            $('.item1').each(function () {
                finalH1 += $(this).height() + topH;
            });
            $('.item2').each(function () {
                finalH2 += $(this).height() + topH;
            });
            $('.item3').each(function () {
                finalH3 += $(this).height() + topH;
            });
            $('.item4').each(function () {
                finalH4 += $(this).height() + topH;
            });
            let maxH = Math.max(finalH1, finalH2, finalH3, finalH4) + 80;
            $(".masonry").css({"height": (maxH + topH) + "px"});
        };

        function executeMyMasonry() {
            $('#masonry').css({"transition": "0s", "opacity": 0});
            myMasonry();
            setTimeout(function () {
                $('#masonry').css({"transition": '1.5s', "opacity": 1});
            }, 200);
        }

        //窗口大小改变后重新设置瀑布布局
        window.onresize = function () {
            myMasonry();
        };

        //窗口滚动时重新设置瀑布布局
        $(document).scroll(function () {
            let scrollTop = $(document).scrollTop;
            //每滚动200px就对瀑布布局进行设置
            if ((scrollTop % 350) > 300 || (scrollTop >= 0 && scrollTop < 10)) {
                myMasonry();
            }
        });

        $(document).ready(function () {
            setTimeout(function () {
                executeMyMasonry();
            },200);
            setTimeout(function () {
                myMasonry();
            },400)
        });
    </script>
@endsection
