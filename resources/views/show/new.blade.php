@extends('layout.master')

@section('title')
    新品上架-Craft
@endsection
@section('link')
    <link rel="stylesheet" href="{{asset('/css/show/new.css')}}">
{{--    <script src="{{asset('/js/masonry.pkgd.min.js')}}"></script>--}}
{{--    <script src="{{asset('/js/imagesloaded.pkgd.min.js')}}"></script>--}}
@endsection

@section('content')
    <section id="app">
        <el-row>
            <el-col :span="18" :offset="3">
                <div id="masonry" class="masonry" style="height: auto !important; opacity: 0">
                    <div class="masonry-item" id="masonry-item" v-for="(item,index) in showGoods" :key="item.id">
                        <el-image class="crafts-item-img" :src="'{{asset('/')}}'+item.imgUrl"
                                  @click="detail(item)"></el-image>
                        <div class="crafts-item-info">
                            <div class="crafts-item-name">@{{item.name}}</div>
                            <div class="crafts-item-author">@{{item.author}}</div>
                            <div class="crafts-item-Size">￥@{{item.price}} | @{{item.size}}</div>
                        </div>
                    </div>
                    <div v-show="showGoods.length<=0"
                         style="width: 100%;text-align: center;margin-top: 50px;font-size: 2em;"
                         class="noData">
                        暂无数据
                    </div>
                </div>
            </el-col>
        </el-row>

        <el-row class="pagination-row">
            <el-col :span="18" :offset="3" style="text-align: center">
                <el-pagination
                    class="pagination"
                    background
                    layout="prev, pager, next"
                    :page-size="pageSize"
                    :total="count"
                    @current-change="currentChange">
                </el-pagination>
            </el-col>
        </el-row>
    </section>



@endsection


@section('js')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    showGoods: @json($data).newGoods,
                    pageSize: 20,
                    count:@json($data).count,
                }
            },
            methods: {
                currentChange(currentPage) {
                    axios.get('/new/newPage/' + currentPage)
                        .then(res => {
                            this.showGoods = [];
                            res.data.data.forEach((item, index) => {
                                this.showGoods.push(item);
                            });
                            app.$nextTick(executeMyMasonry());
                            setTimeout(function () {
                                app.$nextTick(up());
                            }, 500);
                        })
                        .catch(err => {
                            app.$notify.error('服务器繁忙，请稍后再试！');
                        })
                },
                detail(item) {
                    let url = '{{asset('/detail')}}' + '/' + item.id;
                    window.open(url, "_blank");
                    // window.location = '/detail/' + item.id;
                },
            },
        });


        let finalH1 = 0;
        let finalH2 = 0;
        let finalH3 = 0;
        let finalH4 = 0;
        let topH = 80;
        //根据每个item高度设置瀑布布局
        var myMasonry = function () {

            let h1 = 80;
            let h2 = 80;
            let h3 = 80;
            let h4 = 80;
            setTimeout(function () {
                $(".masonry-item").each(function (index, el) {
                    if (h1 <= h2 && h1 <= h3 && h1 <= h4) {
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item1");
                        $(this).css({"top": h1 + "px", 'opacity': 1});
                        h1 += $(this).height() + topH;
                    } else if (h2 < h1 && h2 <= h3 && h2 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item2");
                        $(this).css({"top": h2 + "px", 'opacity': 1});
                        h2 += $(this).height() + topH;
                    } else if (h3 < h1 && h3 < h2 && h3 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item4');
                        $(this).addClass("item3");
                        $(this).css({"top": h3 + "px", 'opacity': 1});
                        h3 += $(this).height() + topH;
                    } else {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).addClass("item4");
                        $(this).css({"top": h4 + "px", 'opacity': 1});
                        h4 += $(this).height() + topH;
                    }
                });
                setHeight();
                // adjustMasonry();
            }, 300)
        };


        // var adjustMasonry = function () {
        //     // for (let adjustTimes = 0; adjustTimes < 1; adjustTimes++) {
        //         setHeight();
        //         ///////////////////////////---------------
        //         let hMax = Math.max(finalH1, finalH2, finalH3, finalH4);  //最高列总高度
        //         let hMin = Math.min(finalH1, finalH2, finalH3, finalH4);  //最低列总高度
        //         let maxClass = '';         //最高列class
        //         let minClass = '';         //最低列class
        //         let maxDistance = 400;  //最大高度差
        //         //如果超过就对布局进行优化
        //         if ((hMax - hMin) >= maxDistance) {
        //             //获取最高列class
        //             switch (hMax) {
        //                 case finalH1:
        //                     maxClass = '.item1';
        //                     break;
        //                 case finalH2:
        //                     maxClass = '.item2';
        //                     break;
        //                 case finalH3:
        //                     maxClass = '.item3';
        //                     break;
        //                 case finalH4:
        //                     maxClass = '.item4';
        //                     break;
        //             }
        //             //获取最低列class
        //             switch (hMin) {
        //                 case finalH1:
        //                     minClass = '.item1';
        //                     break;
        //                 case finalH2:
        //                     minClass = '.item2';
        //                     break;
        //                 case finalH3:
        //                     minClass = '.item3';
        //                     break;
        //                 case finalH4:
        //                     minClass = '.item4';
        //                     break;
        //             }
        //
        //             //最低列元素个数大于0
        //             if ($(minClass).length > 0) {
        //                 let tryTimes = 1;
        //                 //最高列最后一个元素项
        //                 let lastItemOfMax = $($(maxClass)[$(maxClass).length - 1]);
        //                 //最高列最后一个元素项高度
        //                 let lastItemHeightOfMax = lastItemOfMax.height();
        //                 let newHMax = hMax;  //记录原最高列如果重新布局的新高度
        //                 let newHMin = hMin;  //记录原最低列如果重新布局的新高度
        //                 let newDistance = 0; //如果移动元素后的新高度差
        //
        //                 let maxItemNum = $(maxClass).length;//最高列元素个数
        //                 let moveItemIndex = 0;
        //                 for (let i = 1; i < maxItemNum; i++) {
        //                     let currIndex = maxItemNum - i - 1;
        //                     let currItem = $($(maxClass)[currIndex]);
        //                     newHMax -= currItem.height();  //记录原最高列如果重新布局的新高度
        //                     newHMin += currItem.height();  //记录原最低列如果重新布局的新高度
        //                     newDistance = Math.abs(newHMax - newHMin); //取新高度差绝对值
        //                     //如果高度差在可接受高度之内  则对元素进行移动重新布局
        //                     if (newDistance < maxDistance) {
        //                         moveItemIndex = maxItemNum - (currIndex) + 1;
        //                         let moveItemH = currItem.height();
        //                         currItem.removeClass(maxClass.replace('.', ''));
        //                         currItem.addClass(minClass.replace('.', ''));
        //                         maxItemNum--;
        //                         let currMinTop = hMin + topH;
        //                         currItem.css({'top': currMinTop + 'px'});
        //                         console.log(1);
        //                         for (let j = moveItemIndex; j < maxItemNum; j++) {
        //                             console.log(j+'1');
        //                             let item = $($(maxClass)[j]);
        //                             let itemTop;
        //                             if(j===moveItemIndex){
        //                                 itemTop=item.position().top-moveItemH-topH;
        //                             }else{
        //                                 itemTop=item.position().top-moveItemH;
        //                             }
        //                             item.css({
        //                                 'top': itemTop + 'px'
        //                             });
        //                         }
        //                         setHeight();
        //                         break;
        //                     } else {
        //                         newHMax += currItem.height();   //恢复原最高列高度
        //                         newHMin -= currItem.height();   //恢复原最高列高度
        //                     }
        //                 }
        //             }
        //         }
        //     // }
        //
        // };
        let setHeight = function () {
            finalH1 = 0;
            finalH2 = 0;
            finalH3 = 0;
            finalH4 = 0;
            $('.item1').each(function () {
                finalH1 += $(this).height() + topH;
            });
            $('.item2').each(function () {
                finalH2 += $(this).height() + topH;
            });
            $('.item3').each(function () {
                finalH3 += $(this).height() + topH;
            });
            $('.item4').each(function () {
                finalH4 += $(this).height() + topH;
            });
            let maxH = Math.max(finalH1, finalH2, finalH3, finalH4) + 80;
            $(".masonry").css({"height": (maxH + topH) + "px"});
        };

        //
        function executeMyMasonry() {
            $('#masonry').css({"transition": "0s", "opacity": 0});
            myMasonry();
            setTimeout(function () {
                $('#masonry').css({"transition": '1.5s', "opacity": 1});
            }, 200);
        }

        //窗口大小改变后重新设置瀑布布局
        window.onresize = function () {
            myMasonry();
        };

        //窗口滚动时重新设置瀑布布局
        $(document).scroll(function () {
            let scrollTop = $(document).scrollTop;
            //每滚动200px就对瀑布布局进行设置
            if ((scrollTop % 350) > 300 || (scrollTop >= 0 && scrollTop < 10)) {
                myMasonry();
            }
        });

        $(document).ready(function () {
            setTimeout(function () {
                executeMyMasonry();
            }, 200);
            // setTimeout(function () {
            //     myMasonry();
            // }, 400)
        });

    </script>
@endsection
