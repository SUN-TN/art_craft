<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>作品详情</title>
    <link rel="icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="{{asset('/elementUI/index.js')}}"></script>
    <script src="{{asset('/js/axios.min.js')}}"></script>
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/elementUI/index.css')}}">
    <link rel="stylesheet" href="{{asset('/css/show/detail.css')}}">
</head>
<body>
<section id="app">
    <el-page-header @back="goBack" :content="craft.name+'—'+craft.author" class="go-back">
    </el-page-header>

    <el-row :gutter="50" style="padding-top: 50px" class="craft-box">
        <el-col :span="10" :offset="3">
            <el-tooltip class="item" effect="dark" content="点击查看大图" placement="left">
                <div class="demo-image__preview">
                    <el-image
                        style="width: 100%; height: auto"
                        :src="'{{asset('/')}}'+craft.imgUrl"
                        :preview-src-list="srcList">
                    </el-image>
                </div>
            </el-tooltip>
        </el-col>

        <el-col :span="8" :offset="1" class="info-item-box">
            <el-row :gutter="50" class="info-item-row info">
                <el-col :span="12">
                    <span>@{{ craft.name }}</span>
                    <el-divider direction="vertical"></el-divider>
                    <span>
                        <el-tag style="background-color: #2a3237;border: none" effect="dark">@{{ craft.genre }}</el-tag>
                    </span>
                </el-col>
                <el-col :span="10" :offset="2">@{{ craft.author }}</el-col>
            </el-row>
            <el-row :gutter="50" class="info-item-row info">
                <el-col :span="12">￥ @{{ craft.price }}</el-col>
                <el-col :span="10" :offset="2">@{{ craft.size }}</el-col>
            </el-row>
            <el-row :gutter="50" class="info-item-row info">
                <el-col :span="22">@{{ craft.intro }}</el-col>
            </el-row>
            <el-row :gutter="50" class="info-item-row">
                <el-col :span="10">
                    <el-button v-if="isStar" class="el-icon-star-on" @click="removeStar">移出收藏</el-button>
                    <el-button v-else class="el-icon-star-off" @click="addStar">加入收藏</el-button>
                </el-col>
                <el-col :span="10" :offset="4">
                    <el-button v-if="inCart" class="el-icon-remove" @click="removeCart">移出购物车</el-button>
                    <el-button v-else class="el-icon-circle-plus" @click="addCart">加入购物车</el-button>
                </el-col>
            </el-row>
            <el-row class="info-item-row">
                <el-button class="el-icon-shopping-cart-1" @click="buy">购买</el-button>
            </el-row>
        </el-col>

    </el-row>

    <section class="guess-like-section">
        <div class="title">猜你喜欢</div>
        <el-row class="guess-like-row">
            <el-col :span="18" :offset="3">
                <el-row :gutter="30">
                    <el-col :span="6" class="guess-like-box" v-for="(item,index) in likeRow1" :key="item.id">
                        <div>
                            <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                            <div class="img-info" @click="detail(item)">
                                <div class="img-info-item">@{{ item.author }}</div>
                                <div class="img-info-item">@{{ item.name }}</div>
                                <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                            </div>
                        </div>
                    </el-col>
                </el-row>

                <el-row class="guess-like-row" :gutter="30">
                    <el-col :span="6" class="guess-like-box" v-for="(item,index) in likeRow2" :key="item.id">
                        <div>
                            <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                            <div class="img-info" @click="detail(item)">
                                <div class="img-info-item">@{{ item.author }}</div>
                                <div class="img-info-item">@{{ item.name }}</div>
                                <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                            </div>
                        </div>
                    </el-col>
                </el-row>

            </el-col>
        </el-row>


    </section>

</section>

<script>
    let app = new Vue({
        el: "#app",
        created() {
            this.srcList.push('{{asset('')}}' + this.craft.imgUrl);
            for (let i = 0; i < this.like.length; i++) {
                if (i < this.like.length / 2) {
                    this.likeRow1.push(this.like[i]);
                } else {
                    this.likeRow2.push(this.like[i]);
                }
            }
        },
        data() {
            return {
                craft:@json($data).craft,
                like:@json($data).like,
                likeRow1: [],
                likeRow2: [],
                srcList: [],
                isStar:@json($data).isStar,
                inCart:@json($data).inCart,
            }
        },
        methods: {
            goBack() {
                history.back(-1);
            },
            //添加收藏
            addStar() {
                let data = new FormData();
                data.append('id', this.craft.id);
                axios.post('/detail/addStar', data)
                    .then(res => {
                        if (res.data.status_code === 200) {
                            this.isStar = true;
                            this.noticeSuccess(res.data.message);
                        } else {
                            this.noticeWarnings(res.data.error);
                        }
                    })
                    .catch(err => {
                        this.noticeError('服务器繁忙，请稍后再试！');
                    })
            },
            //移除收藏
            removeStar() {
                let data = new FormData();
                data.append('id', this.craft.id);
                axios.post('/detail/removeStar', data)
                    .then(res => {
                        if (res.data.status_code === 200) {
                            this.isStar = false;
                            this.noticeSuccess(res.data.message);
                        } else {
                            this.noticeWarnings(res.data.error);
                        }
                    })
                    .catch(err => {
                        this.noticeError('服务器繁忙，请稍后再试！');
                    })
            },
            //加入购物车
            addCart() {
                let data = new FormData();
                data.append('id', this.craft.id);
                axios.post('/detail/addCart', data)
                    .then(res => {
                        if (res.data.status_code === 200) {
                            this.inCart = true;
                            this.noticeSuccess(res.data.message);
                        } else {
                            this.noticeWarnings(res.data.error);
                        }
                    })
                    .catch(err => {
                        this.noticeError('服务器繁忙，请稍后再试！');
                    })
            },
            //移出购物车
            removeCart() {
                let data = new FormData();
                data.append('id', this.craft.id);
                axios.post('/detail/removeCart', data)
                    .then(res => {
                        if (res.data.status_code === 200) {
                            this.inCart = false;
                            this.noticeSuccess(res.data.message);
                        } else {
                            this.noticeWarnings(res.data.error);
                        }
                    })
                    .catch(err => {
                        this.noticeError('服务器繁忙，请稍后再试！');
                    })
            },
            buy() {
                window.location='/order/create/'+this.craft.id;
            },
            detail(item) {
                window.location = '/detail/' + item.id;
            },
            noticeSuccess(message) {
                this.$notify.success({
                    title: '提示',
                    message: message
                });
            },
            noticeError(message) {
                this.$notify.error({
                    title: '错误',
                    message: message,
                })
            },
            noticeWarnings(message) {
                message.forEach((item, index) => {
                    setTimeout(function () {
                        app.$notify.warning({
                            title: '提示',
                            message: item,
                        });
                    }, 1);
                });
            },
        }
    });

    $(document).ready(function () {
        setTimeout(function () {
            let boxH = $('.craft-box').height();
            let infoBoxH = $('.info-item-box').height();
            let pd = boxH - infoBoxH;
            $('.info-item-box').css({'padding-top': pd + 'px', 'transition': '0.5s'});
        }, 200);

        function img_info() {
            $('.goods_img').on({
                'mouseenter': function () {
                    $(this).next().show();
                },
                "mouseleave": function () {
                    $(this).next().hide();
                }
            });
            $(".img-info").on({
                'mouseenter': function () {
                    $(this).show();
                },
                "mouseleave": function () {
                    $(this).hide();
                }
            });
        }
        img_info();
    });
</script>

</body>
</html>