@extends('admin.layout.master')

@section('title')
    作品信息管理-作品管理
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('css/admin/goods.css')}}">
@endsection

@section('content')
    <el-row class="topBox">
        <el-col :md="3" :sm="3" :xs="3">
            <p class="title">作品列表</p>
        </el-col>
        <el-col :sm="12" :xs="12" :md="12" :offset="3">
            <el-row>
                <el-col :md="18">
                    <el-input v-model="searchText" placeholder="请输入要搜索的作品名称" style="padding: 0">
                        <el-select slot="prepend" v-model="searchGenre" clearable placeholder="分类" style="width: 100px">
                            <el-option v-for="item in searchGenres" :key="item.id" :label="item.genre" :value="item.id">
                            </el-option>
                        </el-select>
                        <el-button slot="append" type="primary" icon="el-icon-search"
                                   style="color: white;background-color: #66B1FF;border-radius: 0px"
                                   @click="search">搜索
                        </el-button>
                    </el-input>
                </el-col>
            </el-row>
        </el-col>
        <el-col :md="2" :sm="2" :xs="2" :offset="4">
            <el-button type="primary" class="btn_add" @click="addDialogVisible = true">添加商品</el-button>
        </el-col>
    </el-row>

    <el-row class="list">
        <el-col :md="24" :sm="24" :xs="24">
            {{--list table--}}
            <el-table style="width: 100%" :height="tableHeight" :data="showGoods">
                <el-table-column label="编号" width="55">
                    <template slot-scope="scope">
                        <div class="rowNumber">
                            <div>@{{ scope.$index+1 }}</div>
                        </div>
                    </template>
                </el-table-column>

                <el-table-column label="作品" width="100" style="position: relative">
                    <template slot-scope="scope">
                        <div style="position: relative">
                        <el-image class="goods_img" style="width: 100px; height:90px;" :src="wwwPath+scope.row.imgUrl"
                                  fit="cover">
                        </el-image>
                        <div class="img_view" @click="goodsPictureCardPreview(scope.row.imgUrl)">
                            <i class="el-icon-zoom-in" style="font-size: 25px"></i>
                        </div>
                        </div>
                    </template>
                </el-table-column>

                <el-table-column label="名称" width="160">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.name }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="作者" width="80">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.author }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="类型" width="100">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.genre }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="价格" width="100">
                    <template slot-scope="scope">
                        <span>￥@{{ scope.row.price }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="尺寸" width="140">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.size }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="简介" width="270">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.intro }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="卖家" width="80">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.creator_name }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="操作" style="min-width: 300px">
                    <template slot-scope="scope">

                        <el-button size="mini" slot="reference" @click="openEditDialog(scope.$index, scope.row)">编辑
                        </el-button>

                        <el-button size="mini" type="warning" slot="reference" @click="openTakenOffDialog(scope)">下架
                        </el-button>

                        <el-popover placement="top" width="160" :ref="'popover-'+scope.$index">
                            <p>确定删除吗？</p>
                            <div style="text-align: right; margin: 0">
                                <el-button size="mini" type="text" @click="closeDelPopover(scope)">取消</el-button>
                                <el-button type="primary" size="mini" @click="delConfirm(scope)">确定</el-button>
                            </div>
                            <el-button slot="reference" size="mini" type="danger">删除</el-button>
                        </el-popover>

                    </template>
                </el-table-column>
            </el-table>
            {{--分页--}}
            <div class="pagination">
                <el-pagination background layout="prev, pager, next"
                               :page-size="pageSize" :total="goodsCount"
                               :current-page.sync="currentPage"
                               @current-change="handleCurrentChange">
                </el-pagination>
            </div>
        </el-col>
    </el-row>

    {{--下架原因--}}
    <el-dialog title="下架原因" :modal="false" :visible.sync="reasonDialogVisible" width="30%">
        <el-form :rules="rules" :model="reasons" ref="reason">
            <el-form-item label="" label-width="200" prop="reason">
                <el-input v-model="reasons.reason" placeholder="请请填写下架原因" type="textarea"></el-input>
            </el-form-item>
        </el-form>
        <div slot="footer" class="dialog-footer">
            <el-button @click="reasonDialogVisible = false">取 消</el-button>
            <el-button type="primary" @click="takeOff">确 定</el-button>
        </div>
    </el-dialog>

    {{--添加作品--}}
    <el-dialog title="添加作品" :visible="addDialogVisible" width="30%" top="1vh" :show-close="false">
        <el-form label-position="right" label-width="80px" :model="formGoods"
                 :rules="rules" :status-icon="true"
                 ref="addGoods" :hide-required-asterisk="true">

            <el-form-item label="名称" prop="name">
                <el-input v-model="formGoods.name"></el-input>
            </el-form-item>
            <el-form-item label="作者" prop="author">
                <el-input v-model="formGoods.author"></el-input>
            </el-form-item>
            <el-form-item label="类型" prop="genre_id">
                <el-select v-model="formGoods.genre_id" placeholder="请选择分类" style="width: 100%" filterable>
                    <el-option v-for="item in genres" :key="item.id" :label="item.genre" :value="item.id">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item label="尺寸" prop="length">
                <el-input v-model.number="formGoods.length">
                    <template slot="prepend">长</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="width">
                <el-input v-model.number="formGoods.width">
                    <template slot="prepend">宽</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="height">
                <el-input v-model.number="formGoods.height">
                    <template slot="prepend">高</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="价格" prop="price">
                <el-input v-model="formGoods.price">
                    <template slot="prepend">￥</template>
                </el-input>
            </el-form-item>
            <el-form-item label="商家" prop="creator_id">
                <el-select v-model="formGoods.creator_id" placeholder="请选择商品所属创作者" style="width: 100%" filterable>
                    <el-option v-for="item in creators" :key="item.id" :label="item.creator_name" :value="item.id">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item label="简介" prop="intro">
                <el-input type="textarea" v-model="formGoods.intro"></el-input>
            </el-form-item>

            <el-form-item label="上传图片">
                <el-upload ref="add-upload"
                           action="123"
                           :auto-upload="false"
                           {{--是否支持多选--}}
                           :multiple="false"
                           {{--list-type--文件列表展示方式--}}
                           list-type="picture-card"
                           {{--接收的参数类型--}}
                           ccept=".jpg,.png"
                           {{--文件状态改变时的钩子，添加文件、上传成功和上传失败时都会被调用--}}
                           :on-change="handleFileChange"
                           :on-preview="handlePictureCardPreview"
                           :on-remove="handleRemove"
                           {{--覆盖默认上传事件--}}
                           :http-request="upload">
                    <i class="el-icon-plus"></i>
                    <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且大小不超过2MB</div>
                </el-upload>
            </el-form-item>
            <el-dialog :modal="false" :visible.sync="dialogVisible">
                <img width="100%" :src="dialogImageUrl" alt="">
            </el-dialog>
        </el-form>
        <span slot="footer" class="dialog-footer">
        <el-button @click="closeAddDialog">关 闭</el-button>
        <el-button type="primary" @click="handleSubmit">提 交</el-button>
    </span>
    </el-dialog>

    {{--编辑修改作品信息--}}
    <el-dialog title="修改作品" :visible="editDialogVisible" width="30%" top="1vh" :show-close="false">
        <el-form label-position="right" label-width="80px" :model="editGoods"
                 :rules="rules" :status-icon="true"
                 ref="editGoods" :hide-required-asterisk="true">

            <el-form-item label="名称" prop="name">
                <el-input v-model="editGoods.name"></el-input>
            </el-form-item>
            <el-form-item label="作者" prop="author">
                <el-input v-model="editGoods.author"></el-input>
            </el-form-item>
            <el-form-item label="类型" prop="genre_id">
                <el-select v-model="editGoods.genre_id" placeholder="请选择分类" style="width: 100%">
                    <el-option v-for="item in genres" :key="item.id" :label="item.genre" :value="item.id">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item label="尺寸" prop="length">
                <el-input v-model.number="editGoods.length">
                    <template slot="prepend">长</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="width">
                <el-input v-model.number="editGoods.width">
                    <template slot="prepend">宽</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="height">
                <el-input v-model.number="editGoods.height">
                    <template slot="prepend">高</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="价格" prop="price">
                <el-input v-model="editGoods.price">
                    <template slot="prepend">￥</template>
                </el-input>
            </el-form-item>
            <el-form-item label="商家" prop="creator_id">
                <el-select v-model="editGoods.creator_id" placeholder="请选择商品所属创作者" style="width: 100%">
                    <el-option v-for="item in creators" :key="item.id" :label="item.creator_name" :value="item.id">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item label="简介" prop="intro">
                <el-input type="textarea" v-model="editGoods.intro"></el-input>
            </el-form-item>

            <el-form-item label="修改图片">
                <el-upload ref="edit-upload"
                           action="/admin/goods/changeImg/"
                           :auto-upload="true"
                           {{--是否支持多选--}}
                           :multiple="false"
                           :show-file-list="false"
                           ccept=".jpg,.png"
                           {{--文件状态改变时的钩子，添加文件、上传成功和上传失败时都会被调用--}}
                           :on-change="editFileChange"
                           :on-preview="handlePictureCardPreview"
                           :on-remove="handleRemove"
                           :http-request="changeImg">
                    <el-image
                        :src="wwwPath+editGoods.imgUrl"
                        fit="fit"
                        class="changeGoodsImg" id="changeGoodsImg">
                    </el-image>
                    <el-avatar
                        {{--v-Show="false"--}}
                        class="tip_changeGoodsImg"
                        id="tip_changeGoodsImg"
                        :size="140"
                        style="font-size: 1.5em;">修改图片
                    </el-avatar>
                    <div slot="tip" class="el-upload__tip" style="margin-top: 100px;">只能上传jpg/png文件，且大小不超过2MB</div>
                </el-upload>
            </el-form-item>
            <el-dialog :modal="false" :visible.sync="editImgDialogVisible">
                <img width="100%" :src="wwwPath+editGoods.imgUrl" alt="">
            </el-dialog>
        </el-form>
        <span slot="footer" class="dialog-footer">
        <el-button @click="closeEditDialog">关 闭</el-button>
        <el-button type="primary" @click="editSubmit">提 交</el-button>
    </span>
    </el-dialog>

    {{--查看大图--}}
    <el-dialog :modal="false" :visible.sync="currentImgDialogVisible">
        <img width="100%" :src="currentDialogImageUrl" alt="">
    </el-dialog>

@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    el_menu: {
                        openIndex: '1',
                        active: '1-1',
                    },
                    searchGenre: 'all', //搜索选择分类
                    searchText: '', //搜索文本

                    thisSearchGenre_id: '',
                    thisSearchText: '',

                    goods: (@json($data)).goods, //从数据库累积加载的所有作品数据
                    goodsCount: (@json($data)).goodsCount, //数据库数据总条数
                    showGoods: [], //当前页显示数据

                    genres: (@json($data)).genres, //分类
                    creators: (@json($data)).creators, //创作者 卖家
                    searchGenres: (@json($data)).genres, //分类

                    pageSize: 10, //每页显示条数
                    currentPage: 1, //当前页码

                    addDialogVisible: false, //新增 Dialog
                    formGoods: {}, //新增 goods

                    editGoods: {},
                    editDialogVisible: false,
                    editImgDialogVisible: false,

                    dialogImageUrl: '', //新增  显示大图 url
                    dialogVisible: false,//新增显示大图  Visible

                    //列表当前goods
                    currentImgDialogVisible: false, //大图Dialog
                    currentDialogImageUrl: '',
                    currentIndex: null,
                    currentRow: null,

                    reasonDialogVisible: false, //下架原因 Dialog
                    reasons: {
                        'reason': ''
                    },

                    wwwPath: '{{asset('/')}}',
                    mainHeight: 800,

                    rules: {
                        reason: [{
                            required: true,
                            message: '请输入下架回理由',
                            trigger: ['blur', 'change']
                        },
                            {
                                min: 5,
                                max: 255,
                                message: '长度在5到255个字符之间',
                                trigger: ['blur', 'change']
                            },
                        ],
                        name: [{
                            required: true,
                            message: '请输入作品名称',
                            trigger: ['blur', 'change']
                        },
                            {
                                min: 1,
                                max: 32,
                                message: '长度在1到32个字符之间',
                                trigger: ['blur', 'change']
                            },
                        ],
                        author: [{
                            required: true,
                            message: '请输入作者名称',
                            trigger: ['blur', 'change']
                        },
                            {
                                min: 1,
                                max: 32,
                                message: '长度在1到32个字符之间',
                                trigger: ['blur', 'change']
                            },
                        ],
                        length: [{
                            required: true,
                            message: '请输入作品长度',
                            trigger: ['blur', 'change']
                        },
                            {
                                type: 'number',
                                message: '只能输入数字',
                                trigger: 'change'
                            },

                        ],
                        width: [{
                            required: true,
                            message: '请输入作品宽度',
                            trigger: ['blur', 'change']
                        },
                            {
                                type: 'number',
                                message: '只能输入数字',
                                trigger: 'change'
                            },
                        ],
                        height: [
                            {required: false, message: '请输入作品高度', trigger: ['blur', 'change']},
                            {pattern: /[^\.\D]/g, message: '只能输入数字', trigger: ['blur', 'change']}
                        ],
                        price: [{
                            required: true,
                            message: '请输入作品价格',
                            trigger: ['blur', 'change']
                        },
                            {
                                pattern: /^(?!0+$)(?!0*\.0*$)\d{1,8}(\.\d{1,2})?$/,
                                message: '只能输入数字,且只能两位小数',
                                trigger: ['blur', 'change']
                            },
                        ],
                        intro: [{
                            required: true,
                            message: '请输入作品简介',
                            trigger: ['blur', 'change']
                        },
                            {
                                min: 10,
                                max: 100,
                                message: '名称长度在10到100个字符之间',
                                trigger: ['blur', 'change']
                            },
                        ],

                        genre_id: [{
                            required: true,
                            message: '请选择作品分类',
                            trigger: ['blur', 'change']
                        },],
                        creator_id: [{
                            required: true,
                            message: '请选择作品所属卖家',
                            trigger: ['blur', 'change']
                        },],

                    }
                };
            },
            created() {
                for (let i = 0; i < this.pageSize; i++) {
                    if (i >= this.goods.length) {
                        break;
                    }
                    this.showGoods.push(this.goods[i]);
                }
                this.searchGenres.push({
                    'id': 'all',
                    'genre': '所有'
                })
            },
            methods: {
                //搜索
                search() {
                    if (this.searchGenre === 'all') {
                        this.thisSearchGenre_id = 'null';
                    } else {
                        this.thisSearchGenre_id = this.searchGenre;
                    }
                    if (this.searchText === '') {
                        this.thisSearchText = 'null';
                    } else {
                        this.thisSearchText = this.searchText;
                    }
                    //根据筛选条件加载一组数据
                    axios.get('/admin/goods/search/' + this.thisSearchGenre_id + '/' + this.thisSearchText)
                        .then(res => {
                            if (res.data.status_code === 200) {
                                app.goods = [];
                                res.data.goods.forEach((item, index) => {
                                    app.goods.push(item);
                                });
                                app.goodsCount = res.data.goodsCount;
                                app.currentPage=1;
                                app.loadShowGoods(0);
                            }
                        })
                        .catch(err => {
                            app.noticeError('服务器错误,请刷新重试！');
                            console.log(err);
                        })
                },

                //添加 提交
                handleSubmit() {
                    let checked = false;
                    this.$refs['addGoods'].validate(result => {
                        checked = result;
                    });
                    if (!checked) {
                        this.noticeWarning('数据填写不规范');
                        return false;
                    }
                    if (this.$refs['add-upload'].uploadFiles.length === 0) {
                        this.noticeWarning('请选择要上传的作品图片');
                    }
                    this.$refs['add-upload'].submit();
                },

                //上传作品信息
                upload(params) {
                    let data = new FormData;
                    let g = this.formGoods;
                    let size = g.length + '×' + g.width;
                    if (g.height !== null) {
                        size = size + '×' + g.height
                    }
                    size = size + ' cm';
                    data.append('file', params.file);
                    data.append('name', g.name);
                    data.append('author', g.author);
                    data.append('intro', g.intro);
                    data.append('size', size);
                    data.append('price', g.price);
                    data.append('genre_id', g.genre_id);
                    data.append('creator_id', g.creator_id);
                    axios.post('/admin/goods', data
                        //     {
                        //     //允许为上传处理进度事件
                        //     onUploadProgress: progressEvent => {
                        //         let percent = (progressEvent.loaded / progressEvent.total * 100) | 0;
                        //         //调用elemnet组件的原始onProgress方法来显示进度条，需要传递个对象 percent为进度值
                        //         params.onProgress({percent: percent})
                        //     }
                        // }
                    )
                        .then(function (res) {
                            if (res.data.status_code === 200) {
                                app.noticeSuccess(res.data.message);
                                app.addDialogVisible = false;
                                app.goodsCount += 1;
                            } else {
                                app.noticeWarnings(res.data.error);
                            }
                        })
                        .catch(err => {
                            app.noticeError('服务器错误，请刷新重试!');
                            console.log(err);
                        })
                },

                //------------------------下架-------------------
                takeOff() {
                    this.$refs['reason'].validate((result) => {
                        app.dataCheck = result;
                    });
                    if (!this.dataCheck) {
                        return false;
                    }
                    this.reasonDialogVisible = false;
                    let data = new FormData();
                    data.append('id', this.currentRow.id);
                    data.append('user_id', this.currentRow.user_id);
                    data.append('reason', this.reasons.reason);
                    data.append('name', this.currentRow.name);
                    axios.post('/admin/goods/takeOff', data)
                        .then(res => {
                            if (res.data.status_code === 200) {
                                app.removeAndADDGoods();
                                app.noticeSuccess(res.data.message);
                            }else{
                                app.noticeWarnings(res.data.error);
                            }
                        })
                        .catch(err => {
                            app.$notify.error({
                                title: '提示',
                                message: '服务器错误，请刷新重试！'
                            });
                            console.log(err);
                        })
                },
                //气泡框取消按钮关闭删除气泡确认框
                closeDelPopover(scope) {
                    scope._self.$refs['popover-' + scope.$index].doClose();
                },
                //删除气泡框确认按钮操作  删除作品
                delConfirm(scope) {
                    this.closeDelPopover(scope);
                    axios.delete('/admin/goods/' + scope.row.id)
                        .then(res => {
                            if (res.data.status_code === 200) {
                                app.noticeSuccess(res.data.message);
                                app.currentIndex=scope.$index;
                                app.currentRow=scope.row;
                                app.removeAndADDGoods();
                            }else{
                                app.noticeWarnings(res.data.error);
                            }

                        })
                        .catch(err => {
                            app.noticeError('服务器错误，请刷新重试！');
                            console.log(err);
                        })
                },

                //下架 删除 操作成功后
                // 从goods showGoods 中移除相应的数据项
                removeAndADDGoods() {
                    this.goodsCount = this.goodsCount - 1;
                    let deleIndex = (this.currentPage - 1) * this.pageSize + this.currentIndex;
                    this.goods.splice(deleIndex, 1);
                    this.showGoods.splice(this.currentIndex, 1);
                    let addIndex = this.currentPage * this.pageSize - 1;
                    if (addIndex < this.goods.length) {
                        this.showGoods.push(this.goods[addIndex]);
                        //当删除下架作品后 需要一条数据填充当前页下架或删除的记录位置 使当前页数据完整
                        //但此前加载数据已全部加载，且加载数据未达到数据库数据总条数，则向后台根据当前筛选条件获取一组数据
                    } else if (addIndex < this.goodsCount) {
                        let start = this.currentPage * this.pageSize - 1;
                        if (this.thisSearchGenre_id = 'all') {
                            this.thisSearchGenre_id = 'null';
                        }
                        if (this.thisSearchText === '') {
                            this.thisSearchText = 'null'
                        }
                        let params = start + '/' + this.thisSearchGenre_id + '/' + this.thisSearchText;
                        axios.get('/admin/goods/fill/' + params)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    res.data.data.forEach((item, index) => {
                                        app.goods.push(item);
                                    });
                                    app.showGoods.push(app.goods[addIndex]);
                                    setTimeout(function () {
                                        app.$nextTick(img_view())
                                    }, 500);
                                }
                            })
                            .catch(err => {
                                app.noticeError('服务器错误，请刷新重试！');
                                console.log(err);
                            });
                    }
                },

                //当前页改变后执行 加载新一页数据
                handleCurrentChange(currentPage) {
                    this.currentPage = currentPage;
                    let start = (currentPage - 1) * this.pageSize;
                    let end = start + this.pageSize;
                    if (start >= this.goods.length && start < this.goodsCount) {
                        if (this.thisSearchGenre_id === 'all') {
                            this.thisSearchGenre_id = 'null';
                        }
                        if (this.thisSearchText === '') {
                            this.thisSearchText = 'null'
                        }
                        let params = this.goods.length + '/' + end
                            + '/' + this.thisSearchGenre_id
                            + '/' + this.thisSearchText;
                        axios.get('/admin/goods/showNew/' + params)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    res.data.data.forEach((item, index) => {
                                        app.goods.push(item);
                                    });
                                    app.loadShowGoods(start);
                                }
                            })
                            .catch(err => {
                                app.noticeError('服务器错误，请刷新重试！');
                                console.log(err)
                            })
                    }
                    this.loadShowGoods(start);
                },

                // 加载新一页数据
                loadShowGoods(start) {
                    this.showGoods = [];
                    for (let i = 0; i < this.pageSize; i++) {
                        if (start >= this.goods.length) {
                            break;
                        }
                        this.showGoods.push(this.goods[start]);
                        start++;
                    }
                    setTimeout(function () {
                        app.$nextTick(img_view())
                    }, 500);
                },

                //选择文件后执行  对文件和文件列表进行验证
                handleFileChange(file, fileList) {
                    if (file.raw.type === 'image/jpeg' || file.raw.type === 'image/png') {
                        if (file.size / 1024 / 1024 > 2) {
                            this.$refs['add-upload'].clearFiles();
                            this.noticeWarning('图片大小不能超过2MB');
                        } else {
                            if (fileList.length > 1) {
                                fileList.splice(0, 1);
                            }
                        }
                    } else {
                        this.$refs['add-upload'].clearFiles();
                        this.noticeWarning('只能上传jpg/png格式图片');
                    }
                },
                handleRemove(file, fileList) {
                    console.log(file, fileList);
                },
                //添加修改商品 打开显示大图 Dialog
                handlePictureCardPreview(file) {
                    this.dialogImageUrl = file.url;
                    this.dialogVisible = true;
                },
                //goods列表打开显示大图Dialog
                goodsPictureCardPreview(imgPath) {
                    this.currentDialogImageUrl = this.wwwPath + imgPath;
                    this.currentImgDialogVisible = true;
                },
                //下架原因Dialog
                openTakenOffDialog(socpe) {
                    this.currentIndex = socpe.$index;
                    this.currentRow = socpe.row;
                    this.reasonDialogVisible = true;
                },

                //关闭添加作品Dialog
                closeAddDialog() {
                    this.addDialogVisible = false;
                    this.formGoods = {};
                },

                //打开编辑dialog
                openEditDialog(index, row) {
                    this.editDialogVisible = true;

                    this.currentIndex = index;
                    let size = row.size.replace('cm', '').replace(' ', '').split('×');
                    row.length = parseInt(size[0]);
                    row.width = parseInt(size[1]);
                    row.height = parseInt(size[2]);
                    this.currentRow = row;
                    if (typeof size[2] === 'undefined') {
                        row.height = 0;
                    }
                    //先转json字符串再转对象 取消引用传递
                    this.editGoods = JSON.parse(JSON.stringify(row));
                    setTimeout(function () {
                        app.$nextTick(edit_change_Img())
                    }, 500);
                },
                //关闭编辑dialog
                closeEditDialog() {
                    this.editDialogVisible = false;
                },
                editSubmit() {
                    let checked = false;
                    this.$refs['editGoods'].validate(result => {
                        checked = result;
                    });
                    if (!checked) {
                        this.noticeWarning('数据填写不规范');
                        return false;
                    }
                    let t = this.editGoods;
                    let size = t.length + '×' + t.width;
                    if (t.height !== null) {
                        size = size + '×' + t.height
                    }
                    size = size + ' cm';
                    let data = {
                        'id': t.id,
                        'name': t.name,
                        'author': t.author,
                        'price': t.price,
                        'size': size,
                        'intro': t.intro,
                        'genre_id': t.genre_id,
                        'creator_id': t.creator_id
                    };
                    axios.patch('/admin/goods/' + t.id, data)
                        .then(res => {
                            this.$notify.success({
                                title: '提示',
                                message: res.data.message,
                            });
                            this.showGoods.splice(this.currentIndex, 1, res.data.goods);
                        })
                        .catch(err => {
                            app.noticeError("服务器出错，请刷新重试");
                            console.log(err);
                        })
                },
                //上传图片
                editFileChange(file, fileList) {
                },
                changeImg(params) {
                    if (params.file.type === 'image/jpeg' || params.file.type === 'image/png') {
                        if (params.file.size / 1024 / 1024 > 2) {
                            this.$refs['edit-upload'].clearFiles();
                            this.noticeWarning('图片大小不能超过2MB');
                            return false;
                        }
                    } else {
                        this.$refs['edit-upload'].clearFiles();
                        this.noticeWarning('只能上传jpg/png格式图片');
                        return false;
                    }

                    let data = new FormData();
                    data.append('file', params.file);
                    data.append('id', this.currentRow.id);
                    axios.post('/admin/goods/changeImg', data)
                        .then(res => {
                            app.noticeSuccess('修改作品图片成功！');
                            app.showGoods[app.currentIndex].imgUrl = res.data.path;
                            app.editGoods.imgUrl = res.data.path;
                        })
                        .catch(err => {
                            app.noticeError('服务器繁忙，请稍后重试');
                            console.log(err);
                        })
                },

                //错误通知
                noticeError(error) {
                    this.$notify.error({
                        title: '错误',
                        message: error
                    })
                },
                //成功通知
                noticeSuccess(message) {
                    this.$notify.success({
                        title: '提示',
                        message: message
                    })
                },
                //警告提示
                noticeWarning(message) {
                    this.$notify.warning({
                        title: '提示',
                        message: message
                    })
                },
                //多警告提示
                noticeWarnings(messages) {
                    messages.forEach((item, index) => {
                        setTimeout(function () {
                            this.$notify.warning({
                                title: '提示',
                                message: item
                            })
                        }, 1);
                    });
                }
            },
            computed: {
                tableHeight: function () {
                    return this.mainHeight;
                },
            },
        });
    </script>

    <script src="{{asset('js/admin/goods.js')}}"></script>

@endsection
