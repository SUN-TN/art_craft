@extends('admin.layout.master')

@section('content')
    <div class="passwordPanel">
        <h1>修改密码</h1>
        <el-form label-position="right" label-width="80px" method="post" action="" >
            {{csrf_field()}}
            <el-form-item label="原密码" class="lab">
                <el-input class="inp" name="original_password"  v-model="pw.original_password" show-password></el-input>
            </el-form-item>
            <el-form-item label="新密码" class="lab">
                <el-input class="inp" name="password" v-model="pw.password" show-password></el-input>
            </el-form-item>
            <el-form-item label="确认密码" class="lab">
                <el-input class="inp" name="password_confirmation" v-model="pw.password_confirmation" show-password></el-input>
            </el-form-item>
            <el-button type="primary" native-type="submit" class="btn_sub">提交</el-button>
        </el-form>
    </div>
@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    el_menu: {
                        openIndex: '0',
                        active:'0',
                    },
                    pw:{
                        original_password:'',
                        password:'',
                        password_confirmation:'',
                    }
                };
            },
            mounted() {
                let message = '';
                @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            message = message + '<div>' +('{{$error}}') + '</div><br>';
                        @endforeach
                    this.$message.error({
                    dangerouslyUseHTMLString: true,
                    message: message.substr(0,message.length-4),
                });
                @endif
            }

        })
    </script>

    <style>
        .passwordPanel {
            position: relative;
            width: 350px;
            height: 400px;
            border-radius: 25px;
            border: 1px solid darkgray;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            background-color: white;
            top: 50%;
            left: 50%;
            padding: 20px;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        h1 {
            width: 100%;
            margin: 0 auto;
            text-align: center;
            font-size: 2em;
            padding-top: 10px;
        }

        .el-form {
            position: absolute;
            margin-left: 25px;
            margin-top: 20px;
        }

        .el-form-item{
            padding-top: 20px;
        }

        /*.inp {*/
            /*width: 350px;*/
        /*}*/

        .btn_sub {
            margin-top: 20px;
            margin-left: 45%;
        }
    </style>
@endsection