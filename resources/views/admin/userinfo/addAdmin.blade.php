@extends('admin.layout.master')

@section('title')
    添加管理员-用户管理
@endsection

@section('content')
    <div id="content">
        <div class="form-title">添加管理员账号</div>
        <el-form ref="admin" :model="admin" :hide-required-asterisk="true" :rules="rules"
                 :validate-on-rule-change="true">
            <el-form-item label="用户名" prop="username">
                <template>
                    <el-input placeholder="请输入用户名" v-model="admin.username"></el-input>
                </template>
            </el-form-item>

            <el-form-item label="密码" prop="password">
                <template>
                    <el-input placeholder="请输入密码" v-model="admin.password" show-password></el-input>
                </template>
            </el-form-item>

            <el-form-item label="确认密码" prop="password_confirm">
                <template>
                    <el-input placeholder="确认密码" v-model="admin.password_confirm" show-password></el-input>
                </template>
            </el-form-item>

            <el-form-item style="padding-top: 20px">
                <template>
                    <el-button type="primary" @click="submit">提交</el-button>
                </template>
            </el-form-item>

        </el-form>
    </div>
@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                let usernameFormat = (rule, value, callback) => {
                    let format = /^[a-zA-Z][a-zA-Z0-9_]*$/;
                    if (!value.match(format)) {
                        callback(new Error('用户名只能由字母数字下划线组成,且不能以数字开头'))
                    } else {
                        callback();
                    }
                };
                let passwordFormat = (rule, value, callback) => {
                    let format = /^[a-zA-Z0-9_]{1,}$/;
                    if (!value.match(format)) {
                        callback(new Error('密码由字母数字下划线组成'))
                    } else {
                        callback();
                    }
                };
                let check_password_confirm = (rule, value, callback) => {
                    if (value !== this.admin.password) {
                        callback(new Error('两次输入密码不一致!'));
                    } else {
                        callback();
                    }
                };
                return {
                    el_menu: {
                        openIndex: '2',
                        active: '2-3',
                    },

                    admin: {},


                    rules: {
                        username: [
                            {required: true, message: '请输入用户名', trigger: ['blur', 'change']},
                            {validator: usernameFormat, trigger: ['blur', 'change']},
                            {min: 3, max: 20, message: '长度在 3 到 20 个字符', trigger: ['blur', 'change']}
                        ],
                        password: [
                            {required: true, message: '密码不能为空', trigger: ['blur', 'change']},
                            {validator: passwordFormat, trigger: ['blur', 'change']},
                            {min: 8, max: 20, message: '密码长度在8-20个字符之间', trigger: ['blur', 'change']},

                        ],
                        password_confirm: [
                            {required: true, message: '确认密码不能为空', trigger: ['blur', 'change']},
                            {validator: check_password_confirm, trigger: ['blur', 'change']}
                        ]
                    }
                }
            },
            methods: {
                submit() {
                    let check = false;
                    this.$refs['admin'].validate(result => {
                        check = result;
                    });
                    console.log(check);
                    if (check) {
                        let data=new FormData();
                        data.append('username',this.admin.username);
                        data.append('password',this.admin.password);
                        data.append('password_confirmation',this.admin.password_confirm);
                        axios.post('/admin/addAdmin',data)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    app.$notify.success(res.data.message);
                                } else {
                                    res.data.error.forEach((item, index) => {
                                        app.$notify.warning(item);
                                    })
                                }
                            })
                            .catch(err => {
                                app.$notify.error('服务器出错，请刷新重试！');
                                console.log(err);
                            })
                    } else {
                        this.$notify.warning('数据填写不规范')
                    }
                }
            }
        })
    </script>

    <style>
        #content {
            width: 350px;
            height: 500px;
            background-color: white;
            border: 1px solid darkgray;
            border-radius: 10px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            text-align: center;
            padding: 0 30px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        #content .form-title {
            padding-top: 20px;
            font-size: 1.5em;
        }

        #content .el-form {
            min-width: 260px;
            position: absolute;
            top: 55%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .el-form input {
            text-align: center;
        }
    </style>

@endsection