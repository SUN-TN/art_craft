@extends('admin.layout.master')

@section('title')
    创作者信息-用户管理
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('css/admin/creatorsInfo.css')}}">
@endsection


@section('content')
    <el-row class="topBox">
        <el-col :md="3" :sm="3" :xs="3">
            <p class="title">创作者</p>
        </el-col>
        <el-col :sm="12" :xs="12" :md="12" :offset="6">
            <el-row>
                <el-col :md="12">
                    <el-input v-model="searchText" placeholder="请输入要搜索的创作者名称">
                        <el-button slot="append" type="primary" icon="el-icon-search"
                                   style="color: white;background-color: #66B1FF;border-radius:0;border: 1px solid #66B1FF"
                                   @click="search">搜索
                        </el-button>
                    </el-input>
                </el-col>
            </el-row>
        </el-col>
    </el-row>

    <el-row class="list">
        <el-col :md="24" :sm="24" :xs="24">
            {{--list table--}}
            <el-table style="width: 100%" :height="tableHeight" :data="showCreators">
                <el-table-column label="编号" width="55">
                    <template slot-scope="scope">
                        <div class="rowNumber">
                            <div>@{{ scope.$index+1 }}</div>
                        </div>
                    </template>
                </el-table-column>

                <el-table-column label="头像" width="100" style="position: relative">
                    <template slot-scope="scope">
                        <el-image class="creator_img" style="width: 100px; height:90px;"
                                  :src="wwwPath+scope.row.img_path"
                                  fit="cover">
                        </el-image>
                        <div class="img_view" @click="openPictureCardPreview(scope.row.img_path)">
                            <i class="el-icon-zoom-in" style="font-size: 25px"></i>
                        </div>
                    </template>
                </el-table-column>

                <el-table-column label="创作名称" width="160">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.creator_name }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="所属用户" width="160">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.name }}</span>
                    </template>
                </el-table-column>


                <el-table-column label="操作" style="min-width: 300px">
                    <template slot-scope="scope">
                        <el-button
                                size="mini" slot="reference"
                                @click="openEditDialog(scope.$index, scope.row)">编辑
                        </el-button>
                    </template>
                </el-table-column>
            </el-table>
            {{--分页--}}
            <div class="pagination">
                <el-pagination background layout="prev, pager, next"
                               :page-size="pageSize" :total="creatorCount"
                               :current-page.sync="currentPage"
                               @current-change="handleCurrentChange">
                </el-pagination>
            </div>
        </el-col>
    </el-row>

    <el-dialog :modal="false" :visible.sync="PictureCardPreviewVisble">
        <img width="100%" :src="PictureCardPreviewImageUrl" alt="">
    </el-dialog>

    <el-dialog :modal="false" :visible.sync="editDialogVisable" width="400px" center>
        <el-form :model="currentCreator" ref="editForm" :rules="rules"  :hide-required-asterisk="true" class="editForm">
            <el-form-item>
                <el-upload
                        class="avatar-uploader"
                        action="#"
                        ref="upload"
                        :auto-upload="false"
                        :show-file-list="false"
                        :http-request="editUpload"
                        :on-change="fileChange"
                >
                    <i v-show="false" class="el-icon-picture"></i>
                    <img :src="uploadImageUrl" class="avatar">
                </el-upload>
            </el-form-item>
            <el-form-item label="创作名称" prop="creator_name" class="form-item-name">
                <el-input v-model="currentCreator.creator_name"></el-input>
            </el-form-item>
        </el-form>

        <span slot="footer" class="dialog-footer">
                    <el-button @click="editDialogVisable = false">取 消</el-button>
                    <el-button type="primary" @click="editSubmit">提 交</el-button>
        </span>
    </el-dialog>

@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            created() {
                for (let i = 0; i < this.pageSize; i++) {
                    if (i >= this.creators.length) {
                        break;
                    }
                    this.showCreators.push(this.creators[i]);
                }
            },
            data() {
                return {
                    el_menu: {
                        openIndex: '2',
                        active: '2-2',
                    },
                    searchText: '',
                    currentSearchText: '',
                    mainHeight: 800,

                    creators: (@json($data)).creators,
                    creatorCount: (@json($data)).count,
                    showCreators: [],
                    pageSize: 10,
                    currentPage:1,

                    editDialogVisable: false,
                    currentCreator: {},
                    currentCreatorIndex: 0,

                    PictureCardPreviewVisble: false,
                    PictureCardPreviewImageUrl: '',
                    wwwPath: '{{asset('/')}}',
                    uploadImageUrl: '',

                    rules: {
                        creator_name: [
                            {required: true, message: '创作者名称不能为空', trigger: ['blur', 'change']},
                            {min: 1, max: 16, message: '长度在1到16个字符之间', trigger: ['blur', 'change']}
                        ]
                    }
                }
            },
            methods: {
                //搜索
                search() {
                    this.currentSearchText = this.searchText;
                    axios.get('/admin/creatorsInfo/searchByCreatorName/' + this.searchText)
                        .then(res => {
                            app.creatorCount = res.data.count;
                            app.creators = res.data.creators;
                            app.currentPage=1;
                            app.loadShowData(0);
                        })
                        .catch(err => {
                            app.$notify.error('服务器繁忙！请稍后再试');
                            console.log(err);
                        })
                },

                //editDialog 头像修改 选择头像后
                fileChange(file, fileList) {
                    if (file.raw.type === 'image/jpeg' || file.raw.type === 'image/png') {
                        if (file.size / 1024 / 1024 > 2) {
                            this.$refs['upload'].clearFiles();
                            this.$notify.warning('图片大小不能超过2MB');
                            return false;
                        } else {
                            if (fileList.length > 1) {
                                fileList.splice(0, 1);
                            }
                        }
                    } else {
                        this.$refs['upload'].clearFiles();
                        this.$message.warning('只能上传jpg/png格式图片');
                        return false;
                    }
                    this.$refs['upload'].submit();
                },
                //修改头像
                editUpload(params) {
                    let data = new FormData();
                    data.append('file', params.file);
                    data.append('id', this.currentCreator.id);
                    axios.post('/uploadCreatorImg', data)
                        .then(res => {
                            app.showCreators[app.currentCreatorIndex].img_path = res.data.path;
                            app.uploadImageUrl = app.wwwPath + res.data.path;
                            app.$notify.success('修改创作者头像成功！');
                        })
                        .catch(err => {
                            app.$notify.error('修改创作者头像失败，请刷新重试！');
                            console.log(err);
                        })
                },
                //编辑提交
                editSubmit() {
                    let check = false;
                    this.$refs['editForm'].validate(result => {
                        check = result;
                    });
                    if (check) {
                        let data = new FormData();
                        data.append('creator_name', this.currentCreator.creator_name);
                        data.append('id', this.currentCreator.id);
                        axios.post('/admin/creatorsInfo/changeName', data)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    app.showCreators[app.currentCreatorIndex].creator_name = res.data.creator_name;
                                    app.$notify.success('创作者信息修改成功！');
                                    app.editDialogVisable = false;
                                } else {
                                    res.data.error.forEach((item, index) => {
                                        app.$notify.warning(item);
                                    })
                                }
                            })
                            .catch(err => {
                                app.$notify.error('服务器繁忙！请稍后重试！');
                                console.log(err);
                            })
                    } else {
                        this.$notify.warning('数据填写不规范！');
                    }
                },
                //当前页改变 加载新一页数据
                handleCurrentChange(currentPage) {
                    let start = this.pageSize * (currentPage - 1);
                    if (start >= this.creators.length && start < this.creatorCount) {
                        let params = start + '/' + this.currentSearchText;
                        axios.get('/admin/creatorInfo/' + params)
                            .then(res => {
                                res.data.creators.forEach((item, index) => {
                                    app.creators.push(item);
                                })
                            })
                            .catch(err => {
                                app.$notify.error('服务器繁忙，请稍后再试！');
                                console.log(err);
                            })
                    }
                    this.loadShowData(start);
                },
                //搜索或分页切换加载数据 start 数据从creators数组中加载的起始位置
                loadShowData(start) {
                    this.showCreators = [];
                    for (let i = 0; i < this.pageSize; i++) {
                        if (start >= this.creators.length) {
                            break;
                        }
                        this.showCreators.push(this.creators[start]);
                        start++;
                    }
                    this.$nextTick(img_view());
                },
                //查看大图
                openPictureCardPreview(img_path) {
                    this.PictureCardPreviewVisble = true;
                    this.PictureCardPreviewImageUrl = this.wwwPath + img_path;
                },
                //点击编辑按钮 打开编辑Dialog
                openEditDialog(index, row) {
                    this.currentCreator = JSON.parse(JSON.stringify(row));
                    this.uploadImageUrl = this.wwwPath + row.img_path;
                    this.currentCreatorIndex = index;
                    this.editDialogVisable = true;
                    this.$nextTick(img_view());
                },
            },
            computed: {
                tableHeight: function () {
                    return this.mainHeight;
                },
            },
        });
    </script>


    <script src="{{asset('/js/admin/creatorsInfo.js')}}"></script>

@endsection
