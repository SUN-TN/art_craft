@extends('admin.layout.master')

@section('title')
    作品上架审核-申请审核管理
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/admin/putaway.css')}}">
@endsection

@section('content')
    <el-row class="topBox">
        <el-col :md="6" :sm="6" :xs="6">
            <p class="title">作品上架审核</p>
        </el-col>
    </el-row>

    <el-row class="list" style="overflow: auto">
        <el-col :md="24" :sm="24" :xs="24">
            <el-table
                style="width: 100%"
                :data="showApplys"
                :height="tableHeight">
                <el-table-column
                    label="编号"
                    width="55">
                    <template slot-scope="scope">
                        <div class="rowNumber">
                            <div>@{{ scope.$index+1 }}
                                <div></div>
                    </template>
                </el-table-column>

                <el-table-column
                    label="作品"
                    width="100">
                    <template slot-scope="scope">
                        <div style="position: relative">
                            <el-image
                                class="goods_img"
                                style="width: 100px; height:90px"
                                :src="wwwPath+scope.row.imgUrl"
                                @click="handlePictureCardPreview(scope.row.imgUrl)"
                                fit="cover">
                            </el-image>
                            <div class="img_view" @click="handlePictureCardPreview(scope.row.imgUrl)">查看大图</div>
                        </div>
                    </template>
                </el-table-column>

                <el-table-column
                    label="作品名称"
                    width="160">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.name }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                    label="作者"
                    width="120">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.author }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                    label="类型"
                    width="100">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.genre }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                    label="价格"
                    width="140">
                    <template slot-scope="scope">
                        <span>￥ @{{ scope.row.price }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                    label="尺寸"
                    width="140">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.size }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                    label="简介"
                    width="240">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.intro }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                    label="申请人"
                    width="100">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.creator_name }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="操作">
                    <template slot-scope="scope">
                        <el-button
                            size="mini"
                            type="success"
                            @click="agree(scope.$index, scope.row)">同意
                        </el-button>

                        <el-button
                            size="mini"
                            type="danger"
                            slot="reference"
                            @click="refuseReason(scope.$index, scope.row)">驳回
                        </el-button>
                    </template>
                </el-table-column>
            </el-table>

            <div class="pagination">
                <el-pagination
                    background
                    layout="prev, pager, next"
                    :page-size="pageSize"
                    :total="applys.length"
                    :current-page="currentPage"
                    @current-change="handleCurrentChange">
                </el-pagination>
            </div>

            <el-dialog :modal="false" :visible.sync="dialogVisible">
                <img width="100%" :src="currentImg" alt="">
            </el-dialog>

            <el-dialog title="驳回理由" :modal="false" :visible.sync="reasonDialogVisible" width="30%">
                <el-form :rules="rules" :model="reasons" ref="reason">
                    <el-form-item label="" label-width="200" prop="reason">
                        <el-input v-model="reasons.reason" placeholder="请请填写驳回理由" type="textarea"></el-input>
                    </el-form-item>
                </el-form>
                <div slot="footer" class="dialog-footer">
                    <el-button @click="reasonDialogVisible = false">取 消</el-button>
                    <el-button type="primary" @click="closeDialogAndRefuse">确 定</el-button>
                </div>
            </el-dialog>
        </el-col>
    </el-row>


@endsection


@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    el_menu: {
                        openIndex: '3',
                        active: '3-1',
                    },
                    applys: @json($data),
                    showApplys: [],
                    pageSize: 20,
                    currentPage: 1,
                    currentRow: null,
                    currentIndex: null,

                    wwwPath: '{{asset('')}}'.substring(0, '{{asset('')}}'.length - 1),

                    mainHeight: 800,
                    //查看大图Dialog imgUrl 是否显示Dialog
                    currentImg: '',
                    dialogVisible: false,

                    //查看大图Dialog imgUrl 是否显示Dialog
                    reasonDialogVisible: false,
                    reasons: {reason: ''},
                    //表单验证规则
                    rules: {
                        reason: [
                            {required: true, message: '请输入驳回理由', trigger: ['blur', 'change']},
                            {min: 5, max: 255, message: '长度在5到255个字符之间', trigger: ['blur', 'change']},
                        ],
                    }
                }
            },

            //首次加载页面 设置第一页要显示的数据
            created() {
                for (let i = 0; i < this.pageSize; i++) {
                    if (i >= this.applys.length) {
                        break;
                    }
                    this.showApplys.push(this.applys[i]);
                }
                this.$nextTick(img_view())
            },
            methods: {
                //打开显示大图Dialog
                handlePictureCardPreview(imgPath) {
                    this.currentImg = this.wwwPath + imgPath;
                    this.dialogVisible = true;
                },

                //单击同意按钮事件
                agree(index, row) {
                    this.currentRow = row;
                    this.currentIndex = index;
                    let data = new FormData();
                    data.append('id', row.id);
                    data.append('user_id', row.user_id);
                    axios.post('/admin/putAwayAudit/agree', data)
                        .then(function (res) {
                            if (res.data.status_code == 200) {
                                app.$notify.success({
                                    title: '提示',
                                    message: res.data.message
                                });
                                app.changeApply();
                            } else {
                                app.$notify.error({
                                    title: '提示',
                                    message: res.data.error
                                });
                            }
                        })
                        .catch(function (err) {
                            app.$notify.error({
                                title: '提示',
                                message: '服务器发生错误，请刷新重试'
                            });
                            console.log(err);
                        });
                },

                //单击驳回按钮事件 打开驳回原因Dialog 并设置当前数据项
                refuseReason(index, row) {
                    this.reasons.reason = '';
                    this.reasonDialogVisible = true;
                    this.currentIndex = index;
                    this.currentRow = row;
                },

                //单击驳回理由Dialog中的确定按钮
                closeDialogAndRefuse() {

                    this.$refs['reason'].validate((result) => {
                        app.dataCheck = result;
                    });
                    if (!this.dataCheck) {
                        return false;
                    }
                    //关闭Dialog
                    this.reasonDialogVisible = false;
                    //调用refuse方法
                    this.refuse();
                },

                //向服务器发送驳回申请请求
                refuse() {
                    let row = this.currentRow;
                    let data = new FormData();
                    data.append('id', row.id);
                    data.append('user_id', row.user_id);
                    data.append('name', row.name);
                    data.append('reason', this.reasons.reason);
                    axios.post('/admin/putAwayAudit/refuse', data)
                        .then(function (res) {
                            if (res.data.status_code == 200) {
                                app.$notify.success({
                                    title: '提示',
                                    message: res.data.message
                                });
                                app.changeApply();
                            } else {
                                app.$notify.error({
                                    title: '提示',
                                    message: res.data.message
                                });
                            }
                        })
                        .catch(function (err) {
                            app.$notify.error({
                                title: '提示',
                                message: '服务器发生错误，请刷新重试'
                            });
                            console.log(err);
                        });
                },
                //当同意或驳回操作成功后
                // 删除展示申请数据与所有申请数据中对应的数据项
                // 并从所有申请数据中添加一个数据项到展示数据项中
                changeApply() {
                    let deleIndex = (this.currentPage - 1) * this.pageSize + this.currentIndex;

                    this.applys.splice(deleIndex, 1);
                    this.showApplys.splice(this.currentIndex, 1);
                    let addIndex = this.currentPage * this.pageSize - 1;
                    if (addIndex < this.applys.length) {
                        this.showApplys.push(this.applys[addIndex]);
                    }
                    let pageCount=Math.ceil((this.applys.length/this.pagesize));
                    if(this.currentPage===pageCount && this.showApplys.length===0){
                        this.currentPage=pageCount;
                    }
                    this.$nextTick(img_view());
                },
                //当前页面变更之后执行 设置当前页面显示数据
                handleCurrentChange(currentPage) {
                    this.showApplys = [];
                    // this.currentPage = currentPage;
                    let start = currentPage * this.pageSize - this.pageSize;
                    for (let i = 0; i < this.pageSize; i++) {
                        if (start >= this.applys.length) {
                            break;
                        }
                        this.showApplys.push(this.applys[start]);
                        start++;
                    }
                    this.$nextTick(img_view())
                },
            },
            computed: {
                tableHeight: function () {
                    return this.mainHeight;
                },
            },
        });

        $(document).ready(function () {
            app.mainHeight = window.innerHeight - 220;
        });

        window.onresize = function () {
            let height = window.innerHeight - 75;
            $('.main').css({"height": height});
            app.mainHeight = window.innerHeight - 220;
        };

        function img_view() {
            setTimeout(function () {
                $('.goods_img').on({
                    'mouseenter': function () {
                        $('.img_view').show();
                    },
                    "mouseleave": function () {
                        $('.img_view').hide();
                    }
                });
                $(".img_view").on({
                    'mouseenter': function () {
                        $(this).show();
                    },
                    "mouseleave": function () {
                        $(this).hide();
                    }
                });
            }, 200)
        }

        img_view();
    </script>

@endsection
