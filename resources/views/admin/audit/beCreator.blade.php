@extends('admin.layout.master')

@section('title')
    创作者申请审核-申请审核管理
@endsection


@section('content')
    <el-row class="topBox">
        <el-col :md="6" :sm="6" :xs="6">
            <p class="title">创作者申请审核</p>
        </el-col>
    </el-row>

    <el-row class="list">
        <el-col :md="24" :sm="24" :xs="24">
            <el-table
                    style="width: 100%"
                    :data="applys">
                <el-table-column
                        label="编号"
                        width="55">
                    <template slot-scope="scope">
                        <div class="rowNumber"><div>@{{ scope.$index+1 }}<div></div>
                    </template>
                </el-table-column>

                <el-table-column
                        label="创作图标"
                        width="180">
                    <template slot-scope="scope">
                        <el-image
                                class="creator_img"
                                style="width: 45px; height: 45px"
                                :src="'{{asset('')}}'+scope.row.img_path"
                                @click="handlePictureCardPreview(scope.row.img_path)"
                                :fit="fit">
                        </el-image>
                    </template>
                </el-table-column>

                <el-table-column
                        label="创作名"
                        width="180">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.creator_name }}</span>
                    </template>
                </el-table-column>

                <el-table-column
                        label="申请用户"
                        width="180">
                    <template slot-scope="scope">
                        <span>@{{ scope.row.name }}</span>
                    </template>
                </el-table-column>

                <el-table-column label="操作">
                    <template slot-scope="scope">
                        <el-button
                                size="mini"
                                type="success"
                                @click="agree(scope.$index, scope.row)">同意
                        </el-button>

                        <el-button
                                size="mini"
                                type="danger"
                                slot="reference"
                                @click="refuse(scope.$index, scope.row)">拒绝
                        </el-button>
                    </template>
                </el-table-column>
            </el-table>
            <el-dialog :modal="false" :visible.sync="dialogVisible">
                <img width="100%" :src="currentImg" alt="">
            </el-dialog>
        </el-col>
    </el-row>
@endsection


@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    el_menu: {
                        openIndex: '3',
                        active: '3-2',
                    },
                    applys:@json($data),
                    currentImg: '',
                    dialogVisible: false,
                }
            },
            methods: {
                handlePictureCardPreview(imgPath) {
                    this.currentImg = '{{asset('')}}' + imgPath;
                    this.dialogVisible = true;
                },

                agree(index,row){
                    let data = new FormData();
                    data.append('id',row.id);
                    data.append('email',row.email);
                    data.append('name',row.name);
                    data.append('user_id',row.user_id);
                    axios.post('/admin/beCreatorAudit/agree',data)
                        .then(function (res) {
                            if(res.data.status_code==200){
                                app.applys.splice(index,1);
                                app.$notify.success({
                                    title:'提示',
                                    message:res.data.message
                                });
                            }else {
                                app.$notify.error({
                                    title:'提示',
                                    message:res.data.message
                                });
                            }
                        })
                        .catch(function (err) {
                            app.$notify.error({
                                title:'提示',
                                message:'服务器发生错误，请刷新重试'
                            });
                        });
                },
                refuse(index,row){
                    let data = new FormData();
                    data.append('id',row.id);
                    data.append('email',row.email);
                    data.append('name',row.name);
                    axios.post('/admin/beCreatorAudit/refuse',data)
                        .then(function (res) {
                            if(res.data.status_code==200){
                                app.applys.splice(index,1);
                                app.$notify.success({
                                    title:'提示',
                                    message:res.data.message
                                });
                            }else {
                                app.$notify.error({
                                    title:'提示',
                                    message:res.data.message
                                });
                            }
                        })
                        .catch(function (err) {
                            app.$notify.error({
                                title:'提示',
                                message:'服务器发生错误，请刷新重试'
                            });
                        });
                }
            }
        })

    </script>


    <style>
        #content {
            padding: 10px;
            height: 100%;
            background-color: white;
        }

        .topBox {
            position: sticky;
            top: 10px;
            height: 70px;
            padding: 10px;
            line-height: 50px;
            border-bottom: solid 1px rgb(245, 240, 240);
        }

        .title {
            margin: 0;
            margin-left: 10px;
            min-width: 300px;
            font-size: 22px;
        }

        .list {
            padding-top: 20px;
            padding-bottom: 50px;
        }

        .list .cell {
            text-align: center;
        }

        .list .creator_img {
          cursor: pointer;
        }

        .list .rowNumber {
            width:34px;
            height: 34px;
            line-height: 30px;
            margin: 0 auto;
            font-weight: lighter;
            font-size: 0.8em;
            border-radius: 50px;
            background-color: white;
            color: black;
            border: 2px slategray inset;
            transform:rotate(-45deg);
            -ms-transform:rotate(-45deg); 	/* IE 9 */
            -moz-transform:rotate(-45deg); 	/* Firefox */
            -webkit-transform:rotate(-45deg); /* Safari 和 Chrome */
            -o-transform:rotate(-45deg);
        }

        .list .rowNumber div{
            /*background: black;*/
            transform:rotate(45deg);
            -ms-transform:rotate(45deg); 	/* IE 9 */
            -moz-transform:rotate(45deg); 	/* Firefox */
            -webkit-transform:rotate(45deg); /* Safari 和 Chrome */
            -o-transform:rotate(45deg);
        }
    </style>
@endsection
