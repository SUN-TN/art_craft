<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="{{asset('/elementUI/index.js')}}"></script>
    <script src="{{asset('/js/axios.min.js')}}"></script>
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/elementUI/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/master.css')}}">
    @yield('link')
</head>

<body>

<div id="app">
    <el-header class="header">
        <span class="icon">后台管理</span>
        <el-dropdown class="adminPanel">
                    <span class="el-dropdown-link">
                        {{Auth::guard('admin')->user()->username}}
                        <i class="el-icon-arrow-down el-icon--right"></i>
                    </span>
            <el-dropdown-menu slot="dropdown">
                <a href="/admin/changePassword">
                    <el-dropdown-item>修改密码</el-dropdown-item>
                </a>
                <a href="/admin/logout">
                    <el-dropdown-item>退出</el-dropdown-item>
                </a>
            </el-dropdown-menu>
        </el-dropdown>
    </el-header>

    <el-row class="mainPanel">
        <el-col class="aside" :span="4">
            <el-menu class="el-menu-vertical-demo" text-color="black" :unique-opened="true"
                     :default-openeds="[el_menu.openIndex]" :default-active="el_menu.active">

                <el-submenu index="1">
                    <template slot="title">
                        <i class="el-icon-menu"></i>
                        <span>作品管理</span>
                    </template>
                    <a href="/admin/goods">
                        <el-menu-item index="1-1">作品信息管理</el-menu-item>
                    </a>
                    <a href="/admin/genres">
                        <el-menu-item index="1-2">作品类型管理</el-menu-item>
                    </a>
                </el-submenu>


                <el-submenu index="2">
                    <template slot="title">
                        <i class="el-icon-menu"></i>
                        <span>用户管理</span>
                    </template>
                    <a href="/admin/usersInfo">
                        <el-menu-item index="2-1">用户信息</el-menu-item>
                    </a>
                    <a href="/admin/creatorsInfo">
                        <el-menu-item index="2-2">创作者信息</el-menu-item>
                    </a>
                    <a href="/admin/addAdmin">
                        <el-menu-item index="2-3">添加管理员</el-menu-item>
                    </a>
                </el-submenu>

                <el-submenu index="3">
                    <template slot="title">
                        <i class="el-icon-menu"></i>
                        <span style="color: black;">申请审核管理</span>
                    </template>
                    <a href="/admin/putAwayAudit">
                        <el-menu-item index="3-1">作品上架审核</el-menu-item>
                    </a>
                    <a href="/admin/beCreatorAudit">
                        <el-menu-item index="3-2">创作者申请审核</el-menu-item>
                    </a>
                </el-submenu>

                <el-submenu index="4">
                    <template slot="title">
                        <i class="el-icon-menu"></i>
                        <span style="color: black;">订单管理</span>
                    </template>
                    <a href="/admin/order">
                        <el-menu-item index="4-1">订单记录</el-menu-item>
                    </a>
                </el-submenu>
            </el-menu>
        </el-col>

        <el-col :span="20" class="main">
            <el-row style="height: 100%;min-width: 800px">
                <el-col :span="23" :offset="1" style="background-color: white;height: 100%;">
                    @yield('content')
                </el-col>
            </el-row>
        </el-col>
    </el-row>
</div>

<script>
    window.onload = function () {
        let height = window.innerHeight - 75;
        $('.mainPanel').css({"height": height})
    };

    window.onresize = function () {
        let height = window.innerHeight - 75;
        $('.mainPanel').css({"height": height})
    };
</script>

@yield('js_css')

</body>

</html>
