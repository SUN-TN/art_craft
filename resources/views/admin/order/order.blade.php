@extends('admin.layout.master')

@section('title')
    订单记录-订单管理
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/admin/order.css')}}">
@endsection

@section('content')
        <el-row class="topBox">
            <el-col :md="6" :sm="6" :xs="6">
                <p class="title">订单记录</p>
            </el-col>
            <el-col :sm="12" :xs="12" :md="12" :offset="1">
                <el-row>
                    <el-col :md="18">
                        <el-input v-model="searchText" placeholder="请输入要搜索订单编号" style="padding: 0">
                            <el-select slot="prepend" v-model="searchStatusCode" clearable placeholder="所有"
                                       style="width: 100px">
                                <el-option v-for="(item,index) in orderStatus" :key="item.index" :label="item.describe"
                                           :value="item.status_code">
                                </el-option>
                            </el-select>
                            <el-button slot="append" type="primary" icon="el-icon-search"
                                       style="color: white;background-color: #66B1FF;border-radius: 0px"
                                       @click="search">搜索
                            </el-button>
                        </el-input>
                    </el-col>
                </el-row>
            </el-col>
        </el-row>

        <el-row class="list">
            <el-col :md="24" :sm="24" :xs="24">
                <el-table
                    style="width: 100%"
                    :height="tableHeight"
                    :data="orders">
                    <el-table-column
                        label="编号"
                        width="55">
                        <template slot-scope="scope">
                            <div class="rowNumber" style="margin-left: 10px">
                                <div>@{{ scope.$index+1 }}</div>
                            </div>
                        </template>
                    </el-table-column>


                    <el-table-column
                        label="作品"
                        width="140">
                        <template slot-scope="scope">
                            <el-image class="goods_img" style="width: 100px; height:90px;"
                                      :src="'{{asset('/')}}'+scope.row.imgUrl"
                                      fit="cover">
                            </el-image>
                            <div class="img_view" @click="goodsPictureCardPreview(scope.row.imgUrl)">
                                <i class="el-icon-zoom-in" style="font-size: 25px"></i>
                            </div>
                        </template>
                    </el-table-column>

                    <el-table-column
                        label="订单号"
                        width="320"
                    >
                        <template slot-scope="scope">
                            <span>@{{ scope.row.id }}</span>
                        </template>
                    </el-table-column>

                    <el-table-column
                        label="作品名称"
                        width="140">
                        <template slot-scope="scope">
                            <span>@{{ scope.row.goods_name }}</span>
                        </template>
                    </el-table-column>

                    <el-table-column
                        label="收货人姓名"
                        width="140">
                        <template slot-scope="scope">
                            <span>@{{ scope.row.consignee_name }}</span>
                        </template>
                    </el-table-column>

                    <el-table-column
                        label="收货人联系电话"
                        width="140">
                        <template slot-scope="scope">
                            <span>@{{ scope.row.phone }}</span>
                        </template>
                    </el-table-column>

                    <el-table-column
                        label="购买用户"
                        width="140">
                        <template slot-scope="scope">
                            <span>@{{ scope.row.username }}</span>
                        </template>
                    </el-table-column>

                    <el-table-column
                        label="订单状态">
                        <template slot-scope="scope">
                            <el-tag v-if="scope.row.status===0">等待付款</el-tag>
                            <el-tag v-if="scope.row.status===1" type="success">已付款</el-tag>
                            <el-tag v-if="scope.row.status===100" type="warning">退款中</el-tag>
                            <el-tag v-if="scope.row.status===2" type="success">已发货</el-tag>
                            <el-tag v-if="scope.row.status===200" type="warning">退货退款中</el-tag>
                            <el-tag v-if="scope.row.status===4" type="success">已完成</el-tag>
                            <el-tag v-if="scope.row.status===5" type="info">已关闭</el-tag>
                        </template>
                    </el-table-column>
                </el-table>

                {{--分页--}}
                <div class="pagination">
                    <el-pagination background layout="prev, pager, next"
                                   :page-size="pageSize"
                                   :total="orderCount"
                                   :current-page.sync="currentPage"
                                   @current-change="pageChanged">
                    </el-pagination>
                </div>
            </el-col>
        </el-row>

        {{--查看大图--}}
        <el-dialog :modal="false" :visible.sync="currentImgDialogVisible">
            <img width="100%" :src="'{{asset('/')}}'+currentDialogImageUrl" alt="">
        </el-dialog>

@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    el_menu: {
                        openIndex: '4',
                        active: '4-1',
                    },
                    pageSize: 20,
                    currentPage: 1,
                    orders: (@json($data)).orders,
                    orderStatus: (@json($data)).orderStatus,
                    orderCount: (@json($data)).orderCount,

                    searchStatusCode: '',
                    searchText: '',

                    theSearchStatusCode: 'all',  //当前筛选订单状态
                    theSearchText: '',       //当前筛选订单号

                    currentImgDialogVisible: false,
                    currentDialogImageUrl: '',
                    mainHeight: 820,

                };
            },
            mounted(){
              this.$nextTick(img_view());
            },
            methods: {
                search() {
                    this.theSearchStatusCode = 'all';
                    if (this.searchStatusCode !== '') {
                        this.theSearchStatusCode = this.searchStatusCode;
                    }
                    this.theSearchText = this.searchText;
                    let params = this.theSearchStatusCode + '/' + this.theSearchText;
                    axios.get('/admin/order/search/' + params)
                        .then(res => {
                            app.currentPage = 1;
                            app.orders = res.data.data.orders;
                            app.orderCount = res.data.data.orderCount;
                            setTimeout(function () {
                                app.$nextTick(img_view())
                            }, 500);
                        })
                        .catch(err => {
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后重试');
                        });
                },
                pageChanged(currentPage) {
                    let params = currentPage + '/' + this.theSearchStatusCode + '/' + this.theSearchText;
                    axios.get('/admin/order/newPage/' + params)
                        .then(res => {
                            this.orders = res.data.data;
                            setTimeout(function () {
                                app.$nextTick(img_view())
                            }, 500);
                        })
                        .catch(err => {
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后重试');
                        });
                },

                //goods列表打开显示大图Dialog
                goodsPictureCardPreview(imgPath) {
                    this.currentDialogImageUrl = imgPath;
                    this.currentImgDialogVisible = true;
                },

                //错误通知
                noticeError(error) {
                    this.$notify.error({
                        title: '错误',
                        message: error
                    })
                },
                //成功通知
                noticeSuccess(message) {
                    this.$notify.success({
                        title: '提示',
                        message: message
                    })
                },
                //警告提示
                noticeWarning(message) {
                    this.$notify.warning({
                        title: '提示',
                        message: message
                    })
                },
                //多警告提示
                noticeWarnings(messages) {
                    messages.forEach((item, index) => {
                        setTimeout(function () {
                            this.$notify.warning({
                                title: '提示',
                                message: item
                            })
                        }, 1);
                    });
                }
            },
            computed: {
                tableHeight: function () {
                    return this.mainHeight;
                },
            },
        });

        $(document).ready(function () {
            app.mainHeight = window.innerHeight - 200;
        });

        window.onresize = function () {
            let height = window.innerHeight - 75;
            $('.main').css({"height": height});
            app.mainHeight = window.innerHeight - 200;
        };

        function img_view() {
            setTimeout(function () {
                $('.goods_img').on({
                    'mouseenter': function () {
                        $(this).next().show();
                    },
                    "mouseleave": function () {
                        $(this).next().hide();
                    }
                });
                $(".img_view").on({
                    'mouseenter': function () {
                        $(this).show();
                    },
                    "mouseleave": function () {
                        $(this).hide();
                    }
                });
            },200);
        }
    </script>


    <style>
        #content {
            width: 100%;
        }
    </style>

@endsection
