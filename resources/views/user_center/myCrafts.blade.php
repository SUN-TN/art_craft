@extends('user_center.master')

@section('title')
    我的作品-个人中心
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/user_center/myCrafts.css')}}">
@endsection
@section('info-title')
    我的作品
@endsection

@section('content')
    <div class="nav-box">
        <el-menu
            default-active="1"
            class="el-menu-demo"
            mode="horizontal"
            @select="navSelect"
            background-color="#394046"
            text-color="white"
            ref="navSelect"
            active-text-color="#ffd04b">
            <el-menu-item index="1">已上架</el-menu-item>
            <el-menu-item index="2">已售/正在交易</el-menu-item>
            <el-tooltip class="item" effect="dark" content="仅包含上架后删除作品" placement="bottom">
                <el-menu-item index="5">已删除</el-menu-item>
            </el-tooltip>
            <el-menu-item index="3">等待审核</el-menu-item>
            <el-menu-item index="4">审核未通过</el-menu-item>
        </el-menu>
    </div>

    <el-row style="margin: 20px 0 20px 0">
        <el-col :sm="12" :xs="12" :md="12" :offset="2">
            <el-input placeholder="请输入作品名称" v-model="searchText" class="input-with-select">
                <el-select v-model="searchGenreId" slot="prepend" placeholder="所有" clearable>
                    <el-option
                        v-for="item in genres"
                        :key="item.id"
                        :label="item.genre"
                        :value="item.id">
                    </el-option>
                </el-select>
                <el-button slot="append" icon="el-icon-search" class="btn-search-icon" @click="search"></el-button>
            </el-input>
        </el-col>
        <el-col :sm="4" :xs="4" :md="4" :offset="4">
            <el-button @click="drawer = true" class="btn_putAway">上架作品</el-button>
        </el-col>
    </el-row>

    <el-divider></el-divider>

    <div id="masonry" class="masonry" style="height: auto !important; opacity: 0">
        <div class="masonry-item" id="masonry-item" v-for="(item,index) in showData" :key="item.id">
            <el-image class="crafts-item-img" :src="wwwPath+item.imgUrl" @click="detail(item)"></el-image>
            <div class="crafts-item-info">
                <div class="crafts-item-name">@{{item.name}}</div>
                <div class="crafts-item-author">@{{item.author}}</div>
                <div class="crafts-item-Size">￥@{{item.price}} | @{{item.size}}</div>
                <div v-if="navSelectItem !=='2'" class="crafts-item-options" style="text-align: right;">
                    <el-button v-if="navSelectItem === '5'" size="mini" @click="restore(item)">恢复</el-button>
                    <el-button v-if="navSelectItem ==='1' || navSelectItem ==='4'"
                               size="mini" @click="openEditDrawer(item,index)" class="btn_edit">编辑
                    </el-button>
                    <el-popover placement="top" width="160"
                                :ref="'popover-'+item.id">
                        <p>确定删除吗？</p>
                        <div style="text-align: right; margin: 0">
                            <el-button size="mini" type="text" @click="closeDelPopover(item)">取消</el-button>
                            <el-button type="primary" size="mini" @click="delConfirm(item)">确定</el-button>
                        </div>
                        <el-button v-if="navSelectItem !== '5'" slot="reference" size="mini" type="danger">删除
                        </el-button>
                        <el-button v-else slot="reference" size="mini" type="danger">彻底删除</el-button>
                    </el-popover>
                </div>
            </div>
        </div>

        <div v-show="showData.length<=0"
             style="width: 100%;text-align: center;margin-top: 50px;font-size: 2em;"
             class="noData">
            暂无数据
        </div>
    </div>

    {{--上架作品Drawer--}}
    <el-drawer
        title=""
        :visible.sync="drawer"
        :with-header="false"
        :modal="false">
        <div style="text-align: center"><h1>上架申请</h1></div>
        <el-form
            label-width="120px"
            label-position="right"
            :model="newCraft"
            :rules="rules"
            :status-icon="true"
            {{--:inline-message="true"--}}
            :hide-required-asterisk="true"
            ref="newCraft"
            class="form-newCraft">
            <el-form-item label="作者" prop="author">
                <el-input v-model="newCraft.author"></el-input>
            </el-form-item>
            <el-form-item label="作品名称" prop="name">
                <el-input v-model="newCraft.name"></el-input>
            </el-form-item>
            <el-form-item label="作品尺寸" prop="length">
                <el-input v-model.number="newCraft.length">
                    <template slot="prepend">长</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="width">
                <el-input v-model.number="newCraft.width">
                    <template slot="prepend">宽</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="height">
                <el-input v-model.number="newCraft.height" :disabled='newCraft.genre_id != 4'>
                    <template slot="prepend">高</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="作品价格" prop="price">
                <el-input v-model="newCraft.price">
                    <template slot="prepend">￥</template>
                </el-input>
            </el-form-item>
            <el-form-item label="作品简介" prop="intro">
                <el-input v-model="newCraft.intro" type="textarea" :autosize="{minRows: 2}"></el-input>
            </el-form-item>
            <el-form-item label="作品分类" prop="genre_id">
                <el-select v-model="newCraft.genre_id" placeholder="分类">
                    <el-option
                        v-for="item in genres"
                        :key="item.id"
                        :label="item.genre"
                        :value="item.id">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item label="选择图片">
                <el-upload
                    ref="add-upload"
                    action="#"
                    :auto-upload="false"
                    :multiple="false"
                    list-type="picture-card"
                    ccept=".jpg,.png"
                    :on-change="handleFileChange"
                    :on-preview="handlePictureCardPreview"
                    :on-remove="handleRemove"
                    :http-request="upload"
                    :withCredentials="false">
                    <i class="el-icon-plus"></i>
                    <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且大小不超过2MB</div>
                </el-upload>
                <el-dialog :modal="false" :visible.sync="dialogVisible">
                    <img width="100%" :src="dialogImageUrl" alt="">
                </el-dialog>
            </el-form-item>
        </el-form>

        <el-row class="drawer_options">
            <el-col :sm="4" :xs="4" :md="4" :offset="8">
                <el-button @click="drawer = false" style="float: right">取消</el-button>
            </el-col>
            <el-col :sm="4" :xs="4" :md="4" :offset="2">
                <el-button type="primary" @click="putAwaySubmit">提交</el-button>
            </el-col>
        </el-row>

    </el-drawer>

    {{--编辑作品Drawer--}}
    <el-drawer
        title="" i
        :visible.sync="editDrawerVisible"
        :with-header="false"
        :modal="false">
        <div style="text-align: center"><h1>修改作品信息</h1></div>
        <el-form
            label-width="120px"
            label-position="right"
            :model="currentCraft"
            :rules="rules"
            :status-icon="true"
            :hide-required-asterisk="true"
            ref="editCraft"
            class="form-newCraft">
            <el-form-item label="作者" prop="author">
                <el-input v-model="currentCraft.author"></el-input>
            </el-form-item>
            <el-form-item label="作品名称" prop="name">
                <el-input v-model="currentCraft.name"></el-input>
            </el-form-item>
            <el-form-item label="作品尺寸" prop="length">
                <el-input v-model.number="currentCraft.length">
                    <template slot="prepend">长</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="width">
                <el-input v-model.number="currentCraft.width">
                    <template slot="prepend">宽</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="" prop="height">
                <el-input v-model.number="currentCraft.height" :disabled='currentCraft.genre_id != 4'>
                    <template slot="prepend">高</template>
                    <template slot="append">cm</template>
                </el-input>
            </el-form-item>
            <el-form-item label="作品价格" prop="price">
                <el-input v-model="currentCraft.price">
                    <template slot="prepend">￥</template>
                </el-input>
            </el-form-item>
            <el-form-item label="作品简介" prop="intro">
                <el-input v-model="currentCraft.intro" type="textarea" :autosize="{minRows: 2}"></el-input>
            </el-form-item>
            <el-form-item label="作品分类" prop="genre_id">
                <el-select v-model="currentCraft.genre_id" placeholder="分类">
                    <el-option
                        v-for="item in genres"
                        :key="item.id"
                        :label="item.genre"
                        :value="item.id">
                    </el-option>
                </el-select>
            </el-form-item>
            <el-form-item label="选择图片">
                <el-upload
                    class="avatar-uploader"
                    action="#"
                    ref="edit-upload"
                    :auto-upload="false"
                    :show-file-list="false"
                    :http-request="editUploadImg"
                    :on-change="editImgChange">
                    <i v-show="false" class="el-icon-picture"></i>
                    <img :src="currentCraft.imgUrl" class="avatar">
                </el-upload>
            </el-form-item>
        </el-form>

        <el-row class="drawer_options">
            <el-col :sm="4" :xs="4" :md="4" :offset="8">
                <el-button @click="editDrawerVisible = false" style="float: right">取消</el-button>
            </el-col>
            <el-col :sm="4" :xs="4" :md="4" :offset="2">
                <el-button type="primary" @click="editSubmit">提交</el-button>
            </el-col>
        </el-row>

    </el-drawer>


@endsection

@section('footer')
    <el-row>
        <el-col :md="14" :offset="7" style="text-align: center">
            <el-pagination
                background
                layout="prev, pager, next"
                :page-size="pageSize"
                :total="allShowData.length"
                :current-page.sync="currentPage"
                ref="pagination"
                @current-change="pageChanged">
            </el-pagination>
        </el-col>
    </el-row>
@endsection

@section('js_css')
    <script>
        var app = new Vue({
                el: '#app',
                created() {
                    for (let i = 0; i < this.pageSize; i++) {
                        if (i >= this.allShowData.length) {
                            break;
                        }
                        this.showData.push(this.allShowData[i]);
                    }
                },
                data() {
                    return {
                        activeItem: [false, false, false, false, false, false, false, true, false, false, false],
                        searchText: '', //搜索内容
                        searchGenreId: '', //搜索选择分类ID
                        genres: (@json($data)).genres,
                        creator: (@json($data)).creator,
                        {{--mydata: @json($data),  //--}}
                        // currentNavData: null, //当前选项卡所有数据
                        user:@json(Auth::user()),
                        navSelectItem: '1',  //1-已上架 2-已售 3-待审核 4-审核未通过

                        allDataOfCurrentSelectItem: (@json($data)).crafts, //当前选项所有数据,
                        allShowData: (@json($data)).crafts,//经过条件筛选后所有可展示的数据
                        showData: [],//当前页要展示的数据，实际要渲染的数据
                        // dataCount: 1,//allShowData.length
                        pageSize: 20,
                        currentPage: 1,

                        {{--creator: @json($data)['creator'], //创作者信息--}}

                        //新品上架
                        drawer: false,//新品上架侧边抽屉是否打开
                        addOrChange: true,//true 为新增上架申请  false 为修改上架申请
                        newCraft: {genre_id: ''},//drawer 数据模型
                        dialogImageUrl: '',//查看图片对话框图片Url
                        dialogVisible: false,//查看图片对话框是否显示
                        dataCheck: false,//数据验证是否通过

                        //编辑
                        editDrawerVisible: false,
                        currentCraft: {},
                        currentCraftIndex: 0,
                        editImgDialogVisible: false,


                        wwwPath: '{{asset('')}}'.substr(0, '{{asset('')}}'.length - 1),
                        //表单数据验证规则
                        rules: {
                            author: [
                                {required: true, message: '请输入作者名称', trigger: ['blur', 'change']},
                                {min: 1, max: 32, message: '作者名称长度在1到32个字符之间', trigger: ['blur', 'change']},
                            ],
                            name: [
                                {required: true, message: '请输入作品名称', trigger: ['blur', 'change']},
                                {min: 1, max: 32, message: '作品名称长度在1到32个字符之间', trigger: ['blur', 'change']},
                            ],
                            length: [
                                {required: true, message: '请输入作品长度', trigger: ['blur', 'change']},
                                {type: 'number', message: '只能输入数字', trigger: 'change'},
                            ],
                            width: [
                                {required: true, message: '请输入作品宽度', trigger: ['blur', 'change']},
                                {type: 'number', message: '只能输入数字', trigger: ['blur', 'change']},
                            ],
                            height: [
                                {required: false, message: '请输入作品高度', trigger: ['blur', 'change']},
                                {pattern: /[^\.\D]/g, message: '只能输入数字', trigger: ['blur', 'change']},
                            ],
                            price: [
                                {required: true, message: '请输入作品价格', trigger: ['blur', 'change']},
                                {
                                    pattern: /^(?!0+$)(?!0*\.0*$)\d{1,8}(\.\d{1,2})?$/,
                                    message: '只能输入数字,且只能两位小数',
                                    trigger: ['blur', 'change']
                                },
                            ],
                            intro: [
                                {required: true, message: '请输入作品简介', trigger: ['blur', 'change']},
                                {min: 10, max: 255, message: '简介长度在10到100个字符之间', trigger: ['blur', 'change']},
                            ],
                            genre_id: [
                                {required: true, message: '请选择作品分类', trigger: ['blur', 'change']},
                            ]
                        },
                    };

                },
                methods: {
                    //editDialog 作品图片修改 选择图片后
                    editImgChange(file, fileList) {
                        if (file.raw.type === 'image/jpeg' || file.raw.type === 'image/png') {
                            if (file.size / 1024 / 1024 > 2) {
                                this.$refs['edit-upload'].clearFiles();
                                this.$notify.warning('图片大小不能超过2MB');
                                return false;
                            } else {
                                if (fileList.length > 1) {
                                    fileList.splice(0, 1);
                                }
                            }
                        } else {
                            this.$refs['edit-upload'].clearFiles();
                            this.$message.warning('只能上传jpg/png格式图片');
                            return false;
                        }
                        this.$refs['edit-upload'].submit();
                    },
                    //修改作品图片
                    editUploadImg(params) {
                        let data = new FormData();
                        data.append('file', params.file);
                        data.append('id', this.currentCraft.id);
                        axios.post('/uploadCraftImg', data)
                            .then(res => {
                                app.showData[app.currentCraftIndex].imgUrl = res.data.path;
                                app.currentCraft.imgUrl = res.data.path;
                                app.$notify.success('修改作品图片成功！');
                            })
                            .catch(err => {
                                app.$notify.error('修改作品图片失败，请刷新重试！');
                                console.log(err);
                            })
                    },
                    //
                    openEditDrawer(item, index) {
                        //先转换为json字符串再转换为对象  
                        //作用：消除直接赋值造成的item与this.currentCraft的引用传值
                        this.currentCraft = JSON.parse(JSON.stringify(item));
                        this.currentCraftIndex = index;
                        let size = this.currentCraft.size.replace('cm', '').split('×');
                        size = size.filter(res => res !== "undefined");
                        this.$set(this.currentCraft, 'length', parseInt(size[0]));
                        this.$set(this.currentCraft, 'width', parseInt(size[1]));
                        this.$set(this.currentCraft, 'height', '');
                        if (typeof (size[2]) !== 'undefined') {
                            this.currentCraft.height = parseInt(size[2]);
                        }
                        this.editDrawerVisible = true;
                        this.$nextTick(img_view())
                    },
                    //编辑Drawer提交
                    editSubmit() {
                        let check = false;
                        this.$refs['editCraft'].validate((result) => {
                            check = result;
                        });
                        if (!check) {
                            this.noticeWarning('数据填写不规范');
                            return false;
                        }
                        let data = new FormData();
                        let d = this.currentCraft;
                        data.append('id', d.id);
                        data.append('author', d.author);
                        data.append('name', d.name);
                        data.append('price', d.price);
                        data.append('intro', d.intro);
                        data.append('genre_id', d.genre_id);
                        let size = d.length + '×' + d.width;
                        if (this.currentCraft.genre_id == 4 && d.height !== '' && d.height !== 0 && typeof (d.height) !== 'undefined') {
                            size = size + '×' + d.height;
                        }
                        size = size + ' cm';
                        data.append('size', size);
                        let url = '';
                        if (this.navSelectItem === '1') {
                            url = '/user/myCrafts/updateCraft';
                        } else if (this.navSelectItem === '4') {
                            url = '/user/myCrafts/updatePutAwayApply';
                        }
                        console.log(url);
                        axios.post(url, data)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    if (app.navSelectItem === '1') {
                                        //更新显示数据
                                        app.showData.splice(app.currentCraftIndex, 1, res.data.data);
                                        //更新原数组数组
                                        app.allDataOfCurrentSelectItem.filter((item,index,arr)=>{
                                            if (item.id === res.data.data.id) {
                                                arr[index]=res.data.data;
                                                return '';
                                            }
                                        });
                                    } else if (app.navSelectItem === '4') {
                                        app.showData.splice(app.currentCraftIndex, 1);
                                        app.$nextTick(myMasonry());
                                    }
                                    app.noticeSuccess(res.data.message);
                                    // app.currentCraft=null;
                                    app.editDrawerVisible = false;
                                } else {
                                    app.noticeWarnings(res.data.error);
                                }
                            })
                            .catch(err => {
                                app.noticeError('服务器繁忙！请稍后重试');
                            });

                    },
                    //从已删除恢复
                    restore(item) {
                        axios.post('/user/myCrafts/restore/' + item.id)
                            .then(res => {
                                app.noticeSuccess(res.data.message);
                                app.removeDelItem(item);
                            })
                            .catch(err => {
                                app.noticeError('服务器繁忙！请稍后重试');
                                console.log(err);
                            })
                    },
                    //气泡框取消按钮 关闭删除气泡确认框
                    closeDelPopover(item) {
                        this.$refs['popover-' + item.id][0].doClose();
                    },

                    //气泡框确认按钮操作
                    delConfirm(item) {
                        this.closeDelPopover(item);
                        //第一参数要删除数据的id
                        // 第二个参数数据属于那个表
                        //第三个参数 是否软删除 1为软删除 2为直接删除
                        let params = new FormData();
                        switch (this.navSelectItem) {
                            case '1':
                                params.append('id', item.id);
                                params.append('table', 'goods');
                                params.append('isSoftDel', '1');
                                break;
                            case '3':
                                params.append('id', item.id);
                                params.append('table', 'goods_putaways');
                                params.append('isSoftDel', '2');
                                break;
                            case '4':
                                params.append('id', item.id);
                                params.append('table', 'goods_putaways');
                                params.append('isSoftDel', '2');
                                break;
                            case '5':
                                params.append('id', item.id);
                                params.append('table', 'goods');
                                params.append('isSoftDel', '2');
                                break;
                        }
                        axios.post('/user/myCrafts/delete', params)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    app.noticeSuccess(res.data.message);
                                    app.removeDelItem(item);
                                }else{
                                    app.noticeWarnings(res.data.error);
                                }
                            })
                            .catch(err => {
                                app.noticeError('服务器错误，请刷新重试！');
                                console.log(err);
                            })
                    },

                    //上架作品 提交
                    putAwaySubmit() {
                        this.$refs['newCraft'].validate((result) => {
                            app.dataCheck = result;
                        });
                        if (!this.dataCheck) {
                            this.noticeWarning('数据填写不规范');
                            return false;
                        }
                        if (this.dialogImageUrl === '') {
                            this.noticeWarning('请选择文件');
                            return false;
                        }
                        this.$refs['add-upload'].submit();
                    },
                    //上架作品提交
                    upload(params) {
                        let data = new FormData();
                        data.append('file', params.file);
                        data.append('name', this.newCraft.name);
                        data.append('author', this.newCraft.author);
                        data.append('intro', this.newCraft.intro);
                        let size = this.newCraft.length + "×" + this.newCraft.width;

                        if (this.newCraft.genre_id == 4 && typeof this.newCraft.height !== "undefined") {
                            if (this.newCraft.height !== '') {
                                size = size + "×" + this.newCraft.height;
                            }
                        }
                        size = size + " cm";
                        data.append('size', size);
                        data.append('price', this.newCraft.price);
                        data.append('genre_id', this.newCraft.genre_id);
                        data.append('creator_id', this.creator.id);

                        axios.post('/user/myCrafts', data)
                            .then(res => {
                                if (res.data.status_code === 200) {
                                    app.noticeSuccess(res.data.message);
                                    if (app.navSelectItem === '3') {
                                        app.allDataOfCurrentSelectItem.push(res.data.data);
                                        let p = app.currentPage;
                                        app.search();
                                        app.currentPage = p;
                                        app.pageChanged(p);
                                    }
                                } else {
                                    res.data.error.forEach(item => {
                                        setTimeout(() => {
                                            app.noticeError(item);
                                        }, 1)
                                    });
                                }
                            })
                            .catch(err => {
                                app.noticeError("服务器错误，请刷新重试！");
                                console.log(err);
                            })

                    },
                    //文件改变
                    handleFileChange(file, fileList) {
                        if (file.raw.type === 'image/jpeg' || file.raw.type === 'image/png') {
                            if (file.size / 1024 / 1024 > 2) {
                                this.clearFiles();
                                this.$message.warning('图片大小不能超过2MB')
                            } else {
                                if (fileList.length > 1) {
                                    fileList.splice(0, 1);
                                }
                                this.dialogImageUrl = file.url;
                            }
                        } else {
                            console.log(file, fileList);
                            this.clearFiles();
                            this.$message.warning('只能上传jpg/png格式图片')
                        }
                    },
                    //图片移除
                    handleRemove(file, fileList) {
                        this.dialogImageUrl = "";
                        console.log(file, fileList);
                    },
                    //打开大图Dialog
                    handlePictureCardPreview(file) {
                        this.dialogVisible = true;
                    },
                    //清空文件列表
                    clearFiles() {
                        this.$refs['add-upload'].clearFiles();
                    },


                    //切换选项卡
                    navSelect(key, keyPath) {
                        this.currentPage = 1; //设置当前页为第一页
                        this.searchGenreId = null; //设置搜索条件1为空
                        this.searchText = ''; //设置搜索条件2为空
                        this.navSelectItem = key;
                        //根据选择选项卡设置获取数据 Url
                        let url = '';
                        switch (this.navSelectItem) {
                            case "1":
                                url = '/user/myCrafts/getCrafts/'; //获取我的作品
                                break;
                            case "2":
                                url = '/user/myCrafts/getSold/';   //已售
                                break;
                            case "3":
                                url = '/user/myCrafts/getPutAwayApply/'; //等待审核 
                                break;
                            case "4":
                                url = '/user/myCrafts/getBeRejected/';    //审核未通过
                                break;
                            case "5":
                                url = '/user/myCrafts/getDeleted/';     //已删除
                                break;
                            default:
                                url = '/user/myCrafts/getCrafts/';
                                break;
                        }
                        //清空数据
                        // this.allDataOfCurrentSelectItem.splice(0, this.allDataOfCurrentSelectItem.length);
                        // this.allShowData.splice(0, this.allShowData.length);
                        // this.showData.splice(0, this.showData.length);
                        this.allDataOfCurrentSelectItem = [];
                        this.allShowData = [];
                        this.showData = [];
                        //获取数据
                        axios.get(url + this.creator.id)
                            .then(res => {
                                res.data.data.forEach((item, index) => {
                                    app.allDataOfCurrentSelectItem.push(item);
                                    app.allShowData.push(item);
                                    app.showData.push(item);
                                });
                                for (let i = app.showData.length; i >= app.pageSize; i--) {
                                    app.showData.splice(i, 1);
                                }
                                app.$nextTick(function () {
                                    executeMyMasonry()
                                });
                            })
                            .catch(err => {
                                app.noticeError('服务器繁忙，请稍后再试');
                            });
                    },

                    //删除后 将删除的数据项从数组中移除，
                    removeDelItem(item) {
                        for (let i = 0; i < this.allDataOfCurrentSelectItem.length; i++) {

                            if (item.id === this.allDataOfCurrentSelectItem[i].id) {
                                this.allDataOfCurrentSelectItem.splice(i, 1);
                            }
                            if (i < this.allShowData.length && item.id === this.allShowData[i].id) {
                                this.allShowData.splice(i, 1);
                            }
                            if (i < this.showData.length && item.id === this.showData[i].id) {
                                this.showData.splice(i, 1);
                            }
                        }
                        // 如果allShowData中的数据条数大于pageSize时则加载第PageSize条数据到要渲染的数据数组中
                        let start = this.pageSize * (this.currentPage - 1) + this.showData.length;
                        if (start < this.allShowData.length) {
                            this.showData.push(this.allShowData[start]);
                        }
                        app.$nextTick(myMasonry());
                    },

                    //搜索
                    search() {
                        this.currentPage = 1;
                        this.allShowData.splice(0, this.allShowData.length);
                        this.showData.splice(0, this.showData.length);
                        if (!this.searchGenreId && this.searchGenreId !== 0) {
                            this.allDataOfCurrentSelectItem.forEach((item, index) => {
                                if (!this.searchText) {
                                    this.allShowData.push(item);
                                } else if (item.name.indexOf(this.searchText) > -1) {
                                    this.allShowData.push(item);
                                }
                            });
                        } else {
                            this.allDataOfCurrentSelectItem.forEach((item, index) => {
                                if (item.genre_id === this.searchGenreId) {
                                    if (!this.searchText) {
                                        this.allShowData.push(item);
                                    } else if (item.name.indexOf(this.searchText) != -1) {
                                        this.allShowData.push(item);
                                    }
                                }
                            })
                        }
                        for (let i = 0; i < this.pageSize; i++) {
                            if (i >= this.allShowData.length) {
                                break;
                            }
                            this.showData.push(this.allShowData[i]);
                        }
                        this.$nextTick(executeMyMasonry());
                    },
                    //当前页改变
                    pageChanged(currentPage) {
                        this.showData.splice(0, this.showData.length);
                        let start = (currentPage - 1) * this.pageSize;
                        for (let i = 0; i < this.pageSize; i++) {
                            if (start >= this.allShowData.length) {
                                break;
                            }
                            this.showData.push(this.allShowData[start]);
                            start++;
                        }
                        this.$nextTick(executeMyMasonry());
                        this.$nextTick(up());
                    },

                    detail(item) {
                        if(this.navSelectItem==='1'){
                            let url = this.wwwPath + '/detail/' + item.id;
                            window.open(url, '_blank');
                        }
                    },
                    //错误通知
                    noticeError(error) {
                        this.$notify.error({
                            title: '错误',
                            message: error
                        })
                    },
                    //成功通知
                    noticeSuccess(message) {
                        this.$notify.success({
                            title: '提示',
                            message: message
                        })
                    },
                    //警告提示
                    noticeWarning(message) {
                        this.$notify.warning({
                            title: '提示',
                            message: message
                        })
                    },
                    //多警告提示
                    noticeWarnings(messages) {
                        messages.forEach((item, index) => {
                            setTimeout(function () {
                                this.$notify.warning({
                                    title: '提示',
                                    message: item
                                })
                            }, 1);
                        });
                    }
                },
            });


        let finalH1 = 0;
        let finalH2 = 0;
        let finalH3 = 0;
        let finalH4 = 0;
        let topH = 25;
        //根据每个item高度设置瀑布布局
        let myMasonry = function () {
            let h1 = 25;
            let h2 = 25;
            let h3 = 25;
            let h4 = 25;
            setTimeout(function () {
                $(".masonry-item").each(function (index, el) {
                    if (h1 <= h2 && h1 <= h3 && h1 <= h4) {
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item1");
                        $(this).css({"top": h1 + "px", 'opacity': 1});
                        h1 += $(this).height() + topH;
                    } else if (h2 < h1 && h2 <= h3 && h2 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item2");
                        $(this).css({"top": h2 + "px", 'opacity': 1});
                        h2 += $(this).height() + topH;
                    } else if (h3 < h1 && h3 < h2 && h3 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item4');
                        $(this).addClass("item3");
                        $(this).css({"top": h3 + "px", 'opacity': 1});
                        h3 += $(this).height() + topH;
                    } else {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).addClass("item4");
                        $(this).css({"top": h4 + "px", 'opacity': 1});
                        h4 += $(this).height() + topH;
                    }
                });
                setHeight();
            }, 300)
        };

        let setHeight = function () {
            finalH1 = 0;
            finalH2 = 0;
            finalH3 = 0;
            finalH4 = 0;
            $('.item1').each(function () {
                finalH1 += $(this).height() + topH;
            });
            $('.item2').each(function () {
                finalH2 += $(this).height() + topH;
            });
            $('.item3').each(function () {
                finalH3 += $(this).height() + topH;
            });
            $('.item4').each(function () {
                finalH4 += $(this).height() + topH;
            });
            let maxH = Math.max(finalH1, finalH2, finalH3, finalH4) + 80;
            $(".masonry").css({"height": (maxH + topH) + "px"});
        };

        //
        function executeMyMasonry() {
            $('#masonry').css({"transition": "0s", "opacity": 0});
            // setTimeout(function () {
            myMasonry();
            // }, );
            setTimeout(function () {
                $('#masonry').css({"transition": '1.5s', "opacity": 1});
            }, 200);
        }

        //窗口大小改变后重新设置瀑布布局
        window.onresize = function () {
            myMasonry();
        };

        //窗口滚动时重新设置瀑布布局
        $(document).scroll(function () {
            let scrollTop = $(document).scrollTop;
            //每滚动200px就对瀑布布局进行设置
            if ((scrollTop % 350) > 300 || (scrollTop >= 0 && scrollTop < 10)) {
                myMasonry();
            }
        });

        $(document).ready(function () {
            setTimeout(function () {
                executeMyMasonry();
            }, 1);
        });

        function img_view() {
            setTimeout(function () {
                $('.avatar').on({
                    'mouseenter': function () {
                        $('.avatar-uploader .el-icon-picture').show();
                    },
                    "mouseleave": function () {
                        $('.avatar-uploader .el-icon-picture').hide();
                    }
                });
                $(".avatar-uploader .el-icon-picture").on({
                    'mouseenter': function () {
                        $(this).show();
                    },
                    "mouseleave": function () {
                        $(this).hide();
                    }
                });
            })
        }


    </script>

@endsection
