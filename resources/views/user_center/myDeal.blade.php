@extends('user_center.master')

@section('title')
    我的交易-个人中心
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/user_center/myOrder.css')}}">
@endsection

@section('info-title')
    我的交易
@endsection

@section('content')
    <div class="nav-box">
        <el-menu
            default-active="1"
            class="el-menu-demo"
            mode="horizontal"
            @select="navSelect"
            background-color="#394046"
            text-color="white"
            ref="navSelect"
            active-text-color="#ffd04b">
            <el-menu-item index="1">正在进行</el-menu-item>
            <el-menu-item index="2">客户申请退款/退货</el-menu-item>
            <el-menu-item index="3">已完成</el-menu-item>
            <el-menu-item index="4">已关闭</el-menu-item>
        </el-menu>
    </div>

    <section class="orders-section" v-if="showOrders.length!==0">

        <el-row v-for="(item,index) in showOrders" :key="item.id"
                class="order-box-row" :gutter="50">
            <el-row>
                <el-col :span="24" class="orderId">
                    <span> <b>订单号：</b></span>
                    <span>@{{ item.id }}</span>
                </el-col>
            </el-row>
            <el-row style="padding-top: 20px">
                <el-col :span="5" class="order-info-item-col img">
                    <a @click="detail(item)">
                        <el-image :src="'{{asset('/')}}'+item.imgUrl">
                            <div slot="error" class="image-slot">
                                图片加载失败或该作品已删除
                            </div>
                        </el-image>
                    </a>
                </el-col>
                <el-col :span="5" class="order-info-item-col goods-info">
                    <div class="goods-info-item">@{{ item.goods_name }}</div>
                    <div class="goods-info-item">@{{ item.author }}</div>
                    <div class="goods-info-item"> @{{ item.size }}</div>
                    <div class="goods-info-item">￥ @{{ item.price }}</div>
                </el-col>
                <el-col :span="6" class="order-info-item-col address-info">
                    <div class="address-info-item">@{{ item.name }}</div>
                    <div class="address-info-item">@{{ item.phone }}</div>
                    <div class="address-info-item">
                        @{{ item.province }} |
                        @{{ item.city }} |
                        @{{ item.county }} |
                        @{{ item.region }}
                    </div>
                    <div class="address-info-item">@{{ item.detail_address }}</div>
                </el-col>

                {{--                //订单状态--}}
                <el-col :span="4" class="order-info-item-col status">

                    <el-tag v-if="item.status===1"
                            type="success"
                            effect="plain">
                        客户已付款
                    </el-tag>

                    <el-tag v-if="item.status===100"
                            type="warning"
                            effect="plain">
                        客户申请退款
                    </el-tag>

                    <el-tag v-if="item.status===2"
                            type="success"
                            effect="plain">
                        已发货
                    </el-tag>

                    <el-tag v-if="item.status===200"
                            type="warning"
                            effect="plain">
                        客户申请退货退款
                    </el-tag>

                    <el-tag v-if="item.status===4"
                            type="success"
                            effect="plain">
                        订单已完成
                    </el-tag>

                    <el-tag v-if="item.status===5"
                            type="info"
                            effect="plain">
                        订单已关闭
                    </el-tag>

                </el-col>

                {{--                //操作--}}
                <el-col :span="3" class="order-info-item-col options">

                    <section v-if="item.status===1">
                        <div>
                            <el-button size="small" @click="delivered(item,index)">已发货</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>

                    <section v-if="item.status===100">
                        <div>
                            <el-button size="small" @click="confirmRefunded(item,index)">确认退款</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>


                    <section v-if="item.status===200">
                        <div>
                            <el-button size="small" @click="confirmRefundedAndReturn(item,index)">确认退款退货</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>

                    <section
                        v-if="item.status===2 || item.status===4 || item.status===5" class="done">
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>
                </el-col>
            </el-row>
        </el-row>
    </section>

    <section v-else class="noData">
        暂无数据
    </section>

@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            created() {
                this.navSelect('1', '1');
            },
            data() {
                return {
                    activeItem: [false, false, false, false, false, false, false, false, true, false, false],
                    user:@json(Auth::user()),  //must has
                    showOrders: [],
                    ordersOfOngoing: (@json($data)).ordersOfOngoing, //正在进行订单
                    ordersOfRefundAndReturn: (@json($data)).ordersOfRefundAndReturn,  //申请退货/退款订单
                    ordersOfDone: (@json($data)).ordersOfDone,//已完成订单
                    ordersOfClose: (@json($data)).ordersOfClose, //已关闭订单
                };
            },
            methods: {
                //导航选项卡切换
                navSelect(key, keyPath) {
                    switch (key) {
                        case '1':
                            this.showOrders = this.ordersOfOngoing;
                            break;
                        case '2':
                            this.showOrders = this.ordersOfRefundAndReturn;
                            break;
                        case '3':
                            this.showOrders = this.ordersOfDone;
                            break;
                        case '4':
                            this.showOrders = this.ordersOfClose;
                            break;
                    }
                },

                //已发货
                delivered(item, index) {
                    let data = new FormData();
                    data.append('id', item.id);
                    axios.post('/user/myDeal/delivered', data)
                        .then(res => {
                            if (res.data.status_code === 200) {
                                item.status = 2;
                                app.ordersOfOngoing.splice(index, 1, item);
                                app.noticeSuccess(res.data.message);
                            } else {
                                app.noticeWarnings(res.data.error);
                            }
                        })
                        .catch(err => {
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后再试');
                        })
                },
                //确认退款
                confirmRefunded(item, index) {
                    let data = new FormData();
                    data.append('id', item.id);
                    data.append('goods_id',item.goods_id);
                    axios.post('/user/myDeal/confirmRefunded', data)
                        .then(res => {
                            item.status = 5;
                            app.ordersOfClose.push(item);
                            app.ordersOfRefundAndReturn.splice(index, 1);
                            app.noticeSuccess(res.data.message);
                        })
                        .catch(err => {
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后再试');
                        })
                },

                //确认退款退货
                confirmRefundedAndReturn(item, index) {
                    let data = new FormData();
                    data.append('id', item.id);
                    data.append('goods_id',item.goods_id);
                    axios.post('/user/myDeal/confirmRefundedAndReturn', data)
                        .then(res => {
                            item.status = 5;
                            app.ordersOfClose.push(item);
                            app.ordersOfRefundAndReturn.splice(index, 1);
                            app.noticeSuccess(res.data.message);
                        })
                        .catch(err => {
                            console.log(err);
                            app.noticeError('服务器繁忙，请稍后再试');
                        })
                },

                orderDetail(item) {
                    let url = '/order/detail/' + item.id;
                    window.open(url, '_blank');
                },
                detail(item) {
                    let url = '/detail/' + item.goods_id;
                    window.open(url, '_blank');
                },

                //错误通知
                noticeError(error) {
                    this.$notify.error({
                        title: '错误',
                        message: error
                    })
                },
                //成功通知
                noticeSuccess(message) {
                    this.$notify.success({
                        title: '提示',
                        message: message
                    })
                },
                //警告提示
                noticeWarning(message) {
                    this.$notify.warning({
                        title: '提示',
                        message: message
                    })
                },
                //多警告提示
                noticeWarnings(messages) {
                    messages.forEach((item, index) => {
                        setTimeout(function () {
                            this.$notify.warning({
                                title: '提示',
                                message: item
                            })
                        }, 1);
                    });
                }

            }
        })
    </script>

    <style>

    </style>
@endsection
