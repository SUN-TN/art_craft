<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <title>@yield('title')</title>
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="{{asset('/elementUI/index.js')}}"></script>
    <script src="{{asset('/js/axios.min.js')}}"></script>
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/elementUI/index.css')}}">
    @yield('link')
</head>
<body>

<div id="app">

    <el-header class="header">
        <el-row>
            <el-col :sm="4" :xs="4" :md="4" :offset="3">
                <a href="/"><span class="home">CRAFTS</span></a>
            </el-col>
            <el-col :sm="4" :xs="4" :md="4" :offset="13">
                <el-popover
                        placement="bottom"
                        width="200"
                        trigger="hover">
                    <div>
                        <div class="user-item"><h4>{{Auth::user()->name}}</h4></div>
                        <div class="user-item">
                            <a href="/user/changePassword">
                                <el-button>修改密码</el-button>
                            </a>
                        </div>
                        <div class="user-item">
                            <el-form action="/entry/logout">
                                <el-button native-type="submit">退出登录</el-button>
                            </el-form>
                        </div>
                    </div>
                    <el-image class="user-avatar" slot="reference" :src="'{{asset('/')}}'+user.imgUrl">
                        <el-avatar slot="error" :size="45">user</el-avatar>
                    </el-image>
                </el-popover>
            </el-col>
        </el-row>
    </el-header>

    <div class="main">
        <el-row>
            <el-col :sm="3" :xs="3" :md="3" :offset="3">
                <el-menu
                        class="el-menu-vertical-demo">
                    <a href="/user/myinfo">
                        <el-menu-item index="1" :class="{active:activeItem[0]}">我的资料</el-menu-item>
                    </a>
                    <a href="/user/myStar">
                        <el-menu-item index="2" :class="{active:activeItem[1]}">我的收藏</el-menu-item>
                    </a>
                    <a href="/user/myCart">
                        <el-menu-item index="3" :class="{active:activeItem[2]}">我的购物车</el-menu-item>
                    </a>
                    <a href="/user/myOrder">
                        <el-menu-item index="4" :class="{active:activeItem[3]}">我的订单</el-menu-item>
                    </a>
                    <a href="/user/myAddress">
                        <el-menu-item index="5" :class="{active:activeItem[4]}">我的地址</el-menu-item>
                    </a>
                    <a v-if="{{Auth::user()->is_sale}}" href="/user/myCreatorInfo">
                        <el-menu-item index="7" :class="{active:activeItem[6]}">我的创作者信息</el-menu-item>
                    </a>
                    <a v-else href="/user/beCreator">
                        <el-menu-item index="6" :class="{active:activeItem[5]}">申请成为创作者</el-menu-item>
                    </a>
                    <a v-if="{{Auth::user()->is_sale}}" href="/user/myCrafts">
                        <el-menu-item index="8" :class="{active:activeItem[7]}">我的作品</el-menu-item>
                    </a>
                    <a v-if="{{Auth::user()->is_sale}}" href="/user/myDeal">
                        <el-menu-item index="9" :class="{active:activeItem[8]}">我的交易</el-menu-item>
                    </a>
                    <a href="/user/changePassword">
                        <el-menu-item index="10" :class="{active:activeItem[9]}">修改密码</el-menu-item>
                    </a>
                    <a href="/user/opinion">
                        <el-menu-item index="11" :class="{active:activeItem[10]}">意见反馈</el-menu-item>
                    </a>
                </el-menu>
            </el-col>

            <el-col :sm="14" :xs="14" :md="14" :offset="1" class="main-panel">
                <el-row class="info-title">
                    <span>@yield('info-title')</span>
                </el-row>
                @yield('content')
            </el-col>
        </el-row>
    </div>


    <el-footer class="footer">
        {{--<el-row>--}}
            {{--<el-col :md="18" :offset="3">--}}
                @yield('footer')
            {{--</el-col>--}}
        {{--</el-row>--}}
    </el-footer>


    <div class="el-backtop" onclick="up()">
        <el-tooltip class="item" effect="dark" content="回到顶部" placement="top">
            <p>UP</p>
        </el-tooltip>
    </div>
</div>


<script>
    $(document).ready(function () {
        axios.get('/unlogout')
            .then(res => {
                if (res.data.unlogout === true) {
                    app.$notify.warning({
                        title: '提示',
                        message: '请先登出此账号！或使用其他客户端'
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });

        window.addEventListener("scroll", function (e) {
            let top = $(document).scrollTop();
            $('.el-menu-vertical-demo').css({'margin-top': top + 'px'});
        });
    });

    function up() {
        var gotoTop = function () {
            var currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
            currentPosition -= 10;
            if (currentPosition > 0) {
                window.scrollTo(0, currentPosition);
            }
            else {
                window.scrollTo(0, 0);
                clearInterval(timer);
                timer = null;
            }
        };
        var timer = setInterval(gotoTop, 1);
    }
</script>


<style>
    html {
        overflow-y: scroll;
    }

    /*:root {*/
    /*overflow-y: auto;*/
    /*overflow-x: auto;*/
    /*}*/

    body, html #app {
        padding: 0;
        margin: 0;
        min-width: 1400px;
        overflow: visible;
        transition: 2s;
    }

    a {
        /*color: black;*/
        text-decoration: none;
    }

    .home {
        color: black;
    }

    #app {
        width: 100%;
    }

    .header {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        border-bottom: #55555510 solid 1px;
        background-color: white;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        z-index: 10;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.1), 0 2px 2px 0 rgba(0, 0, 0, 0.1);
    }

    .header span {
        line-height: 56px;
        font-size: 2em;
        cursor:pointer;
    }

    .el-avatar:hover {
        cursor: pointer;
    }

    .user-avatar {
        width: 45px;
        height: 45px;
        border-radius: 50px;
        margin-top: 5px;
    }

    .user-item {
        padding-top: 5px;
        text-align: center;
    }

    .user-item button {
        width: 100%;
        border: none;
    }

    .main {
        display: block;
        margin-top: 80px;
        z-index: 1;
        min-width: 1250px;
        /*min-height: 700px;*/
        height: auto;
        /*min-height: 850px;*/
        overflow: visible;
    }

    .el-menu-vertical-demo {
        transition: 0.5s;
        border-radius: 10px;
        background-color: #2b3237;
    }

    .el-menu-item {
        border-radius: 5px;
        transition: 0.5s;
        color: white;
    }

    .el-menu-item:hover {
        background-color: #394046;
    }
    .el-menu-item:focus{
        color: white;
    }

    .info-title {
        width: 100%;
        text-align: center;
        height: 56px;
        line-height: 56px;
        color: white;
        background-color: #2b3237;
        border-bottom: 2px solid #FFD04B;
        font-size: 1.2em;
        font-weight: bold;
        border-radius: 5px;
    }

    .footer {
        width: 100%;
        position: fixed;
        bottom: 0;
        background-color: white;
        z-index: 10;
        border-top: #55555510 1px solid;
    }

    .footer .el-row .el-col {
        text-align: center;
    }

    .el-backtop,.el-backtop:hover {
        right: 40px;
        bottom: 100px;
        background-color: #2a3237;
        color: white;;
    }

    .el-backtop a {
        background-color: #2a3237;
        color: white;
    }

    .active {
        color: #FFD04B;
        /*background-color: #434A50;*/
        background-color: #394046;
        border-bottom: solid 2px #FFD04B;
        font-size: 1em;
    }

</style>

@yield('js_css')
{{--<script src="{{asset('/js/require.min.js')}}"></script>--}}

</body>
</html>
