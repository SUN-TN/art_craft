@extends('user_center.master')

@section('title')
    我的订单-个人中心
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/user_center/myOrder.css')}}">
@endsection


@section('info-title')
    我的订单
@endsection

@section('content')
    <div class="nav-box">
        <el-menu
            default-active="1"
            class="el-menu-demo"
            mode="horizontal"
            @select="navSelect"
            background-color="#394046"
            text-color="white"
            ref="navSelect"
            active-text-color="#ffd04b">
            <el-menu-item index="1">等待付款</el-menu-item>
            <el-menu-item index="2">正在进行</el-menu-item>
            <el-menu-item index="3">已完成</el-menu-item>
            <el-menu-item index="4">已关闭</el-menu-item>
        </el-menu>
    </div>

    <section class="orders-section" v-if="showOrders.length!==0">

        <el-row v-for="(item,index) in showOrders" :key="item.id"
                class="order-box-row" :gutter="50">
            <el-row>
                <el-col :span="24" class="orderId">
                    <span> <b>订单号：</b></span>
                    <span>@{{ item.id }}</span>
                </el-col>
            </el-row>
            <el-row style="padding-top: 20px">
                <el-col :span="5" class="order-info-item-col img">
                    <a @click="detail(item)">
                        <el-image :src="'{{asset('/')}}'+item.imgUrl">
                            <div slot="error" class="image-slot">
                                图片加载失败或该作品已删除
                            </div>
                        </el-image>
                    </a>
                </el-col>
                <el-col :span="5" class="order-info-item-col goods-info">
                    <div class="goods-info-item">@{{ item.goods_name }}</div>
                    <div class="goods-info-item">@{{ item.author }}</div>
                    <div class="goods-info-item"> @{{ item.size }}</div>
                    <div class="goods-info-item">￥ @{{ item.price }}</div>
                </el-col>
                <el-col :span="6" class="order-info-item-col address-info">
                    <div class="address-info-item">@{{ item.name }}</div>
                    <div class="address-info-item">@{{ item.phone }}</div>
                    <div class="address-info-item">
                        @{{ item.province }} |
                        @{{ item.city }} |
                        @{{ item.county }} |
                        @{{ item.region }}
                    </div>
                    <div class="address-info-item">@{{ item.detail_address }}</div>
                </el-col>
                <el-col :span="4" class="order-info-item-col status">
                    <el-tag v-if="item.status===0"
                            type="success"
                            effect="plain">
                        @{{ clock[index] }} 后取消
                    </el-tag>

                    <el-tag v-if="item.status===1"
                            type="success"
                            effect="plain">
                        已付款
                    </el-tag>

                    <el-tag v-if="item.status===100"
                            type="warning"
                            effect="plain">
                        已申请退款
                    </el-tag>

                    <el-tag v-if="item.status===2"
                            type="success"
                            effect="plain">
                        卖家已发货
                    </el-tag>

                    <el-tag v-if="item.status===200"
                            type="warning"
                            effect="plain">
                        已申请退货退款
                    </el-tag>

                    <el-tag v-if="item.status===4"
                            type="success"
                            effect="plain">
                        订单已完成
                    </el-tag>

                    <el-tag v-if="item.status===5"
                            type="info"
                            effect="plain">
                        订单已关闭
                    </el-tag>

                </el-col>
                <el-col :span="3" class="order-info-item-col options">
                    <section v-if="item.status===0">
                        <div>
                            <el-button size="small" @click="pay(item)">去支付</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="cancelOrder(item,index)">取消订单</el-button>
                        </div>
                    </section>

                    <section v-if="item.status===1">
                        <div>
                            <el-button size="small" @click="applyRefund(item,index)">申请退款</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>

                    <section v-if="item.status===2">
                        <div>
                            <el-button size="small" @click="confirmReceipt(item,index)">确认收货</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="applyRefundAndReturn(item,index)">申请退货退款</el-button>
                        </div>
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>

                    <section
                        v-if="item.status===4 || item.status===5
                        || item.status===100 || item.status===200" class="done">
                        <div>
                            <el-button size="small" @click="orderDetail(item)">查看详情</el-button>
                        </div>
                    </section>
                </el-col>
            </el-row>
        </el-row>
    </section>

    <section v-else class="noData">
        暂无数据
    </section>


@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    activeItem: [false, false, false, true, false, false, false, false, false, false, false],
                    user:@json(Auth::user()),  //must has
                    showOrders: [],
                    orderOfWaitPay: (@json($data)).ordersOfWaitPay,     //等待支付订单
                    ordersOfOngoing: (@json($data)).ordersOfOngoing,     //正在进行订单
                    ordersOfDone: (@json($data)).ordersOfDone,          //已完成订单
                    ordersOfClose: (@json($data)).ordersOfClose,       //已关闭订单
                    clock: [],//未付款订单定时器标签显示内容数组
                };
            },
            methods: {
                //导航选项卡切换
                navSelect(key, keyPath) {
                    switch (key) {
                        case '1':
                            this.showOrders = this.orderOfWaitPay;
                            this.openTimer();
                            break;
                        case '2':
                            this.showOrders = this.ordersOfOngoing;
                            this.closeTimer();
                            break;
                        case '3':
                            this.showOrders = this.ordersOfDone;
                            this.closeTimer();
                            break;
                        case '4':
                            this.showOrders = this.ordersOfClose;
                            this.closeTimer();
                            break;
                    }
                },

                //取消订单
                cancelOrder(item, index) {
                    let data = new FormData();
                    data.append('id', item.id);
                    axios.post('/user/myOrder/cancelOrder', data)
                        .then(res => {
                            app.ordersOfClose.push(item);
                            app.orderOfWaitPay.splice(index, 1);
                            app.noticeSuccess(res.data.message);
                        })
                        .catch(err => {
                            console.log(err);
                            app.noticeError('取消订单失败,服务器繁忙，请稍后再试！');
                        })

                },

                //申请退款
                applyRefund(item, index) {
                    this.$confirm('确定要提交此申请吗？')
                        .then(() => {
                            let data = new FormData();
                            data.append('id', item.id);
                            data.append('goods_id', item.goods_id);
                            axios.post('/order/applyRefund', data)
                                .then(res => {
                                    if (res.data.status_code === 200) {
                                        item.status = 100;
                                        app.ordersOfOngoing.splice(index, 1, item);
                                        app.noticeSuccess(res.data.message);
                                    } else {
                                        app.noticeWarnings(res.data.error);
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                    app.noticeError('服务器错误，请刷新重试！');
                                })
                        })
                        .catch();
                },
                //确认收货
                confirmReceipt(item, index) {
                    this.$confirm('确定货物已收到了吗？')
                        .then(() => {
                            let data = new FormData();
                            data.append('id', item.id);
                            axios.post('/user/myOrder/confirmReceipt', data)
                                .then(res => {
                                        item.status = 4;
                                        app.ordersOfDone.push(item);
                                        app.ordersOfOngoing.splice(index, 1);
                                        app.noticeSuccess(res.data.message);
                                })
                                .catch(err => {
                                    console.log(err);
                                    app.noticeError('服务器错误，请刷新重试！');
                                })
                        })
                        .catch();
                },
                //申请退货退款
                applyRefundAndReturn(item, index) {
                    this.$confirm('确定要提交此申请吗？')
                        .then(() => {
                            let data = new FormData();
                            data.append('id', item.id);
                            data.append('goods_id', item.goods_id);
                            axios.post('/order/applyRefundAndReturn', data)
                                .then(res => {
                                    if (res.data.status_code === 200) {
                                        item.status = 200;
                                        app.ordersOfOngoing.splice(index, 1, item);
                                        app.noticeSuccess(res.data.message);
                                    } else {
                                        app.noticeWarnings(res.data.error);
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                    app.noticeError('服务器错误，请刷新重试！');
                                })
                        })
                        .catch();
                },

                //开启未支付订单定时器
                openTimer() {
                    let now = new Date();
                    this.orderOfWaitPay.forEach((item, index) => {
                        let orderD = new Date(item.created_at);
                        orderD.setMinutes(orderD.getMinutes() + 15);
                        let hasTime = Math.floor((orderD - now) / 1000);
                        app.clock.push('');
                        myClock[index] = setInterval(function () {
                            let minutes = Math.floor(hasTime / 60);
                            let seconds = (hasTime % 60);
                            if (minutes > 0) {
                                app.clockText = minutes + '分' + seconds + '秒';
                            } else {
                                app.clockText = seconds + '秒';
                            }
                            // let text = Math.floor(hasTime / 60) + '分' + (hasTime % 60) + '秒';
                            // app.$set(app.clock, index, text);
                            hasTime--;
                            if (hasTime <= 0) {
                                item.status = 5;
                                app.noticeSuccess('您的订单已超时');
                                app.cancelOrder(item, index);
                                // app.ordersOfClose.push(item);
                                // app.orderOfWaitPay.splice(index, 1);
                                clearInterval(myClock[index]);
                            }
                        }, 1000);
                    })
                },
                //清除定时器
                closeTimer() {
                    myClock.forEach((item, index) => {
                        clearInterval(item);
                    })
                },
                //作品详情
                detail(item) {
                    let url = '{{asset('/detail')}}' + '/' + item.goods_id;
                    window.open(url, "_blank");
                    // window.location = '/detail/' + item.goods_id;
                },
                //订单详情
                orderDetail(item) {
                    let url = '{{asset('/order/detail')}}' + '/' + item.id;
                    window.open(url, '_blank');
                },
                //支付页面
                pay(item) {
                    let url = '{{asset('/order/pay')}}' + '/' + item.id;
                    window.open(url, "_blank");
                    // window.location = '/order/pay/' + item.id;
                },
                //成功通知
                noticeSuccess(message) {
                    this.$notify.success({
                        title: '提示',
                        message: message
                    })
                },
                //错误通知
                noticeError(error) {
                    this.$notify.error({
                        title: '错误',
                        message: error
                    })
                },

                //警告提示
                noticeWarning(message) {
                    this.$notify.warning({
                        title: '提示',
                        message: message
                    })
                },
                //多警告提示
                noticeWarnings(messages) {
                    messages.forEach((item, index) => {
                        setTimeout(function () {
                            this.$notify.warning({
                                title: '提示',
                                message: item
                            })
                        }, 1);
                    });
                }
            }
        });

        var myClock = [];  //定时器全局数组
        $(document).ready(function () {
            setTimeout(function () {
                app.navSelect('1', '1');
            }, 1)
        })
    </script>

@endsection
