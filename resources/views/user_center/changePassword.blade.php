@extends('user_center.master')

@section('title')
    修改密码-个人中心
@endsection

@section('info-title')
    修改密码
@endsection

@section('content')
    <el-container class="passwordPanel">
        <el-form action="#">
            <el-input v-model="original_password" placeholder="原密码" show-password></el-input>
            <el-input v-model="password" placeholder="新密码" show-password></el-input>
            <el-input v-model="password_confirmation" placeholder="确认密码" show-password></el-input>
            <el-button class="btn" @click="changePassword" round>修改密码</el-button>
        </el-form>
    </el-container>

@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    activeItem:[false,false,false,false,false,false,false,false,false,true,false],
                    user:@json($data),
                    password:null,
                    password_confirmation:null,
                    original_password:null,
                };
            },
            methods: {

                //修改密码
                changePassword(){
                    let data = new FormData();
                    data.append('password',this.password);
                    data.append('password_confirmation',this.password_confirmation);
                    data.append('original_password',this.original_password);
                    axios.post('/user/changePassword',data)
                        .then(function (res) {
                            if(res.data.status_code == 200){
                                app.$notify.success({
                                    title:"提示",
                                    message:res.data.message
                                })
                            }else{
                                res.data.error.forEach(item => {
                                    setTimeout(function () {
                                        app.$notify.warning({
                                            title:'提示',
                                            message:item
                                        });
                                    },1);
                                });
                            }
                        })
                        .catch(function (err) {
                            app.$notify.error({
                                title:'提示',
                                message:'服务器发生错误，请刷新重试！'
                            });
                            console.log(err);
                        })
                },


            }
        })
    </script>

    <style>

        .passwordPanel {
            /*width: 455px;*/
            /*padding: 20px;*/
            /*margin-top: 260px;*/
            /*margin-left: 50%;*/
            /*transform: translate(-50%, -50%);*/
            /*background: #252525;*/
            /*!* background: #eceaea; *!*/
            /*text-align: center;*/
            /*border-radius: 24px;*/
            /*opacity: 0.9;*/

            width: 410px;
            padding: 20px;
            margin-top: 260px;
            margin-left: 50%;
            transform: translate(-50%, -50%);
            background-color: #111111;
            /* background: #eceaea; */
            text-align: center;
            border-radius: 24px;
            opacity: 0.9;
        }

        .passwordPanel h1 {
            color: white;
            text-transform: uppercase;
            font-weight: 500;
        }

        .passwordPanel input{
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #3498db;
            padding: 14px 10px;
            width: 260px;
            height: 50px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
        }
        .passwordPanel input:hover{
            border: 2px solid #3498db;
        }
        .passwordPanel input:focus{
            width: 300px;
            border-color: #2ecc71;
        }

        .passwordPanel .btn{
            background-color: transparent;
            border: 2px #2ecc71 solid;
            color: white;
            border-radius: 50px;
            padding: 15px 15px;
        }

        .passwordPanel .btn:hover{
            background: #2ecc71;
            color: white;
            border: 2px solid transparent;
        }
    </style>
@endsection
