@extends('user_center.master')

@section('title')
    我的收藏-个人中心
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('css/user_center/myStar.css')}}">
@endsection

@section('info-title')
    我的收藏
@endsection

@section('content')

        <div id="masonry" class="masonry" style="height: auto !important; opacity: 0">
            <div class="masonry-item" id="masonry-item" v-for="(item,index) in stars" :key="item.id">
                <el-image class="crafts-item-img" :src="'{{asset('/')}}'+item.imgUrl" @click="detail(item)"></el-image>
                <div class="crafts-item-info">
                    <div class="crafts-item-name">@{{item.name}}</div>
                    <div class="crafts-item-author">@{{item.author}}</div>
                    <div class="crafts-item-Size">￥@{{item.price}} | @{{item.size}}</div>
                    <div class="crafts-item-options" style="text-align: right;">
                        <el-button size="small" @click="removeStar(item,index)" class="btn_removeStar">移出收藏</el-button>
                    </div>
                </div>
            </div>

            <div v-show="stars === null || stars.length === 0"
                 style="width: 100%;text-align: center;margin-top: 50px;font-size: 2em;"
                 class="noData">
                暂无数据
            </div>
        </div>

@endsection

@section('js_css')
    <script>
        let app = new Vue({
            el: '#app',
            data() {
                return {
                    activeItem: [false, true, false, false, false, false, false, false, false, false, false],
                    user:@json(Auth::user()),  //must has
                    stars:@json($data).stars,
                };
            },
            methods: {
                removeStar(item,index) {
                    let data = new FormData();
                    data.append('id', item.goods_id);
                    axios.post('/detail/removeStar', data)
                        .then(res => {
                            if (res.data.status_code === 200) {
                                app.stars.splice(index,1);
                                app.$nextTick(myMasonry());
                                app.noticeSuccess(res.data.message);
                            } else {
                                app.noticeWarnings(res.data.error);
                            }
                        })
                        .catch(err => {
                            this.noticeError('服务器繁忙，请稍后再试！');
                        })
                },
                detail(item) {
                    let url='{{asset('/detail')}}'+'/'+item.id;
                    window.open(url, "_blank");
                    // window.location = '/detail/' + item.id;
                },
                //成功通知
                noticeSuccess(message) {
                    this.$notify.success({
                        title: '提示',
                        message: message
                    })
                },
                //警告提示
                noticeWarning(message) {
                    this.$notify.warning({
                        title: '提示',
                        message: message
                    })
                },
                //多警告提示
                noticeWarnings(messages) {
                    messages.forEach((item, index) => {
                        setTimeout(function () {
                            this.$notify.warning({
                                title: '提示',
                                message: item
                            })
                        }, 1);
                    });
                }
            }
        });


        let finalH1 = 0;
        let finalH2 = 0;
        let finalH3 = 0;
        let finalH4 = 0;
        let topH = 25;
        //根据每个item高度设置瀑布布局
        let myMasonry = function () {
            let h1 = 25;
            let h2 = 25;
            let h3 = 25;
            let h4 = 25;
            setTimeout(function () {
                $(".masonry-item").each(function (index, el) {
                    if (h1 <= h2 && h1 <= h3 && h1 <= h4) {
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item1");
                        $(this).css({"top": h1 + "px", 'opacity': 1});
                        h1 += $(this).height() + topH;
                    } else if (h2 < h1 && h2 <= h3 && h2 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item3');
                        $(this).removeClass('item4');
                        $(this).addClass("item2");
                        $(this).css({"top": h2 + "px", 'opacity': 1});
                        h2 += $(this).height() + topH;
                    } else if (h3 < h1 && h3 < h2 && h3 <= h4) {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item4');
                        $(this).addClass("item3");
                        $(this).css({"top": h3 + "px", 'opacity': 1});
                        h3 += $(this).height() + topH;
                    } else {
                        $(this).removeClass('item1');
                        $(this).removeClass('item2');
                        $(this).removeClass('item3');
                        $(this).addClass("item4");
                        $(this).css({"top": h4 + "px", 'opacity': 1});
                        h4 += $(this).height() + topH;
                    }
                });
                setHeight();
            }, 500)
        };

       
        let setHeight = function () {
            setTimeout(() => {
                finalH1 = 0;
                finalH2 = 0;
                finalH3 = 0;
                finalH4 = 0;
                $('.item1').each(function () {
                    finalH1 += $(this).height() + topH;
                });
                $('.item2').each(function () {
                    finalH2 += $(this).height() + topH;
                });
                $('.item3').each(function () {
                    finalH3 += $(this).height() + topH;
                });
                $('.item4').each(function () {
                    finalH4 += $(this).height() + topH;
                });
                let maxH = Math.max(finalH1, finalH2, finalH3, finalH4) + 80;
                $(".masonry").css({"height": (maxH + topH) + "px"});
            },200);
        };

        //
        function executeMyMasonry() {
            $('#masonry').css({"transition": "0s", "opacity": 0});
            // setTimeout(function () {
            myMasonry();
            // }, );
            setTimeout(function () {
                $('#masonry').css({"transition": '1.5s', "opacity": 1});
            }, 200);
        }

        //窗口大小改变后重新设置瀑布布局
        window.onresize = function () {
            myMasonry();
        };

        //窗口滚动时重新设置瀑布布局
        $(document).scroll(function () {
            let scrollTop = $(document).scrollTop;
            //每滚动200px就对瀑布布局进行设置
            if ((scrollTop % 350) > 300 || (scrollTop >= 0 && scrollTop < 10)) {
                myMasonry();
            }
        });

        $(document).ready(function () {
            setTimeout(function () {
                executeMyMasonry();
            }, 1);
        });

    </script>

    <style>

    </style>
@endsection
