@extends('user_center.master')

@section('title')
    意见反馈-个人中心
@endsection

@section('info-title')
    意见反馈
@endsection

@section('content')

    <el-form class="form-opinion" :model="formData" :rules="rules" ref="form" :hide-required-asterisk="true">
        <el-form-item label="标题" prop="title">
            <el-input class="form-input-title" v-model="formData.title"></el-input>
        </el-form-item>
        <el-form-item label="内容" prop="content">
            <el-input type="textarea"  class="form-input-content" v-model="formData.content"></el-input>
        </el-form-item>
        <el-form-item label="验证码">
            <el-input  v-model="formData.securityCode">
                <img slot="prepend" src="{{captcha_src('mini')}}" class="codeImg"
                     onclick="this.src='{{captcha_src('mini')}}'+Math.random()">
                <el-button slot="append" @click="refreshCode">刷新验证码</el-button>
            </el-input>
        </el-form-item>
        <el-form-item style="text-align: center">
            <el-button type="primary" @click="submit" >提 交</el-button>
        </el-form-item>
    </el-form>

@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    activeItem:[false,false,false,false,false,false,false,false,false,false,true],
                    user:@json(Auth::user()),  //must has
                    formData:{
                        title:'',
                        content:'',
                        securityCode:'',
                    },
                    rules:{
                        title:[
                            {required:true,message:'标题不能为空',trigger:['blur','change']},
                            {max:20,message:'标题长度在20个字符以内',trigger:['blur','change']}
                        ],
                        content:[
                            {required:true,message:'内容不能为空',trigger:['blur','change']},
                            {min:15,max:255,message:'标题长度在15-255个字符以内',trigger:['blur','change']}
                            ]
                    }

                };
            },
            methods: {
                refreshCode(){
                    $('.codeImg').click();
                },
                submit(){
                    let check=false;
                    this.$refs['form'].validate(result=>{
                        check=result;
                    });
                    if (!check) {
                        this.$notify.warning('数据填写不规范');
                        return false;
                    }
                    let data = new FormData();
                    data.append('title', this.formData.title);
                    data.append('content', this.formData.content);
                    data.append('captcha', this.formData.securityCode);
                    data.append('user_id', this.user.id);
                    axios.post('/user/opinion', data)
                        .then(res => {
                            if (res.data.status_code === 200) {
                                app.$notify.success(res.data.message);
                            } else if (res.data.status_code === 500) {
                                res.data.error.forEach((item, index) => {
                                    setTimeout(function () {
                                        app.$notify.error(item);
                                    },1);
                                })
                            }
                            $('.codeImg').click();
                        })
                        .catch(err => {
                            app.$notify.error('服务器繁忙！请刷新重试！');
                            console.log(err);
                            $('.codeImg').click();
                        })
                }
            }
        })
    </script>

    <style>
        .form-opinion{
            margin: 60px auto 0 auto;
            width: 400px;
            border-radius: 25px;
            border: 1px solid darkgray;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            background-color: white;
            padding: 20px;

        }
        .form-input-content{
            min-height: 80px;
        }
        .el-input-group__prepend{
            border: none transparent 0;
            padding: 0 !important;
        }
        .codeImg{
            height: 38px;
            padding: 0 !important;
            cursor: pointer;
        }
    </style>
@endsection