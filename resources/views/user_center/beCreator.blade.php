@extends('user_center.master')

@section('title')
    申请成为创作者-个人中心
@endsection

@section('info-title')
    申请成为创作者
@endsection

@section('content')
    <el-container class="panel">
        <el-form label-position="right" label-width="80px">

            <el-form-item label="名称">
                <el-input v-model="creator"></el-input>
            </el-form-item>

            <el-form-item label="上传图标">
                <el-upload
                        ref="add-upload"
                        {{--上传地址--}}
                        action="123"
                        {{--是否自动上传--}}
                        :auto-upload="false"
                        {{--是否支持多选--}}
                        :multiple="false"
                        {{--list-type--}}
                        list-type="picture-card"
                        {{--接收的参数类型--}}
                        ccept=".jpg,.png"
                        {{--文件状态改变时的钩子，添加文件、上传成功和上传失败时都会被调用--}}
                        :on-change="handleFileChange"
                        {{--:on-preview="handlePictureCardPreview"--}}
                        :on-remove="handleRemove"
                        {{--覆盖默认上传事件--}}
                        :http-request="upload"
                        :withCredentials="false">
                    <i class="el-icon-plus"></i>
                    <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且大小不超过2MB</div>
                </el-upload>
            </el-form-item>

            <el-form-item>
                <el-button type="primary" @click="handleSubmit">提交申请</el-button>
            </el-form-item>

        </el-form>
    </el-container>
@endsection

@section('js_css')
    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    activeItem: [false, false, false, false, false, true, false, false, false,false,false],
                    user:@json($data),
                    creator: null,
                };
            },
            methods: {

                //单击提交按钮时 手动触发上传文件事件
                //覆盖默认上传事件 :http-request="upload"
                // this.$refs['add-upload'].submit() 执行后会调用upload方法进行上传图片处理
                handleSubmit() {
                    this.$refs['add-upload'].submit();
                },
                //上传图片
                upload(params) {
                    let data = new FormData;
                    data.append('file', params.file);
                    data.append('creator_name', this.creator);
                    axios.post('/user/beCreator', data, {
                        //允许为上传处理进度事件
                        onUploadProgress: progressEvent => {
                            // let percent = (progressEvent.loaded / progressEvent.total * 100) | 0;
                            // //调用elemnet组件的原始onProgress方法来显示进度条，需要传递个对象 percent为进度值
                            // params.onProgress({percent: percent})
                        }
                    })
                        .then(function (res) {
                            if (res.data.status_code == 200) {
                                app.$notify.success({
                                    title: '提示',
                                    message: res.data.message
                                })
                            } else {
                                res.data.error.forEach(item => {
                                    app.$notify.error({
                                        title: '提示',
                                        message: item,
                                    });
                                });
                            }
                        })
                        .catch(err => {
                            app.$notify.error({
                                title: '提示',
                                message: '上传失败！请刷新重试，或联系网站管理员进行处理！'
                            });
                            console.log(err)
                        })
                },

                handleFileChange(file, fileList) {
                    if (file.raw.type === 'image/jpeg' || file.raw.type === 'image/png') {
                        if (file.size / 1024 / 1024 > 2) {
                            this.clearFiles();
                            this.$message.warning('图片大小不能超过2MB')
                        } else {
                            if (fileList.length > 1) {
                                fileList.splice(0, 1);
                            }
                        }
                    } else {
                        this.clearFiles();
                        this.$message.warning('只能上传jpg/png格式图片')
                    }
                },
                handleRemove(file, fileList) {
                    console.log(file, fileList);
                },

                //清空文件列表
                clearFiles() {
                    this.$refs['add-upload'].clearFiles();
                }
            },
        })
    </script>

    <style>
        .panel {
            width: 60%;
            padding: 20px;
            margin-top: 50px;
            margin-left: 20%;
        }

    </style>
@endsection
