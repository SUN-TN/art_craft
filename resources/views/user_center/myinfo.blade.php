@extends('user_center.master')

@section('title')
    我的资料-个人中心
@endsection

@section('info-title')
    我的资料
@endsection

@section('content')
    <el-row class="info-item headImgBox">
        <el-col :sm="6" :xs="6" :md="4" :offset="10">
            <div>
                <el-upload
                        class="avatar-uploader"
                        :auto-upload="true"
                        action="{{asset('/uploadUserImg')}}"
                        :show-file-list="false"
                        :http-request="upload">
                    <el-image class="headImg" :src="'{{asset('/')}}'+user.imgUrl">
                        <el-avatar class="headImg_error" slot="error" :size="140" style="font-size: 1.5em">user
                        </el-avatar>
                    </el-image>
                    <el-avatar
                            v-show="false"
                            class="tip_upload"
                            :size="140"
                            style="font-size: 1.5em;">修改头像
                    </el-avatar>
                </el-upload>
            </div>
        </el-col>
    </el-row>

    <el-row class="info-item">
        <el-col :sm="8" :xs="6" :md="6" :offset="9">
            <el-input v-model="user.name"></el-input>
        </el-col>
        <el-col :sm="2" :xs="2" :md="2" :offset="1">
            <el-button type="primary" @click="changeUsername" style="border-color: #2a3237;background-color: #2a3237;color: white">修改用户名</el-button>
        </el-col>
    </el-row>

    <el-row class="info-item">
        <el-col :md="8" :offset="8">
            <span>@{{user.email}}</span>
        </el-col>
    </el-row>

@endsection

@section('js_css')
    <script>
        $(document).ready(function () {

            $(".headImg,.headImg_error,.tip_upload").hover(function () {
                $(".tip_upload").show();
            });

            $(".headImg,.headImg_error,.tip_upload").mouseleave(function () {
                $(".tip_upload").hide();
            });
        });

        var app = new Vue({
            el: '#app',
            data() {
                return {
                    activeItem: [true, false, false, false, false, false, false, false, false,false,false],  //must has
                    user:@json($data),  //must has
                }
            },
            methods: {
                beforeAvatarUpload(file) {
                    const isJPG = file.type === 'image/jpeg' | file.type === 'image/png';
                    const isLt2M = file.size / 1024 / 1024 < 2;

                    if (!isJPG) {
                        this.$notify.error({
                            title: '提示',
                            message: '上传头像图片只能是 JPG/PNG 格式!'
                        })
                    }
                    if (!isLt2M) {
                        this.$notify.error({
                            title: '提示',
                            message: '上传头像图片大小不能超过 2MB!'
                        });
                    }
                    return isJPG && isLt2M;
                },

                //修改头像图片
                upload(params) {
                    let check = this.beforeAvatarUpload(params.file);
                    if (check == false) {
                        return '';
                    }
                    let fd = new FormData;
                    fd.append('file', params.file);
                    fd.append('id', this.user.id)
                    axios.post('/uploadUserImg', fd, {
                        //允许为上传处理进度事件
                        onUploadProgress: progressEvent => {
                            // let percent = (progressEvent.loaded / progressEvent.total * 100) | 0;
                            //调用elemnet组件的原始onProgress方法来显示进度条，需要传递个对象 percent为进度值
                            // params.onProgress({percent: percent})
                        }
                    })
                        .then(function (res) {
                            if (res.data.status_code === 200) {
                                // app.imgUrl = res.data.path;
                                app.user.imgUrl = res.data.path;
                                app.$notify.success({
                                    title: '提示',
                                    message: '头像修改成功！'
                                })
                            } else {
                                app.$notify.error({
                                    title: '提示',
                                    message: res.data.error
                                });
                            }
                        })
                        .catch(err => {
                            app.$notify.error({
                                title: '提示',
                                message: '上传失败！请刷新重试，或联系网站管理员进行处理！'
                            });
                            console.log(err)
                        })
                },
                //修改用户名
                changeUsername() {
                    let data = new FormData();
                    data.append('username', this.user.name);
                    data.append('id', this.user.id);
                    axios.post('/user/changeUsername', data)
                        .then(function (res) {
                            if (res.data.status_code === 200) {
                                app.$notify.success({
                                    title: '提示',
                                    message: res.data.message
                                });
                            } else {
                                res.data.error.forEach(item => {
                                    setTimeout(function () {
                                        app.$notify.error({
                                            title: '提示',
                                            message: item
                                        });
                                    }, 1)
                                });
                            }
                        })
                        .catch(function (err) {
                            app.$notify.error({
                                title: '提示',
                                message: '服务器错误，请刷新重试'
                            });
                            console.log(err);
                        })
                }
            },
        });


    </script>


    <style>


        .info-item {
            margin-top: 50px;
        }

        .info-item .el-col {
            text-align: center;
        }

        .headImgBox {
            height: 140px;
            line-height: 140px;
        }

        .headImg {
            position: absolute;
            top: 0;
            left: 42%;
            height: 140px;
            width: 140px;
            border-radius: 50%;
            z-index: 1;
        }

        .tip_upload {
            position: absolute;
            top: 0;
            left: 42%;
            z-index: 999999;
            background-color: #55555577;
        }

        .footer{
            display: none;
        }

    </style>
@endsection
