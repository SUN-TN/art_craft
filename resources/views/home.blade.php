@extends('layout.master')

@section('title')
    Craft
@endsection

@section('link')
    <link rel="stylesheet" href="{{asset('/css/show/home.css')}}">
@endsection

@section('content')
    <section class="carousel-section">
        <el-row id="carousel">
            <el-col :span="24">
                <template>
                    <el-carousel :interval="4000" type="card" height="450px" @change="carouselChange">
                        <el-carousel-item v-for="(item,index) in imgs" :key="item.id">
                            <div class="carousel-item-box">
                                <a @click="carouselsHref(item,index)">
                                    <img class="medium" :src="wwwPath+item.imgUrl">
                                </a>
                                <div class="carousel-img-info" @click="carouselsHref(item,index)">
                                    @{{ item.describe }}
                                </div>
                            </div>
                        </el-carousel-item>
                    </el-carousel>
                </template>
            </el-col>
        </el-row>
    </section>

    <section id="app">

        <section id="recommend" style="margin-top: 100px;margin-bottom: 80px">
            <el-row>
                <el-col :span="18" :offset="3" class="row-recommend">
                    <el-row class="recommend-title">
                        <el-col :span="12" class="title">
                            <span>发现</span>
                        </el-col>
                        <el-col :span="2" :offset="8">
                            <div class="hot-more more">
                                <a @click="eyesRefresh">
                                    <span class="el-icon-refresh eyes-refresh"></span>
                                    <span>刷新</span>
                                </a>
                            </div>
                        </el-col>
                        <el-col :span="2">
                            <div class="hot-more more">
                                <a href="/eyes">
                                    <span>更多</span>
                                    <span class="el-icon-caret-right"></span>
                                </a>
                            </div>
                        </el-col>
                    </el-row>

                    <el-row style="margin-top: 20px">
                        <el-divider class="divider"></el-divider>
                        <el-col :span="22" :offset="1">
                            <el-row :gutter="30">

                                <el-col :span="8" v-for="(item,index) in eyes" :key=item.index>
                                    <div class="eyes-goods-box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">
                                                ￥ @{{item.price }} | @{{item.size}}
                                            </div>
                                        </div>
                                    </div>
                                </el-col>

                            </el-row>
                        </el-col>
                    </el-row>
                </el-col>
            </el-row>
        </section>

        <section id="hot-section">
            <el-row>
                <el-col :span="18" :offset="3" class="hot-title">
                    <el-row>
                        <el-col :span="12" class="title">
                            <span>热门作品</span>
                        </el-col>
                        <el-col :span="12">
                            <div class="hot-more more">
                                <a href="/hot">
                                    <span> 更多</span>
                                    <span class="el-icon-caret-right"></span>
                                </a>
                            </div>
                        </el-col>
                    </el-row>
                </el-col>
            </el-row>

            <el-row style="margin-top: 20px">
                <el-col :span="18" :offset="3">
                    <el-row :gutter="30">
                        <el-col :span="13">
                            <el-carousel indicator-position="outside" height="460px" class="goods-carousel">
                                <el-carousel-item v-for="item in carouselHots" :key="item.id">
                                    <div class="hot-left-box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                        </div>
                                    </div>
                                </el-carousel-item>
                            </el-carousel>
                        </el-col>

                        <el-col :span="11">
                            <el-row :gutter="15">
                                <el-col :span="12" v-for="(item,index) in showHotsRow1" :key="item.id">
                                    <div class="goods_box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                        </div>
                                    </div>
                                </el-col>
                            </el-row>

                            <el-row :gutter="15" class="goods_row2">
                                <el-col :span="12" v-for="(item,index) in showHotsRow2" :key="item.id">
                                    <div class="goods_box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                        </div>
                                    </div>
                                </el-col>
                            </el-row>
                        </el-col>
                    </el-row>
                </el-col>
            </el-row>
        </section>

        <section id="new-section" style="padding-bottom: 100px">
            <el-row>
                <el-col :span="18" :offset="3" class="new-title">
                    <el-row>
                        <el-col :span="12" class="title">
                            <span>新品上架</span>
                        </el-col>
                        <el-col :span="12">
                            <div class="hot-more more">
                                <a href="/new">
                                    <span>更多</span>
                                    <span class="el-icon-caret-right"></span>
                                </a>
                            </div>
                        </el-col>
                    </el-row>
                </el-col>
            </el-row>

            <el-row style="margin-top: 20px">
                <el-col :span="18" :offset="3">
                    <el-row :gutter="30">

                        <el-col :span="11">
                            <el-row :gutter="15">
                                <el-col :span="12" v-for="(item,index) in showNewsRow1" :key="item.id">
                                    <div class="goods_box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                        </div>
                                    </div>
                                </el-col>
                            </el-row>

                            <el-row :gutter="15" class="goods_row2">
                                <el-col :span="12" v-for="(item,index) in showNewsRow2" :key="item.id">
                                    <div class="goods_box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                        </div>
                                    </div>
                                </el-col>
                            </el-row>
                        </el-col>

                        <el-col :span="13">
                            <el-carousel direction="vertical" arrow="hover" height="460px" class="goods-carousel">
                                <el-carousel-item v-for="item in carouselNews" :key="item.id">
                                    <div class="hot-left-box">
                                        <img class="goods_img" :src="'{{asset('/')}}'+item.imgUrl">
                                        <div class="img-info" @click="detail(item)">
                                            <div class="img-info-item">@{{ item.author }}</div>
                                            <div class="img-info-item">@{{ item.name }}</div>
                                            <div class="img-info-item">￥ @{{ item.price }} | @{{ item.size }}</div>
                                        </div>
                                    </div>
                                </el-carousel-item>
                            </el-carousel>
                        </el-col>

                    </el-row>
                </el-col>
            </el-row>
        </section>

    </section>

    
@endsection

@section('js')
    <script>

        //轮播图
        let carousel = new Vue({
            el: '#carousel',
            created() {
                let c = this.imgs[0];
                document.styleSheets[0].addRule('#carousel::before', 'background-image:url(' + this.wwwPath + c.imgUrl + ');');
                for (let i = 0; i < this.imgs.length; i++) {
                    if (i === 0) {
                        this.carouselsA.push(true);
                    } else {
                        this.carouselsA.push(false);
                    }
                }
            },
            data() {
                return {
                    imgs: (@json($data)).carousels,
                    carouselsA: [],
                    wwwPath: '{{asset('')}}'.substring(0, '{{asset('')}}'.length - 1),
                }
            },
            methods: {
                //当前轮播图改变
                carouselChange(current, prev) {
                    let c = this.imgs[current];
                    for (let i = 0; i < this.carouselsA.length; i++) {
                        if (i === current) {
                            this.carouselsA.splice(i, 1, true);
                        } else {
                            this.carouselsA.splice(i, 1, false);
                        }
                    }
                    //设置轮播图背景为当前轮播图
                    document.styleSheets[0].addRule('#carousel::before', `background-image:url(${this.wwwPath}${c.imgUrl})`);
                },
                //点击轮播图 如果点击的轮播图为当前最顶层轮播图 则跳转至对应页面
                carouselsHref(item, index) {
                    if (this.carouselsA[index] === true) {
                        window.location = item.to;
                    }
                }
            }
        });

        let app = new Vue({
            el: '#app',
            created() {
                //将作品图放入对应的容器对应的数据模型数组
                for (let i = 0; i < 8; i++) {
                    if (i < 4) {
                        this.carouselHots.push(this.allHots[i]);
                        this.carouselNews.push(this.allNews[i]);
                    } else if (i < 6) {
                        this.showHotsRow1.push(this.allHots[i]);
                        this.showNewsRow1.push(this.allNews[i]);
                    } else {
                        this.showHotsRow2.push(this.allHots[i]);
                        this.showNewsRow2.push(this.allNews[i]);
                    }
                }
            },
            data() {
                return {
                    eyes:@json($data).eyes,

                    allHots:@json($data).hots,
                    carouselHots: [],
                    showHotsRow1: [],
                    showHotsRow2: [],

                    allNews:@json($data).news,
                    carouselNews: [],
                    showNewsRow1: [],
                    showNewsRow2: [],

                    wwwPath: '{{asset('/')}}'
                }
            },
            methods: {
                detail(item) {
                    let url = '{{asset('/detail')}}' + '/' + item.id;
                    window.open(url, "_blank");
                    // window.location = '/detail/' + item.id;
                },

                //刷新
                eyesRefresh() {
                    $('.eyes-refresh').addClass('eyes-refresh-click');
                    $('.eyes-goods-box').addClass('eyes-goods-box-refresh');

                    setTimeout(function () {
                        $('.eyes-refresh').removeClass('eyes-refresh-click');
                        $('.eyes-goods-box').removeClass('eyes-goods-box-refresh');
                    }, 1000);

                    axios.get('/eyesRefresh')
                        .then(res => {
                            app.eyes = [];
                            res.data.data.forEach((item, index) => {
                                app.eyes.push(item);
                            });
                        })
                        .catch(err => {
                            console.log(err);
                            app.$notify.error('服务器繁忙，请稍后再试')
                        });

                }
            }
        });

        $(document).ready(function () {
            function img_info() {
                $('.goods_img').on({
                    'mouseenter': function () {
                        $(this).next().show();
                    },
                    "mouseleave": function () {
                        $(this).next().hide();
                    }
                });
                $(".img-info").on({
                    'mouseenter': function () {
                        $(this).show();
                    },
                    "mouseleave": function () {
                        $(this).hide();
                    }
                });

            }

            img_info();
        });
    </script>
@endsection
