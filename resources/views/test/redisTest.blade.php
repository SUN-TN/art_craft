<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="{{asset('/elementUI/index.js')}}"></script>
    <script src="{{asset('/js/axios.min.js')}}"></script>
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/elementUI/index.css')}}">
    <title>Document</title>
</head>
<body>
<div id="app">
    <el-button type="primary" @click="lock">锁</el-button>
</div>
<script>
    var app = new Vue({
        el: "#app",
        data() {
            return {
                stop:false,
            }
        },
        methods: {
            lock() {

                for (let i = 0; i < 1; i++) {
                    setTimeout(function () {
                        app.locks(i);
                    }, 1);

                    setTimeout(function () {
                        app.locks(i);
                    }, 1);

                    setTimeout(function () {
                        app.locks(i);
                    }, 1);

                    setTimeout(function () {
                        app.locks(i);
                    }, 1);

                    setTimeout(function () {
                        app.locks(i);
                    });
                    setTimeout(function () {
                        app.locks(i);

                    }, 1);
                    setTimeout(function () {
                        app.locks(i);

                    }, 1);
                    setTimeout(function () {
                        app.locks(i);

                    }, 1);
                }
            },

            locks(i) {
                if(app.stop){
                    return false;
                }
                let data = new FormData();
                data.append('id', '123');
                axios.post('/redis/lock', data)
                    .then(res => {
                        if (res.data.status_code === 200) {
                            setTimeout(function () {
                               let s=res.data.message;
                               console.log(s);
                                app.$notify.success('加锁成功'+s);
                                if(parseInt(s)>0){
                                    console.log(parseInt(s)>0);
                                    app.lock();
                                }else{
                                    app.stop=true;
                                }
                            }, 1);
                        } else {
                            setTimeout(function () {
                                app.$notify.warning('加锁失败');
                            }, 1)
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        setTimeout(function () {
                            app.$notify.error('错误');
                        }, 1)
                    })
            }
        }

    })
</script>
</body>
</html>
