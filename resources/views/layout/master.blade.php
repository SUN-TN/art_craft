<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{asset('images/icon.png')}}"/>
    <script src="{{asset('/js/vue.js')}}"></script>
    <script src="{{asset('/elementUI/index.js')}}"></script>
    <script src="{{asset('/js/axios.min.js')}}"></script>
    <!-- <script src="https://cdn.bootcdn.net/ajax/libs/axios/0.19.2/axios.min.js"></script> -->
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('/elementUI/index.css')}}">
    @yield('link')
</head>
<body scroll="auto">

<header id="header">
    <el-row>
        <el-col :span="2" :offset="1" class="col-icon">
            <a href="/home"><img src="{{asset('images/icon.png')}}" class="icon"></a>
        </el-col>
        <el-col :span="8" :offset="1" class="nav-menu-left">
            <el-menu
                default-active="1"
                class="el-menu-demo"
                @select="navSelect"
                :default-active="activeIndex"
                mode="horizontal">
                <el-menu-item index="1"><a href="/home">首 页</a></el-menu-item>
                <el-menu-item index="2"><a href="/eyes">发 现</a></el-menu-item>
                <el-menu-item index="3"><a href="/hot">热门作品</a></el-menu-item>
                <el-menu-item index="4"><a href="/new">新品上架</a></el-menu-item>
            </el-menu>
        </el-col>

        @if(Auth::check())
            <el-col :span=7 :offset="5" class="nav-menu-right">
                <el-menu
                    default-active="0"
                    class="el-menu-demo"
                    mode="horizontal"
                    background-color="#fff"
                    text-color="#00000"
                    active-text-color="#000000">

                    <el-menu-item v-if="cartFullVisible" index="1">
                        <a href="/user/myCart" class="el-icon-shopping-cart-full">购物车</a>
                    </el-menu-item>

                    <el-menu-item v-else index="1">
                        <a href="/user/myCart" class="el-icon-shopping-cart-2">购物车</a>
                    </el-menu-item>

                    <el-menu-item v-if="starFullVisible" index="2">
                        <a href="/user/myStar" class="el-icon-star-on">收藏</a>
                    </el-menu-item>
                    <el-menu-item v-else index="2">
                        <a href="/user/myStar" class="el-icon-star-off">收藏</a>
                    </el-menu-item>


                    <el-menu-item v-if="orderFullVisible" index="3">
                        <a href="/user/myOrder" class="el-icon-s-goods">订单</a>
                    </el-menu-item>
                    <el-menu-item v-else index="3">
                        <a href="/user/myOrder" class="el-icon-goods">订单</a>
                    </el-menu-item>


                    <el-menu-item>
                        <el-popover
                            placement="bottom"
                            width="200"
                            trigger="hover">
                            <div>
                                <div class="user-item user-name">
                                    <h4>{{Auth::user()->name}}</h4>
                                </div>
                                <div class="user-item">
                                    <a href="/user">
                                        <el-button>个人中心</el-button>
                                    </a>
                                </div>
                                <div class="user-item">
                                    <a href="/user/myCart">
                                        <el-button>购物车</el-button>
                                    </a>
                                </div>
                                <div class="user-item">
                                    <a href="/user/myStar">
                                        <el-button>收藏</el-button>
                                    </a>
                                </div>
                                <div class="user-item">
                                    <a href="/user/myOrder">
                                        <el-button>订单</el-button>
                                    </a>
                                </div>
                                <div class="user-item">
                                    <a href="/user/changePassword">
                                        <el-button>修改密码</el-button>
                                    </a>
                                </div>
                                <div class="user-item">
                                    <el-form action="/entry/logout">
                                        <el-button native-type="submit">退出登录</el-button>
                                    </el-form>
                                </div>
                            </div>
                            <el-image class="user-avatar" slot="reference" src="{{Auth::user()->imgUrl}}">
                                <i slot="error" class="el-icon-user error-user"></i>
                            </el-image>
                        </el-popover>
                    </el-menu-item>
                </el-menu>
            </el-col>
        @else
            <el-col :span=7 :offset="5" class="nav-menu-right">
                <el-menu
                    default-active="0"
                    class="el-menu-demo"
                    mode="horizontal"
                    text-color="#00000"
                    active-text-color="#000000">
                    <el-menu-item index="6">
                        <a href="/entry/login">登录</a>
                    </el-menu-item>
                </el-menu>
            </el-col>
        @endif
    </el-row>

</header>

<section id="main">
    @yield('content')
</section>

<div class="el-backtop" onclick="up()">
    <el-tooltip class="item" effect="dark" content="回到顶部" placement="top">
        <p>UP</p>
    </el-tooltip>
</div>

<script>
    var header = new Vue({
        el: '#header',
        data() {
            return {
                activeIndex:@json($data).activeIndex,
                cartFullVisible:@json($data).cartFullVisible,
                starFullVisible:@json($data).starFullVisible,
                orderFullVisible:@json($data).orderFullVisible,
            }
        },
        methods: {
            navSelect(key, keyPath) {
                switch (key) {
                    case '1':
                        window.location.href = '/home';
                        break;
                    case '2':
                        window.location.href = '/eyes';
                        break;
                    case '3':
                        window.location.href = '/hot';
                        break;
                    case '4':
                        window.location.href = '/new';
                        break;
                }
            }
        }
    });

    function up() {
        var gotoTop = function () {
            var currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
            currentPosition -= 10;
            if (currentPosition > 0) {
                window.scrollTo(0, currentPosition);
            }
            else {
                window.scrollTo(0, 0);
                clearInterval(timer);
                timer = null;
            }
        };
        var timer = setInterval(gotoTop, 1);
    }
</script>
@yield('js')

<style>
    * {
        padding: 0;
        margin: 0;
    }
    html {
        overflow-y: scroll;
    }

    body {
        overflow: visible;
        min-width: 1400px;
    }

    a {
        color: black;
        text-decoration: none;
    }

    header {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 60px;
        z-index: 10;
        background-color: white;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.1), 0 2px 2px 0 rgba(0, 0, 0, 0.1);
    }

    .icon {
        width: 45px;
        height: 45px;
        padding-top: 8px;
        overflow: hidden;
        background-size: cover;
    }

    .el-menu-demo {
        border-bottom: none !important;
    }

    .nav-menu-left .el-menu-demo {
        min-width: 500px !important;
    }

    .nav-menu-right .el-menu-demo {
        min-width: 500px !important;
    }


    .el-menu-demo .el-menu-item {
        color: #2a3237;
        font-family: "Arial Black";

    }

    .el-menu-demo .el-menu-item:hover {
        background-color: white !important;
    }

    .nav-menu-left .el-menu-demo .el-menu-item {
        font-size: 1.1em !important;
        margin-left: 20px;
        background-color: white;
        color: black;
    }

    .nav-menu-left .el-menu--horizontal > .el-menu-item.is-active {
        background-color: #2a3237 !important;
        color: white;
        border-bottom: none;
    }

    .nav-menu-right .el-menu-demo .el-menu-item {
        font-size: 1em;
        margin-left: 20px;
        border-bottom: none;
    }

    .nav-menu-right .userInfo {
        text-align: center;
    }

    .el-avatar:hover {
        cursor: pointer;
    }

    .user-avatar {
        width: 45px;
        height: 45px;
        border-radius: 50px;
    }

    .error-user {
        width: 45px;
        height: 45px;
        border-radius: 50px;
    }

    .error-user::before {
        font-size: 1.5em;
    }

    .user-item {
        padding-top: 5px;
        text-align: center;
    }

    .user-item button {
        width: 100%;
        border: none;
    }

    .user-name {
        font-size: 1.3em;
    }

    .user-item button:hover {
        color: white;
        background-color: #2a3237 !important;
    }

    #main {
        margin-top: 60px;
        width: 100%;
        min-width: 1400px;
        height: auto;
    }

    .el-backtop,.el-backtop:hover  {
        right: 40px;
        bottom: 100px;
        background-color: #2a3237;
        color: white;
    }


    .el-backtop a {
        color: white;
    }


</style>
</body>
</html>
