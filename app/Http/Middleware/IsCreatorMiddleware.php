<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsCreatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect('/entry/login');
        }

        if (!Auth::user()->is_sale){
            return redirect('/entry/login');
        }
        return $next($request);
    }
}
