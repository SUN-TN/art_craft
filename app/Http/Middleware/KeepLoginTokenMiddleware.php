<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;

class KeepLoginTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $loginToken = md5($request->getClientIp() . Auth::id() . time());
            // 当前 loginToken 存入 Redis 120分钟 key为 [STRING_SINGLETOKEN_用户id]
            Cache::put('STRING_SINGLETOKEN_' . Auth::id(), $loginToken, 60 * 120);
        }
        return $next($request);
    }
}
