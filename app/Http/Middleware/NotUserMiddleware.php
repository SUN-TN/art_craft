<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class NotUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() && !Auth::guard('admin')->check()){
            return '您无此权限';
        }
        return $next($request);
    }
}
