<?php

namespace App\Http\Controllers\BuyProcess;

use App\Http\Controllers\Controller;
use App\Model\Goods;

class PaySuccessController extends Controller
{
    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index($id)
    {
        $like = Goods::inRandomOrder()->where('status','=',1)->take(8)->get();
        $data = array(
            'like' => $like,
            'id' => $id,
        );
        return view('buy_process.doneOfPayment', compact('data'));
    }
}
