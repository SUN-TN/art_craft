<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Component\MasterResponseController;
use Illuminate\Http\Request;
use App\Model\Goods;
use Illuminate\Support\Facades\Redis;

class TestController extends MasterResponseController
{
    public function restore($id)
    {
//        Goods::where('id',$id)->restore();
        return response()->json(['success' => 'ok']);
    }

    public function restoreAll()
    {
        Goods::where('deleted_at', '<>', null)->restore();
        return 'ok';
    }

    public function show($start, $end, $filters = [])
    {
        return response()->json([
            "data" => $filters,
        ]);
    }

    public function index()
    {
        $redis = Redis::connection('redis-order');
        $redis->set('s1',200,'ex',60*3);
        return view('test.redisTest');
    }

    public function lock(Request $request)
    {
        $redis = Redis::connection('redis-order');
        $key=$request['id'];
        $random=rand();
        $ttl=10;
        $ok = $redis->set($key, $random,  'ex' , $ttl,'nx');
        if ($ok) {
            $v=$redis->get('s1');
            $v-=1;
            $redis->set('s1',$v);
            if ($redis->get($key) == $random) {
                $redis->del($key);
            }
            if ($v<0){
                return parent::error($v);
            }
            return parent::success($v);
        }else{
            return parent::error('回销失败');
        }


    }


}
