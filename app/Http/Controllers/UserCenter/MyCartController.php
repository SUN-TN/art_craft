<?php

namespace App\Http\Controllers\UserCenter;

use App\Model\Cart;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class MyCartController extends Controller
{

    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {

        $subQuery = Cart::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc');

        $carts = DB::table(DB::raw("({$subQuery->toSql()}) as sub"))
            ->leftJoin('goods', 'goods.id', '=', 'sub.goods_id')
            ->mergeBindings($subQuery->getQuery())
            ->get();
        $data = array(
            'carts' => $carts
        );
        return view('user_center.myCart', compact('data'));
    }
}
