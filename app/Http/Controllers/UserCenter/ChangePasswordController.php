<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Component\MasterResponseController;
use App\Http\Requests\UserCenter\ChangePasswordRequest;
use Auth;
use App\User;

class ChangePasswordController extends MasterResponseController
{

    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {
        $data = Auth::user();
        return view('user_center.changePassword',compact('data'));
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        User::where('id',Auth::user()->id)->update(['password'=> bcrypt($request['password'])]);
        return parent::success('密码已修改');
    }
}
