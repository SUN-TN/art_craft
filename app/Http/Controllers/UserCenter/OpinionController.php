<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Admin\MasterController;
use App\Http\Requests\UserCenter\OpinionRequest;
use App\Model\Opinion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OpinionController extends MasterController
{
    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {


        return view('user_center.opinion');
    }

    public function add(OpinionRequest $request)
    {
        Opinion::create([
           'user_id'=>$request['user_id'],
           'title'=>$request['title'],
           'content'=>$request['content'],
        ]);

        return parent::success('提交成功，感谢您对本站提出的建议！');
    }
}
