<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Controller;
use App\Model\Star;
use Auth;
use DB;

class MyStarController extends Controller
{

    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {
        $subQuery=Star::where('user_id',Auth::user()->id)
            ->orderBy('created_at','desc');

        $stars=DB::table(DB::raw("({$subQuery->toSql()}) as sub"))
            ->leftJoin('goods','goods.id','=','sub.goods_id')
            ->mergeBindings($subQuery->getQuery())
            ->get();
        $data = array(
            'stars' => $stars,
        );
        return view('user_center.myStar', compact('data'));
    }
}
