<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Component\MasterResponseController;
use App\Http\Requests\UserCenter\BeCreatorRequest;
use Auth;
use App\Model\Creator;
use Webpatser\Uuid\Uuid;

class BeCreatorController extends MasterResponseController
{

    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {
        $data = Auth::user();
        return view('user_center.beCreator', compact('data'));
    }

    public function beCreator(BeCreatorRequest $request)
    {
        $upload = $request->file;
        if ($upload->isValid()) {
            $path = '/images/creatorHead/' . $upload->store('', 'creatorHead');
            Creator::create([
                'id' => Uuid::generate(4),
                'user_id' => Auth::user()->id,
                'creator_name' => $request['creator_name'],
                'img_path' => $path,
                'check_passed' => false,
            ]);
            return parent::success('申请已提交，请耐心等待管理审核');
        }
        return parent::error('上传失败！请刷新重试！');
    }
}
