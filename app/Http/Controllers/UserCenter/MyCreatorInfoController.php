<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Component\MasterResponseController;
use App\Http\Requests\UserCenter\BeCreatorRequest;
use App\Model\Creator;
use Auth;
use App\User;

class MyCreatorInfoController extends MasterResponseController
{
    public function __construct()
    {
        $this->middleware('isCreator');
    }

    public function index()
    {
        $userId = Auth::user()->id;
        $data = Creator::where('user_id', $userId)->first();
        return view('user_center.myCreatorInfo', compact('data'));
    }

    public function changeCreatorName(BeCreatorRequest $request)
    {
        $dd = $request->all();
        Creator::where('id', $dd['id'])->update(['creator_name' => $dd['creator_name']]);
        return parent::success('创作名修改成功');
    }
}
