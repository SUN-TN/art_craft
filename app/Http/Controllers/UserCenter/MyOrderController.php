<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Component\MasterResponseController;
use App\Model\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class MyOrderController extends MasterResponseController
{

    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {
        $user_id = Auth::user()->id;

        //等待付款订单
        $ordersOfWaitPay = Order::leftJoin('goods_v', 'orders.goods_id', 'goods_v.id')
            ->leftJoin('china_region', 'china_region.town_id', 'orders.town_id')
            ->select('orders.*',
                'china_region.province_name as province',
                'china_region.city_name as city',
                'china_region.county_name as county',
                'china_region.town_name as region',
                'goods_v.name as goods_name',
                'goods_v.author as author',
                'goods_v.price as price',
                'goods_v.size as size',
                'goods_v.genre as genre',
                'goods_v.imgUrl as imgUrl')
            ->where('orders.user_id', $user_id)
            ->where('orders.status', '=', 0)
            ->orderBy('orders.created_at', 'desc')
            ->get();

        //正在进行订单
        $ordersOfOngoing = Order::leftJoin('goods_v', 'orders.goods_id', 'goods_v.id')
            ->leftJoin('china_region', 'china_region.town_id', 'orders.town_id')
            ->select('orders.*',
                'china_region.province_name as province',
                'china_region.city_name as city',
                'china_region.county_name as county',
                'china_region.town_name as region',
                'goods_v.name as goods_name',
                'goods_v.author as author',
                'goods_v.price as price',
                'goods_v.size as size',
                'goods_v.genre as genre',
                'goods_v.imgUrl as imgUrl')
            ->where('orders.user_id', $user_id)
            ->where(function ($query) {
                $query->where('orders.status', 1)->orWhere('orders.status', 100)
                    ->orWhere('orders.status', 2)->orWhere('orders.status', 200)
                    ->orWhere('orders.status', 3);
            })
            ->orderBy('orders.created_at', 'desc')
            ->get();

        //已关闭订单
        $ordersOfClose = Order::leftJoin('goods_v', 'orders.goods_id', 'goods_v.id')
            ->leftJoin('china_region', 'china_region.town_id', 'orders.town_id')
            ->select('orders.*',
                'china_region.province_name as province',
                'china_region.city_name as city',
                'china_region.county_name as county',
                'china_region.town_name as region',
                'goods_v.name as goods_name',
                'goods_v.author as author',
                'goods_v.price as price',
                'goods_v.size as size',
                'goods_v.genre as genre',
                'goods_v.imgUrl as imgUrl')
            ->where('orders.user_id', $user_id)
            ->where('orders.status', 5)
            ->orderBy('orders.created_at', 'desc')
            ->get();

        //已完成订单
        $ordersOfDone = Order::leftJoin('goods_v', 'orders.goods_id', 'goods_v.id')
            ->leftJoin('china_region', 'china_region.town_id', 'orders.town_id')
            ->select('orders.*',
                'china_region.province_name as province',
                'china_region.city_name as city',
                'china_region.county_name as county',
                'china_region.town_name as region',
                'goods_v.name as goods_name',
                'goods_v.author as author',
                'goods_v.price as price',
                'goods_v.size as size',
                'goods_v.genre as genre',
                'goods_v.imgUrl as imgUrl')
            ->where('orders.user_id', $user_id)
            ->where('orders.status', 4)
            ->orderBy('orders.created_at', 'desc')
            ->get();

        $data = array(
            'ordersOfWaitPay' => $ordersOfWaitPay,
            'ordersOfOngoing' => $ordersOfOngoing,
            'ordersOfClose' => $ordersOfClose,
            'ordersOfDone' => $ordersOfDone,
        );
        return view('user_center.myOrder', compact('data'));
    }

    //取消订单
    public function cancelOrder(Request $request)
    {
        $time = Carbon::now();
        Order::where('id', $request['id'])->update([
            'status' => 5,
            'cancel_time' => $time
        ]);
        return parent::success('订单已取消');
    }

    //确认收货
    public function confirmReceipt(Request $request)
    {

        $time = Carbon::now();
        Order::where('id', $request['id'])->update([
            'status' => 4,
            'finish_time' => $time
        ]);
        return parent::success('订单完成,感谢您的支持');
    }


}
