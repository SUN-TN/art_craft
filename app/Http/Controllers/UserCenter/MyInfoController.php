<?php

namespace App\Http\Controllers\UserCenter;

use App\Http\Controllers\Component\MasterResponseController;
use App\Http\Requests\UserCenter\ChangeNameRequest;
use App\User;
use Auth;
use Cache;

class MyInfoController extends MasterResponseController
{
    public function __construct()
    {
        $this->middleware('unlogin');
    }

    public function index()
    {
        $data = Auth::user();
        return view('user_center.myinfo', compact('data'));
    }

    //修改用户名
    public function changeUsername(ChangeNameRequest $request)
    {
        $dd = $request->all();
        User::where('id', $dd['id'])->update(['name' => $dd['username']]);
        return parent::success('用户名修改成功');
    }

    public function unLogout()
    {
        $unlogout = Cache::get('unlogout');
        if ($unlogout!=null && $unlogout==true){
            Cache::forget('unlogout');
            return response()->json(['unlogout'=>true]);
        }else{
            return response()->json(['unlogout'=>false]);
        }

    }


}
