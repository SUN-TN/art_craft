<?php

namespace App\Http\Controllers\Show;

use App\Http\Controllers\Component\MasterResponseController;
use App\Model\Cart;
use App\Model\Star;
use App\Model\Order;
use Auth;


class ShowMasterController extends MasterResponseController
{

    //获取设置导航栏显示图标项相关信息
    public function getNavInfo()
    {
        $starCount = 0;
        $cartCount = 0;
        $orderCount = 0;
        if (Auth::check()) {
            $starCount = Star::where('user_id', Auth::user()->id)->count('*');
            $cartCount = Cart::where('user_id', Auth::user()->id)->count('*');
            $orderCount = Order::where('user_id', Auth::user()->id)->count('*');
//                ->where('status',0)->orwhere('status',1)->orwhere('status',2)
        }
        $starFullVisible = false;
        $cartFullVisible = false;
        $orderFullVisible = false;
        if ($starCount > 0) {
            $starFullVisible = true;
        }
        if ($cartCount > 0) {
            $cartFullVisible = true;
        }
        if ($orderCount > 0) {
            $orderFullVisible = true;
        }
        $data = array(
            'starFullVisible' => $starFullVisible,
            'cartFullVisible' => $cartFullVisible,
            'orderFullVisible' => $orderFullVisible,
        );

        return $data;

    }
}
