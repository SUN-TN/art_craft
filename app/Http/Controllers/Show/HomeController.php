<?php

namespace App\Http\Controllers\Show;

use App\Model\Carousel;
use App\Model\Goods;

class HomeController extends ShowMasterController
{
    public function index(){
        $navVisible=parent::getNavInfo();
        $carousels=Carousel::all();
        $hots=Goods::where('status','=',1)->take(8)->orderBy('stars','desc')->get();
        $news=Goods::where('status','=',1)->take(8)->orderBy('created_at','desc')->get();
        $eyes=Goods::where('status','=',1)->inRandomOrder()->take(3)->get();

        $data=array(
            'activeIndex'=>'1',
            'starFullVisible' => $navVisible['starFullVisible'],
            'cartFullVisible' => $navVisible['cartFullVisible'],
            'orderFullVisible' => $navVisible['orderFullVisible'],
            'carousels'=>$carousels,
            'hots'=>$hots,
            'news'=>$news,
            'eyes'=>$eyes,
        );
        return view('home',compact('data'));
    }

    public function eyesRefresh(){
        $eyes=Goods::where('status','=',1)->inRandomOrder()->take(3)->get();
        return parent::withData($eyes);
    }
}
