<?php

namespace App\Http\Controllers\Show;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class NewController extends ShowMasterController
{
    protected $pageSize = 20;
    protected $filter = false; //如果为true则用近30天方式进行获取数据，如果为false则加载最近100条数据

    public function index()
    {
        $navVisible = parent::getNavInfo();

        $starTime = Carbon::now()->subDays(30);
        $count = DB::table('goods_v')
            ->where('status','=',1)
            ->whereDate('created_at', '>=', $starTime)
            ->orderBy('created_at', 'desc')->count('*');
        if ($count < ($this->pageSize * 2)) {
            $this->filter = false;
            $count = DB::table('goods_v')->where('status','=',1)->count('*');
            if ($count >= 100) {
                $count = 100;
            }
        }
        if ($this->filter) {
            $newGoods = DB::table('goods_v')
                ->where('status','=',1)
                ->whereDate('created_at', '>=', $starTime)
                ->take($this->pageSize)
                ->orderBy('created_at', 'desc')->get();
        } else {
            $newGoods = DB::table('goods_v')
                ->where('status','=',1)
                ->take($this->pageSize)
                ->orderBy('created_at', 'desc')->get();
        }


        $data = array(
            'activeIndex' => '4',
            'starFullVisible' => $navVisible['starFullVisible'],
            'cartFullVisible' => $navVisible['cartFullVisible'],
            'orderFullVisible' => $navVisible['orderFullVisible'],
            'newGoods' => $newGoods,
            'count' => $count,
        );
        return view('show.new', compact('data'));
    }

    public function newPage($page)
    {
        $start = ($page - 1) * $this->pageSize;
        $starTime = Carbon::now()->subDays(30);
        if ($this->filter) {
            $newGoods = DB::table('goods_v')
                ->where('status','=',1)
                ->whereDate('created_at', '>=', $starTime)
                ->skip($start)->take($this->pageSize)
                ->orderBy('created_at', 'desc')->get();
        } else {
            $newGoods = DB::table('goods_v')
                ->where('status','=',1)
                ->skip($start)->take($this->pageSize)
                ->orderBy('created_at', 'desc')->get();
        }
        return $this->withData($newGoods);
    }

}
