<?php

namespace App\Http\Controllers\Show;

use App\Http\Controllers\Component\MasterResponseController;
use App\Model\Cart;
use App\Model\Star;
use Illuminate\Http\Request;
use DB;
use App\Model\Goods;
use Auth;
use Webpatser\Uuid\Uuid;

class DetailController extends MasterResponseController
{
    public function index($id)
    {
        $craft = Goods::withTrashed()
            ->leftJoin('genres', 'genres.id', 'goods.genre_id')
            ->select(
                'goods.id as id',
                'goods.name as name',
                'goods.author as author',
                'goods.intro as intro',
                'goods.size as size',
                'goods.price as price',
                'goods.imgUrl as imgUrl',
                'goods.stars as stars',
                'goods.status as status',
                'goods.created_at as created_at',
                'goods.updated_at as updated_at',
                'genres.genre as genre',
                'goods.genre_id as genre_id'
            )
            ->where('goods.id', $id)
            ->first();
        $like = Goods::inRandomOrder()->take(8)->get();
        if (Auth::check()) {
            $isStar = Star::where('goods_id', $id)
                ->where('user_id', Auth::user()->id)->count('*');
            if ($isStar > 0) {
                $isStar = true;
            } else {
                $isStar = false;
            }
            $inCart = Cart::where('goods_id', $id)
                ->where('user_id', Auth::user()->id)->count('*');
            if ($inCart > 0) {
                $inCart = true;
            } else {
                $inCart = false;
            }
        } else {
            $isStar = false;
            $inCart = false;
        }

        $data = array(
            'craft' => $craft,
            'isStar' => $isStar,
            'inCart' => $inCart,
            'like' => $like,
        );
        return view('show.detail', compact('data'));
    }

    //加入收藏
    public function addStar(Request $request)
    {
        if (!Auth::check()) {
            return parent::error('请先登录！');
        } else {
            Star::create([
                'id' => Uuid::generate(4),
                'goods_id' => $request['id'],
                'user_id' => Auth::user()->id,
            ]);
            $stars = Goods::where('id', $request['id'])->value('stars');
            $stars++;
            Goods::where('id', $request['id'])->update([
                'stars' => $stars
            ]);
            return parent::success('已添加到您的收藏，快去查看吧！');
        }
    }

    //移出收藏
    public function removeStar(Request $request)
    {
        Star::where('goods_id', $request['id'])
            ->where('user_id', Auth::user()->id)->delete();
        $stars = Goods::where('id', $request['id'])->select('stars')->first();
        $stars--;
        Goods::where('id', $request['id'])->update([
            'stars' => $stars
        ]);
        return parent::success('已从您的收藏中移出！');
    }

    //加入购物车
    public function addCart(Request $request)
    {
        if (!Auth::check()) {
            return parent::error('请先登录！');
        } else {
            Cart::create([
                'id' => Uuid::generate(4),
                'goods_id' => $request['id'],
                'user_id' => Auth::user()->id,
            ]);
            return parent::success('已添加到您的购物车，快去查看吧！');
        }
    }

    //移出购物车
    public function removeCart(Request $request)
    {
        Cart::where('goods_id', $request['id'])
            ->where('user_id', Auth::user()->id)->delete();
        return parent::success('已从您的购物车中移出！');
    }
}
