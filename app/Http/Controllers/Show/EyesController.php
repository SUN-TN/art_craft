<?php

namespace App\Http\Controllers\Show;

use App\Model\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Goods;

class EyesController extends ShowMasterController
{
    protected $pageSize = 20;

    public function index()
    {
        $navVisible = parent::getNavInfo();
        $genres = Genre::all();
        $count = Goods::where('status','=',1)->count('*');
        $goods = Goods::where('status','=',1)->take($this->pageSize)->get();
        $data = array(
            'activeIndex' => '2',
            'starFullVisible' => $navVisible['starFullVisible'],
            'cartFullVisible' => $navVisible['cartFullVisible'],
            'orderFullVisible' => $navVisible['orderFullVisible'],
            'genres' => $genres,
            'count' => $count,
            'goods' => $goods,
        );

        return view('show.eyes', compact('data'));
    }

    /**
     * @param $currentPage
     * @param $min
     * @param $max //如果max选择节点为max则传入'max' 否则传入选择节点数值即可，
     * @param $genre_id //如果选择所有分类则传入all
     * @param string $searchText //如果无查询作品名则无需传入
     * @return \Illuminate\Http\JsonResponse
     */

    public function newPage($currentPage,$min, $max, $genre_id, $searchText = '')
    {
        $start=$this->pageSize*($currentPage-1);
        $query=Goods::where('status','=',1)
            ->where('price','>=',$min)
            ->skip($start)->take($this->pageSize);
        $queryCount=Goods::where('status','=',1)
            ->where('price','>=',$min);

        if($max!='max'){
            $query=$query->where('price','<=',$max);
            $queryCount=$queryCount->where('price','<=',$max);
        }

        if ($genre_id!='all'){
            $query=$query->where('genre_id',$genre_id);
            $queryCount=$queryCount->where('genre_id',$genre_id);
        }

        if($searchText!=''){
            $query=$query->where('name','like','%'.$searchText.'%');
            $queryCount=$queryCount->where('name','like','%'.$searchText.'%');
        }

        $count=$queryCount->count('*');
        $goods=$query->get();
       $data=array(
           'count'=>$count,
           'goods'=>$goods
       );
       return parent::withData($data);
    }
}
