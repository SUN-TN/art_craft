<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth');
    }


    public function success($message)
    {
        return response()->json([
            'status_code' => 200,
            'message' => $message,
        ], 200);
    }


    public function error(...$error)
    {
        return response()->json([
            'status_code' => 500,
            'error' => $error,
        ], 200);
    }

    public function successWithData($data){
        return response()->json([
            'status_code'=>200,
            'data'=>$data,
            ]);
    }

}
