<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AdminsRequest;
use App\Model\Admin;


class AddAdminController extends MasterController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('admin.userinfo.addAdmin');
    }

    public function addAdmin(AdminsRequest $request)
    {
        Admin::create([
            'username' => $request['username'],
            'password' => bcrypt($request['password'])
        ]);
        return parent::success('管理员添加成功！');
    }
}
