<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use App\Model\OrderStatus;


class OrderController extends MasterController
{
    protected const pageSize = 20;

    public function index()
    {
        //获取所有订单状态
        $orderStatus = OrderStatus::all();
        //获取订单总数
        $orderCount = Order::count('*');
        //获取前20条订单信息
        $orders = Order::leftJoin('users', 'orders.user_id', 'users.id')
            ->leftJoin('goods', 'orders.goods_id', 'goods.id')
            ->select(
                'orders.id',
                'orders.name as consignee_name',
                'orders.phone',
                'orders.status',
                'goods.imgUrl',
                'goods.name as goods_name',
                'users.name as username'
            )
            ->take(self::pageSize)
            ->orderBy('orders.created_at', 'desc')
            ->get();

        $data = array(
            'orderStatus' => $orderStatus,
            'orders' => $orders,
            'orderCount' => $orderCount
        );
        return view('admin.order.order', compact('data'));
    }

    //加载当前页数据
    public function newPage($currentPage, $status, $orderId='')
    {
        $startIndex=($currentPage-1)*self::pageSize;
        $query = Order::leftJoin('users', 'orders.user_id', 'users.id')
            ->leftJoin('goods', 'orders.goods_id', 'goods.id')
            ->select(
                'orders.id',
                'orders.name as consignee_name',
                'orders.phone',
                'orders.status',
                'goods.imgUrl',
                'goods.name as goods_name',
                'users.name as username'
            );
        if ($status != 'all') {
            $query = $query->where('orders.status', $status);
        }
        if ($orderId != '') {
            $query = $query->where('orders.id', $orderId);
        }
        $orders=$query->skip($startIndex)
            ->take(self::pageSize)
            ->orderBy('orders.created_at', 'desc')
            ->get();
        return parent::successWithData($orders);
    }

    //根据筛选条件加载第一页数据
    public function search($status, $orderId='')
    {
        $query = Order::leftJoin('users', 'orders.user_id', 'users.id')
            ->leftJoin('goods', 'orders.goods_id', 'goods.id')
            ->select(
                'orders.id',
                'orders.name as consignee_name',
                'orders.phone',
                'orders.status',
                'goods.imgUrl',
                'goods.name as goods_name',
                'users.name as username'
            );
        if ($status != 'all') {
            $query = $query->where('orders.status', $status);
        }
        if ($orderId != '') {
            $query = $query->where('orders.id', $orderId);
        }

        $orderCount = $query->count('*');

        $orders = $query->take(self::pageSize)
            ->orderBy('orders.created_at', 'desc')
            ->get();

        $data = array(
            'orderCount'=>$orderCount,
            'orders'=>$orders
        );
        return parent::successWithData($data);

    }
}
