<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Component\SendMailController;
use App\Jobs\SendEmail;
use App\Mail\NoticeMail;
use App\User;
use Illuminate\Http\Request;
use App\Model\Creator;
use DB;

class BeCreatorAuditController extends MasterController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data = DB::table('audit_creator_v')
            ->select('*')
            ->get();
        return view('admin.audit.beCreator', compact('data'));
    }

    //同意
    public function agree(Request $request)
    {
        Creator::where('id', $request['id'])->update(['check_passed' => true]);
        User::where('id',$request['user_id'])->update(['is_sale'=>true]);
        $to = $request['email'];
        $subject = '创作者审核结果—CRAFTS';
        $message = "您在本站的创作者申请已通过审核，快去上架您的作品吧！";
        $mail = (new NoticeMail($request['name'],$message,$subject))
            ->onConnection('redis')
            ->onQueue('emails');
        SendEmail::dispatch($to,$mail);
        return parent::success('审核结果已发送至用户邮箱！');
    }

    //拒绝
    public function refuse(Request $request)
    {
        Creator::where('id', $request['id'])->delete();
        $to = $request['email'];
        $subject = '创作者审核结果—CRAFTS';
        $message = "非常抱歉，由于" . $request['reason'] . ",您在本站的创作者申请未通过审核，请您修改相关信息后重新进行申请";
        $mail = (new NoticeMail($request['name'],$message,$subject))
            ->onConnection('redis')
            ->onQueue('emails');
        SendEmail::dispatch($to,$mail);
        return parent::success('审核结果已发送至用户邮箱！');
    }
}
