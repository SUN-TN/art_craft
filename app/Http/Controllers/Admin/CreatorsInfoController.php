<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreatorRequest;
use DB;
use App\Model\Creator;
use Illuminate\Http\Request;
use Storage;


class creatorsInfoController extends MasterController
{
    protected $takeRows = 100;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

//        DB::enableQueryLog();
        $creators = DB::table('creators_v')->take($this->takeRows)
            ->orderBy('created_at')->get();
//        $sql = DB::getQueryLog();
        $count = DB::table('creators_v')->count('*');
        $data = array(
            'creators' => $creators,
            'count' => $count,
//            'sql' => $sql,
        );
//        DB::disableQueryLog();
        return view('admin.userinfo.creatorsInfo', compact('data'));
    }

    public function searchByCreatorName($name=''){
        $creators=null;
        if ($name==''){
            $creators=DB::table('creators_v')
                ->take($this->takeRows)
                ->orderBy('created_at')
                ->get();
            $count=DB::table('creators_v')->count('*');
        }else{
            $creators=DB::table('creators_v')
                ->take($this->takeRows)
                ->where('creator_name','like','%'.$name.'%')
                ->orderBy('created_at')
                ->get();
            $count=DB::table('creators_v')
                ->where('creator_name','like','%'.$name.'%')
                ->orderBy('created_at')
                ->count('*');
        }
        return response()->json([
            'creators'=>$creators,
            'count'=>$count
        ]);
    }
    public function loadData($start,$searchText=''){
        if ($searchText==''){
            $creators= $creators=DB::table('creators_v')
                ->skip($start)
                ->take($this->takeRows)
                ->orderBy('created_at')
                ->get();
        }else{
            $creators= $creators=DB::table('creators_v')
                ->skip($start)
                ->take($this->takeRows)
                ->where('creator_name','like','%'.$searchText.'%')
                ->orderBy('created_at')
                ->get();
        }
        return response()->json([
            'creators'=>$creators
        ]);
    }

    public function changeName(CreatorRequest $request)
    {

        Creator::where('id', $request['id'])->update([
            'creator_name'=>$request['creator_name']
        ]);
        return response()->json([
            'status_code'=>200,
            'creator_name'=>$request['creator_name'],
        ]);

    }


}
