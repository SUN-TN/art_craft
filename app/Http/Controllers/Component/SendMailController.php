<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use App\Mail\PutAwayAuditRefuse;
use App\Mail\TakeOffNotice;
use Mail;


class SendMailController extends Controller
{
    public function sendNotice($to,$subject,$message)
    {
        Mail::raw($message, function ($message) use ($to,$subject) {
            $message->to($to);
            $message->subject($subject);
        });

        return true;
    }

//    public function sendRefuseHtmlNotice($to,$subject,$message,$reason,$username)
//    {
//        $data=['username'=>$username,'message' => $message,'reason'=>$reason];
//        Mail::send('emails.putAwayAuditRefuse',['data'=>$data], function($message) use ($to,$subject){
//            $message->to($to);
//            $message->subject($subject);
//        });
//        return true;
//    }

    public function sendRefuseHtml($to,$subject,$message,$reason,$username)
    {
        $mail = new PutAwayAuditRefuse($subject,$message,$reason,$username);
        Mail::to($to)->send($mail);
        return true;
    }

    public function takeOffNotice($to,$message,$reason,$username){
        $mail=(new TakeOffNotice($message,$reason,$username))
        ->onConnection('redis')
        ->onQueue('emails');
        Mail::to($to)->queue($mail);
    }


}
