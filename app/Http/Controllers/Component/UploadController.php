<?php

namespace App\Http\Controllers\Component;

use App\Model\Goods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Creator;
use Storage;

class UploadController extends Controller
{

    public function  __construct()
    {
        $this->middleware('notUser');
    }
    public function upload(Request $request)
    {
        $upload = $request->file;
        if ($upload->isValid()) {
            $path = $upload->store('', 'goods');
            return response()->json([
                'status_code' => 200,
                'message' => '上传成功',
                'path' => '/images/goods/' . $path,
            ]);
        }
        return response()->json([
            'status_code' => 500,
            'message' => '上传失败！请刷新重试！'
        ]);

    }

    //上传作品图片
    public function uploadCraftImg(Request $request)
    {
        $upload = $request->file;
        if ($upload->isValid()) {
            $id = $request['id'];
            $oldPath = Goods::where('id', $id)->first('imgUrl');
            if ($oldPath != '') {
                $oldPath = explode('/',$oldPath);
                $oldPath = $oldPath[count($oldPath)-1];
                $oldPath = str_replace('}', '', $oldPath);
                $oldPath = str_replace('"', '', $oldPath);
                Storage::disk('goods')->delete($oldPath);
            }
            $path = '/images/goods/' . $upload->store('', 'goods');
            Goods::where('id', $id)->update(['imgUrl' => $path]);
            return response()->json([
                'status_code' => 200,
                'message' => '上传成功',
                'path' => $path,
            ]);
        }
        return response()->json([
            'status_code' => 500,
            'message' => '上传失败！请刷新重试！'
        ]);
    }

    //上传用户头像
    public function uploadUserHead(Request $request)
    {
        $upload = $request->file;
        if ($upload->isValid()) {
            $uid = $request['id'];
            $oldPath = User::where('id', $uid)->first('imgUrl');
            if ($oldPath != '') {
                $oldPath = explode('/',$oldPath);
                $oldPath = $oldPath[count($oldPath)-1];
                $oldPath = str_replace('}', '', $oldPath);
                $oldPath = str_replace('"', '', $oldPath);
                Storage::disk('userHead')->delete($oldPath);
            }
            $path = '/images/userHead/' . $upload->store('', 'userHead');
            User::where('id', $uid)->update(['imgUrl' => $path]);
            return response()->json([
                'status_code' => 200,
                'message' => '上传成功',
                'path' => $path,
            ]);
        }
        return response()->json([
            'status_code' => 500,
            'message' => '上传失败！请刷新重试！'
        ]);
    }


    //上传创作者头像
    public function uploadCreatorHead(Request $request)
    {
        $upload = $request->file;
        if ($upload->isValid()) {
            $uid = $request['id'];
            $oldPath = Creator::where('user_id', $uid)->first('img_path');
            if ($oldPath != '') {
                $oldPath = explode('/',$oldPath);
                $oldPath = $oldPath[count($oldPath)-1];
                $oldPath = str_replace('}', '', $oldPath);
                $oldPath = str_replace('"', '', $oldPath);
                Storage::disk('creatorHead')->delete($oldPath);
            }
            $path='';
            $path = '/images/creatorHead/' . $upload->store('', 'creatorHead');
            Creator::where('id', $uid)->update(['img_path' => $path]);
            return response()->json([
                'status_code' => 200,
                'message' => '上传成功',
                'path' => $path,
            ]);
        }
        return response()->json([
            'status_code' => 500,
            'message' => '上传失败！请刷新重试！'
        ]);
    }
}
