<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use Session;

class MasterResponseController extends Controller
{
    public function success($message)
    {
        return response()->json([
            'status_code' => 200,
            'message' => $message,
        ], 200);
    }

    public function successWithData($message, $data)
    {
        return response()->json([
            'status_code' => 200,
            'message' => $message,
            'data' => $data,
        ], 200);
    }

    public function withData($data)
    {
        return response()->json([
            'status_code' => 200,
            'data' => $data,
        ], 200);
    }


    public function error(...$error)
    {
        return response()->json([
            'status_code' => 500,
            'error' => $error,
        ], 200);
    }

    public function successWithCookie($message, $cookieName, $value, $time = 30)
    {
        $data = [
            'status_code' => 200,
            'message' => $message,
        ];
        return response($data, 200)->cookie($cookieName, $value, $time);
    }

    public function messageWithRepeatLogin()
    {
        $isRepeatLogin = false;
        $message = '';
        if (Session::has('isRepeatLogin')) {
            $isRepeatLogin = Session::get('isRepeatLogin');
            if ($isRepeatLogin) {
                $message = '您的帐号在其他地方登录,请确认是否本人操作!';
            }
            Session::forget('isRepeatLogin');
        }

        return response()->json([
            'status_code' => 200,
            'message' => $message,
            'isRepeatLogin' => $isRepeatLogin,
        ], 200);
    }


}
