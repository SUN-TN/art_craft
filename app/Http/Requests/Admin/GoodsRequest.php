<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class GoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|between:1,32',
            'author' => 'sometimes|required|between:1,32',
            'size' => 'sometimes|required',
            'price' => 'sometimes|required',
            'intro' => 'sometimes|required|between:10,255',
            'genre_id' => 'sometimes|required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '作品名称不能为空',
            'name.between' => '作品名称长度必须在1-32个字符之间',
            'author.required' => '作者名称不能为空',
            'author.between' => '作者名称长度必须在1-32个字符之间',
            'size.required' => '作品尺寸不能为空',
            'price.required' => '作品价格不能为空',
            'intro.required' => '作品简介不能为空',
            'intro.between' => '作品简介必须在10-100个字符之间',
            'genre_id.required' => '作品分类不能为空',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json([
            'status_code' => 500,
            'error' => $validator->errors()->all(),
        ], 200)));
    }
}
