<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Validator;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
//        $this->addValidator();
        return [
            'name'=>[
                'sometimes',
                'required',
                'between:1,16'
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'用户名称不能为空',
            'name.between'=>'用户名称长度为1-16个字符',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json([
            'status_code' => 500,
            'error' => $validator->errors()->all(),
        ])));
    }
}
