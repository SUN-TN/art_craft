<?php

namespace App\Http\Requests\UserCenter;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class OpinionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'captcha' => 'required|captcha',
            'title'=>'required',
            'content'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'captcha.required' => '验证码不能为空',
            'captcha.captcha'=>'验证码错误或已失效',
            'title.required' => '标题不能为空',
            'content.required' => '内容不能为空',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw (new HttpResponseException(response()->json([
            'status_code'=>500,
            'error'=>$validator->errors()->all()
        ])));
    }
}
