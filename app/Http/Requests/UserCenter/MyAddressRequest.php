<?php

namespace App\Http\Requests\UserCenter;

use Illuminate\Foundation\Http\FormRequest;

class MyAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'sometimes|required',
            'phone'=>[
                'sometimes',
                'required',
                'regex:/^1[3|4|5|7|8|9][0-9]\d{8}$/'
            ],
            'province'=>'sometimes|required',
            'city'=>'sometimes|required',
            'county'=>'sometimes|required',
            'region'=>'sometimes|required',
            'detail_address'=>'sometimes|required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'收货人不能为空',
            'phone.required'=>'手机号不能为空',
            'phone.regex'=>'请输入正确的11位手机号',
            'province.required' => '省份/地区不能为空',
            'city.required' => '城市/地区不能为空',
            'county.required' => '城镇地区不能为空',
            'region.required' => '地区不能为空',
            'detail_address.required' => '详细地址不能为空',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json([
            'status_code' => 500,
            'error' => $validator->errors()->all(),
        ], 200)));
    }
}
