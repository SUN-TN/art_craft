<?php

namespace App\Http\Requests\BuyProcess;

use App\Http\Requests\MastrerRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrderRequest extends MastrerRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'sometimes|required',
            'phone'=>[
                'sometimes',
                'required',
                'regex:/^1[3|4|5|7|8|9][0-9]\d{8}$/'
            ],
            'detail_address'=>'sometimes|required',

        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'收货人不能为空',
            'phone.required'=>'手机号不能为空',
            'phone.regex'=>'请输入正确的11位手机号',
            'detail_address.required' => '详细地址不能为空',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json([
            'status_code' => 500,
            'error' => $validator->errors()->all(),
        ], 200)));
    }
}
