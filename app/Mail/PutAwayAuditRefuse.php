<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PutAwayAuditRefuse extends Mailable
{
    use Queueable, SerializesModels;


    public $data=['username'=>'','message'=>'','reason'=>''];
    public $subject;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$message, $reason, $username)
    {
        $this->data['username'] = $username;
        $this->data['message'] = $message;
        $this->data['reason'] = $reason;
        $this->subject=$subject;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.putAwayAuditRefuse')->subject($this->subject);
    }
}
