<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Order;
class OrderTimeOutJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $orderID;
    public function __construct($orderID)
    {
        $this->orderID=$orderID;
        //15分钟后执行
        $this->delay(60*15);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //如果15分钟后此订单还未支付 则将此订单关闭
        Order::where('id',$this->orderID)->where('status',0)->update(['status'=>5]);
    }
}
