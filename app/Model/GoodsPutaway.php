<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GoodsPutaway
 * @package App\Model
 */
class GoodsPutaway extends Model
{

    protected $guarded=[];
    //非递增或者非数字的主键
    public $incrementing = false;
}
