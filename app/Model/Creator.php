<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Creator
 *
 * @property int $id
 * @property string $shop_name 店铺名
 * @property string $img_path 店铺头像
 * @property string $notice 店铺公告
 * @property string $user_id 店铺所属用户外键id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereImgPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereShopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Creator whereUserId($value)
 * @mixin \Eloquent
 */
class Creator extends Model
{
    protected $guarded=[];
    //非递增或者非数字的主键
    public $incrementing = false;

//    public function user()
//    {
//        return $this->belongsTo('\App\User')->select(['shops.id,','imgUrl']);
//    }
}
