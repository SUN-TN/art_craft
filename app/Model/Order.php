<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Order extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    //非数字主键
    public $incrementing = false;
    //
    protected $dates = ['deleted_at'];
}
