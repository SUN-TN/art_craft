<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//发送邮箱验证码
Route::post('/sendSecurityCode', 'Component\SendSecurityCodeController@sendSecurityCode');
//上传作品图片
Route::any('/component/upload', 'Component\UploadController@upload');
Route::post('/uploadCraftImg', 'Component\UploadController@uploadCraftImg');
//上传用户头像
Route::post('/uploadUserImg', 'Component\UploadController@uploadUserHead');
//上传创作者头像
Route::post('/uploadCreatorImg', 'Component\UploadController@uploadCreatorHead');

Route::get('/phpinfo', function () {
    echo phpinfo();
});

Route::get('/unlogout', 'UserCenter\MyInfoController@unLogout');

include __DIR__ . '/admin/web.php';
include __DIR__ . '/entry/web.php';
include __DIR__ . '/user_center/web.php';
include __DIR__ . '/show/web.php';
include __DIR__ . '/buy_process/web.php';
include __DIR__ . '/test/web.php';

