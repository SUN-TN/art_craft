<?php
Route::group(['prefix'=>'order','namespace'=>'BuyProcess'],function (){
    //创建订单
    Route::get('/create/{id}','CreateOrderController@index');
    Route::post('/create/','CreateOrderController@createOrder');

    //支付
    Route::get('/pay/{id}','PayController@index');
    Route::post('/pay/changeOrderAddress','PayController@changeOrderAddress');
    Route::post('/pay','PayController@pay');

    //支付成功
    Route::get('/paySuccess/{id}','PaySuccessController@index');

    //订单详情
    Route::get('/detail/{id}','OrderDetailController@index');
    Route::post('/applyRefund','OrderDetailController@applyRefund');
    Route::post('/applyRefundAndReturn','OrderDetailController@applyRefundAndReturn');
});
