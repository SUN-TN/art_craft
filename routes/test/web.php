<?php
Route::get('/goods/restore/{id}','TestController@restore');
Route::get('/goods/restoreAll','TestController@restoreAll');
Route::get('/goods/getShow/{start}/{end}/{filters?}','TestController@Show');

Route::get('/redis','testController@index');
Route::post('/redis/lock','testController@lock');
