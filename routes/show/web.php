<?php
//首页
Route::group(['prefix' => '/', 'namespace' => 'Show'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/index', 'HomeController@index');
    Route::get('/eyesRefresh', 'HomeController@eyesRefresh');
});


//发现
Route::group(['prefix' => 'eyes', 'namespace' => 'Show'], function () {
    Route::get('/','EyesController@index');
    Route::get('/newPage/{currentPage}/{min}/{max}/{genre_id}/{searchText?}','EyesController@newPage');
});


//热门
Route::group(['prefix' => 'hot', 'namespace' => 'Show'], function () {
    Route::get('/','HotController@index');
    Route::get('/newPage/{page}','HotController@newPage');
});


//新品
Route::group(['prefix' => 'new', 'namespace' => 'Show'], function () {
    Route::get('/','NewController@index');
    Route::get('/newPage/{page}','NewController@newPage');
});


//detail 作品详情

Route::group(['prefix'=>'detail','namespace'=>'Show'],function (){
   Route::get('/{id}','DetailController@index');

   Route::post('/addStar','DetailController@addStar');
   Route::post('/removeStar','DetailController@removeStar');
   Route::post('/addCart','DetailController@addCart');
   Route::post('/removeCart','DetailController@removeCart');
});




