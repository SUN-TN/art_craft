<?php
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    //后台登录界面
    Route::get('/login', 'LoginController@loginForm');
    //后台登录
    Route::post('/login', 'LoginController@login');
    //退出登录
    Route::get('/logout', 'LoginController@logout');

    //修改密码界面
    Route::get('/changePassword', 'AdminController@passwordForm');
    //修改密码
    Route::post('/changePassword', 'AdminController@changePassword');

    //后台界面
    Route::get('/index', 'LoginController@index');

    //作品管理
    Route::resource('/goods', 'GoodsController');
    Route::post('/goods/takeOff','GoodsController@takeOff');
    Route::get('/goods/showNew/{start}/{end}/{genre_id}/{searchText}','GoodsController@showNew');
    Route::get('/goods/fill/{start}/{genre_id}/{searchText}','GoodsController@fill');
    Route::get('/goods/search/{genre_id}/{searchText}','GoodsController@search');
    Route::any('/goods/changeImg','GoodsController@changeImg');
    //作品分类
    Route::resource('/genres', 'GenresController');


    //用户管理
    //用户信息
    Route::get('/usersInfo','UsersInfoController@index');
    Route::get('/usersInfo/searchByUsername/{name?}','UsersInfoController@searchByUsername');
    Route::get('/usersInfo/{start}/{searchText?}','UsersInfoController@loadData');
    Route::post('/usersInfo/changeName','UsersInfoController@changeName');
    //创作者信息
    Route::get('/creatorsInfo','CreatorsInfoController@index');
    Route::get('/creatorsInfo/searchByCreatorName/{name?}','CreatorsInfoController@searchByCreatorName');
    Route::get('/creatorsInfo/{start}/{searchText?}','CreatorsInfoController@loadData');
    Route::post('/creatorsInfo/changeName','CreatorsInfoController@changeName');


    //添加管理员
    Route::get('/addAdmin','AddAdminController@index');
    Route::post('/addAdmin','AddAdminController@addAdmin');


    //创作人申请
    //审核界面
    Route::get('/beCreatorAudit', 'BeCreatorAuditController@index');
    //同意申请
    Route::post('beCreatorAudit/agree', 'BeCreatorAuditController@agree');
    //拒绝申请
    Route::post('beCreatorAudit/refuse', 'BeCreatorAuditController@agree');


    //作品上架申请
    //审核界面
    Route::get('/putAwayAudit', 'PutAwayAuditController@index');
    //作品上架同意申请
    Route::post('/putAwayAudit/agree', 'PutAwayAuditController@agree');
    //作品上架拒绝申请
    Route::post('/putAwayAudit/refuse', 'PutAwayAuditController@refuse');


    //订单管理
    Route::get('/order/','OrderController@index');
    Route::get('/order/newPage/{currentPage}/{status}/{orderId?}','OrderController@newPage');
    Route::get('/order/search/{status}/{orderId?}','OrderController@search');

});



