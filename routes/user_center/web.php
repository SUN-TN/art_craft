<?php
/**
 * Created by PhpStorm.
 * User: s
 * Date: 2020/3/13
 * Time: 12:29
 */

Route::group(['prefix' => 'user', 'namespace' => 'UserCenter'], function () {
    //个人中心 - 我的资料
    Route::get('/', 'MyInfoController@index');
    Route::get('/myinfo', 'MyInfoController@index');
    Route::post('/changeUsername','MyInfoController@changeUsername');

    //个人中心 - 我的收藏
    Route::get('/myStar', 'MyStarController@index');

    //个人中心 - 我的购物车
    Route::get('/myCart', 'MyCartController@index');

    //个人中心 - 我的订单
    Route::get('/myOrder','MyOrderController@index');
    Route::post('/myOrder/cancelOrder','MyOrderController@cancelOrder');
    Route::post('/myOrder/confirmReceipt','MyOrderController@confirmReceipt');

    //个人中心 - 我的地址
    Route::get('/myAddress', 'MyAddressController@index');
    Route::get('/getCity/{id}','MyAddressController@getCity');
    Route::get('/getCounty/{id}','MyAddressController@getCounty');
    Route::get('/getRegion/{id}','MyAddressController@getRegion');
    Route::post('/myAddress/add','MyAddressController@addAddress');
    Route::post('/myAddress/change','MyAddressController@changeAddress');
    Route::delete('/myAddress/delete/{id}','MyAddressController@deleteAddress');

    //个人中心 - 申请成为创作者
    Route::get('/beCreator', 'BeCreatorController@index');
    Route::post('/beCreator', 'BeCreatorController@beCreator');


    //个人中心 - 我的创作者信息
    Route::get('/myCreatorInfo', 'MyCreatorInfoController@index');
    Route::post('/changeCreatorName','MyCreatorInfoController@changeCreatorName');


    //个人中心 - 我的作品
   Route::resource('/myCrafts','MyCraftsController');
   Route::post('/myCrafts/updateCraft','MyCraftsController@updateCraft');
   Route::post('/myCrafts/updatePutAwayApply','MyCraftsController@updatePutAwayApply');
   Route::post('/myCrafts/delete','MyCraftsController@myDelete');
   Route::post('/myCrafts/restore/{id}','MyCraftsController@restore');


   Route::get('/myCrafts/getCrafts/{creator_id}','MyCraftsController@getCrafts');
   Route::get('/myCrafts/getSold/{creator_id}','MyCraftsController@getSold');
   Route::get('/myCrafts/getDeleted/{creator_id}','MyCraftsController@getDeleted');
   Route::get('/myCrafts/getPutAwayApply/{creator_id}','MyCraftsController@getPutAwayApply');
   Route::get('/myCrafts/getBeRejected/{creator_id}','MyCraftsController@getBeRejected');

   //个人中心 - 我的交易
    Route::get('/myDeal','MyDealController@index');
    //已发货
    Route::post('/myDeal/delivered','MyDealController@delivered');
    //确认退款
    Route::post('/myDeal/confirmRefunded','MyDealController@confirmRefunded');
    //确认退款退货
    Route::post('/myDeal/confirmRefundedAndReturn','MyDealController@confirmRefundedAndReturn');

    //个人中心 - 修改密码
    Route::get('/changePassword', 'ChangePasswordController@index');
    Route::post('changePassword','ChangePasswordController@changePassword');

    //个人中心 - 意见反馈
    Route::get('/opinion', 'OpinionController@index');
    Route::post('/opinion', 'OpinionController@add');


});
