<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->uuid('user_id');
            $table->uuid('goods_id');
            $table->integer('town_id',11);
            $table->string('detail_address');
            $table->unsignedTinyInteger('status')->comment('0-等待付款  1-买家已付款  100-买家申请退款 2-卖家已发货  200-买家申请退款退货 3-已收货 4-订单完成(正常交易完成)  5-订单关闭(取消订单/退款/退货完成)');
            $table->timestamp('submit_time')->comment('订单提交时间')->nullable($value = true);;
            $table->timestamp('cancel_time')->comment('取消订单时间')->nullable($value = true);
            $table->timestamp('pay_time')->comment('支付完成时间')->nullable($value = true);
            $table->timestamp('apply_refund_time')->comment('申请退款时间')->nullable($value = true);
            $table->timestamp('refunded_time')->comment('退款完成时间')->nullable($value = true);
            $table->timestamp('apply_refund_return_time')->comment('申请退款退货时间')->nullable($value = true);
            $table->timestamp('refunded_return_time')->comment('退款退货完成时间')->nullable($value = true);
            $table->timestamp('finish_time')->comment('订单完成时间')->nullable($value = true);
            $table->timestamp('close_time')->comment('订单关闭时间')->nullable($value = true);
            $table->primary('id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
