$(document).ready(function () {
    app.mainHeight = window.innerHeight - 220;
});

window.onresize = function () {
    let height = window.innerHeight - 75;
    $('.main').css({"height": height});
    app.mainHeight = window.innerHeight - 220;
};

function img_view() {
    setTimeout(function () {
        $('.goods_img').on({
            'mouseenter': function () {
                $(this).next().show();
            },
            "mouseleave": function () {
                $(this).next().hide();
            }
        });
        $(".img_view").on({
            'mouseenter': function () {
                $(this).show();
            },
            "mouseleave": function () {
                $(this).hide();
            }
        });
    },100);
}

function edit_change_Img(){
    $('.changeGoodsImg').on({
        'mouseenter': function () {
            $('.tip_changeGoodsImg').show();
        },
        "mouseleave": function () {
            $('.tip_changeGoodsImg').hide();
        }
    });

    $(".tip_changeGoodsImg").on({
        'mouseenter': function () {
            $(this).show();
        },
        "mouseleave": function () {
            $(this).hide();
        }
    });
}

img_view();
edit_change_Img();
