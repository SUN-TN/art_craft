$(document).ready(function () {
    app.mainHeight = window.innerHeight - 220;
    img_view();
});

window.onresize = function () {
    let height = window.innerHeight - 75;
    $('.main').css({"height": height});
    app.mainHeight = window.innerHeight - 220;
};


function img_view() {
    setTimeout(function () {
        $('.user_img').on({
            'mouseenter': function () {
                $(this).next().show();
            },
            "mouseleave": function () {
                $(this).next().hide();
            }
        });
        $(".img_view").on({
            'mouseenter': function () {
                $(this).show();
            },
            "mouseleave": function () {
                $(this).hide();
            }
        });


        $('.avatar').on({
            'mouseenter': function () {
                $('.avatar-uploader .el-icon-picture').show();
            },
            "mouseleave": function () {
                $('.avatar-uploader .el-icon-picture').hide();
            }
        });
        $(".avatar-uploader .el-icon-picture").on({
            'mouseenter': function () {
                $(this).show();
            },
            "mouseleave": function () {
                $(this).hide();
            }
        });
    },200)
}

img_view();
